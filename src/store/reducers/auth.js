import {
    AUTH_FAIL,
    AUTH_START,
    FETCH_USER_INVITATIONS_START, FETCH_USER_INVITATIONS_SUCCESS,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS, USER_ACCEPT_INVITATION_START, USER_REMOVE_INVITATION_SUCCESS, USER_SET_CURRENT_BOARD_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    userId: null,
    firstName: null,
    lastName: null,
    email: null,
    boards: [],
    teams: [],
    invitations: [],
    currentBoard: null,
    error: false,
    loading: false,
    errorMsg: ''
}
const reducer = (state = initialState, action) => {
    const reducersMap = {
        [AUTH_START]: () => {
            return {
                ...state,
                loading: true,
                error: false,
                errorMsg: ''
            }
        },
        [LOGIN_SUCCESS]: () => {
            return {
                ...state,
                error: false,
                loading: false,
                errorMsg: '',
                userId: action.userData.id,
                firstName: action.userData.firstName,
                lastName: action.userData.lastName,
                email: action.userData.email,
                boards: action.userData.boards,
                teams: action.userData.teams,
                currentBoard: action.userData.currentBoard
            }
        },
        [REGISTER_SUCCESS]: () => {
            return {
                ...state,
                loading: false,
                error: false,
                errorMsg: ''
            }
        },
        [LOGOUT_SUCCESS]: () => {
            return {
                ...initialState
            }
        },
        [AUTH_FAIL]: () => {
            return {
                ...state,
                loading: false,
                error: true,
                errorMsg: action.errorMsg
            }
        },
        [FETCH_USER_INVITATIONS_START]: () => {
            return {
                ...state,
                loading: true,
                error: false,
                errorMsg: ''
            }
        },
        [FETCH_USER_INVITATIONS_SUCCESS]: () => {
            return {
                ...state,
                invitations: [...action.invitations, ...state.invitations]
            }
        },
        [USER_ACCEPT_INVITATION_START]: () => {
            return {
                ...state,
                loading: true,
                error: false
            }
        },
        [USER_REMOVE_INVITATION_SUCCESS]: () => {
            const invitationIndex = state.invitations.findIndex((inv) => inv.id === action.teamId);
            let invitations = [...state.invitations];
            invitations.splice(invitationIndex, 1);
            return {
                ...state,
                loading: false,
                error: false,
                invitations
            }
        },
        [USER_SET_CURRENT_BOARD_SUCCESS]: () => {
            return {
                ...state,
                currentBoard: action.boardId
            }
        }

    };
    if (Object.keys(reducersMap).includes(action.type)) {
        return reducersMap[action.type]();
    }
    return state;
};
export default reducer;