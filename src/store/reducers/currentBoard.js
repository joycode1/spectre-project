import {
    CHANGE_COLUMN_COMMENT_INDEX,
    CREATE_BOARD_START,
    CREATE_BOARD_SUCCESS,
    CURRENT_BOARD_ADD_COMMENT_START,
    CURRENT_BOARD_ADD_COMMENT_SUCCESS,
    CURRENT_BOARD_EDIT_COMMENT_START,
    CURRENT_BOARD_EDIT_COMMENT_SUCCESS,
    CURRENT_BOARD_FETCH_COMMENTS_START,
    CURRENT_BOARD_FETCH_COMMENTS_SUCCESS,
    CURRENT_BOARD_FETCH_START,
    CURRENT_BOARD_FETCH_SUCCESS,
    CURRENT_BOARD_COMMENT_CHANGE_COLOR_SUCCESS,
    CURRENT_BOARD_DELETE_COMMENT_SUCCESS,
    CURRENT_BOARD_SET_BOARD_INDEX,
    CURRENT_BOARD_SET_MAX_VOTE_COUNT,
    CURRENT_BOARD_FETCH_VOTES_START,
    CURRENT_BOARD_FETCH_VOTES_SUCCESS,
    CURRENT_BOARD_REMOVE_VOTE,
    CURRENT_BOARD_ADD_VOTE,
    CURRENT_BOARD_FETCH_LABELS_START,
    CURRENT_BOARD_FETCH_LABELS_SUCCESS,
    CURRENT_BOARD_FETCH_COMMENTS_LABELS_START,
    CURRENT_BOARD_FETCH_COMMENTS_LABELS_SUCCESS,
    CURRENT_BOARD_ADD_LABEL,
    CURRENT_BOARD_DELETE_LABEL,
    CURRENT_BOARD_DELETE_COMMENT_LABEL,
    CURRENT_BOARD_ADD_COMMENT_LABEL,
    CURRENT_BOARD_ADD_ACTION_ITEM,
    CURRENT_BOARD_FETCH_ACTION_ITEMS,
    CURRENT_BOARD_LOCK,
    CURRENT_BOARD_EDIT_NAME,
    CURRENT_BOARD_SET_ANONYMOUS_COMMENTS,
    CURRENT_BOARD_FETCH_USERS_MOOD,
    CURRENT_BOARD_SET_USERS_MOOD
} from "../actions/actionTypes";


const initialState = {
    boardId: null,
    creatorId: null,
    teamId: null,
    template: null,
    boardName: null,
    dateCreated: null,
    dateEdited: null,
    progressIndex: null,
    maxVoteCount: null,
    columns: [],
    labels: [],
    votes: [],
    userMood: [],
    commentsLabels: [],
    actionItems: [],
    isBoardLocked: false,
    loading: false,
    areAnonymousComments: false
};

const reducer = (state = initialState, action) => {
    const reducersMap = {
        [CREATE_BOARD_START]: () => {
            return {
                ...state,
                loading: true
            }

        },
        [CREATE_BOARD_SUCCESS]: () => {
            return {
                ...state,
                loading: false,
                boardId: action.board.boardId,
                maxVoteCount: action.board.maxVoteCount,
                creatorId: action.board.creatorId,
                teamId: action.board.teamId,
                template: action.board.template,
                boardName: action.board.boardName,
                dateCreated: action.board.dateCreated,
                dateEdited: action.board.dateEdited,
                progressIndex: action.board.progressIndex,
                labels: action.board.labels,
                isBoardLocked: action.board.isBoardLocked,
                areAnonymousComments: action.board.areAnonymousComments,
                columns: action.board.columns
            }
        },
        [CURRENT_BOARD_FETCH_START]: () => {
            return {
                ...initialState,
                loading: true
            }
        },
        [CURRENT_BOARD_FETCH_SUCCESS]: () => {
            return {
                ...state,
                loading: false,
                boardId: action.board.id,
                creatorId: action.board.creatorId,
                maxVoteCount: action.board.maxVoteCount,
                teamId: action.board.teamId,
                template: action.board.template,
                boardName: action.board.boardName,
                dateCreated: action.board.dateCreated,
                dateEdited: action.board.dateEdited,
                progressIndex: action.board.progressIndex,
                labels: action.board.labels,
                isBoardLocked: action.board.isBoardLocked,
                areAnonymousComments: action.board.areAnonymousComments,
                actionItems: action.board.actionItems,
                columns: action.board.columns
            }
        },
        [CURRENT_BOARD_FETCH_COMMENTS_START]: () => {
            return {
                ...state,
                loading: true
            }
        },
        [CURRENT_BOARD_FETCH_COMMENTS_SUCCESS]: () => {
            const columns = state.columns.reduce((acc, curr, index) => {
                acc[index.toString()] = curr;
                acc[index.toString()].comments = action.comments.filter(comment => comment.boardIndex === index).reduce((acc, comment) => {
                    acc.push({
                        ...comment,
                        id: comment.id.toString()
                    })
                    return acc;
                }, [])
                return acc;
            }, {})
            return {
                ...state,
                loading: false,
                columns
            }
        },
        [CHANGE_COLUMN_COMMENT_INDEX]: () => {
            return {
                ...state,
                columns: action.columns
            }
        },
        [CURRENT_BOARD_ADD_COMMENT_START]: () => {
            return {
                ...state,
                loading: true
            }
        },
        [CURRENT_BOARD_ADD_COMMENT_SUCCESS]: () => {
            const columns = {...state.columns};
            columns[action.comment.boardIndex].comments.push(action.comment)
            return {
                ...state,
                loading: false,
                columns
            }
        },
        [CURRENT_BOARD_EDIT_COMMENT_START]: () => {
            return {
                ...state,
                loading: true
            }
        },
        [CURRENT_BOARD_EDIT_COMMENT_SUCCESS]: () => {
            const findCommentIndex = state.columns[action.boardIndex].comments.findIndex(comment => Number(comment.id) === action.commentId);
            const columns = {...state.columns};
            columns[action.boardIndex].comments[findCommentIndex].message = action.message;
            return {
                ...state,
                loading: false,
                columns
            }
        },
        [CURRENT_BOARD_COMMENT_CHANGE_COLOR_SUCCESS]: () => {
            const findCommentIndex = state.columns[action.boardIndex].comments.findIndex(comment => Number(comment.id) === action.commentId);

            const columns = {...state.columns};
            columns[action.boardIndex].comments[findCommentIndex].color = action.color;
            return {
                ...state,
                columns
            }
        },
        [CURRENT_BOARD_DELETE_COMMENT_SUCCESS]: () => {
            const findCommentIndex = state.columns[action.boardIndex].comments.findIndex(comment => Number(comment.id) === Number(action.commentId));
            const columns = {...state.columns};
            columns[action.boardIndex].comments.splice(findCommentIndex, 1);
            return {
                ...state,
                columns
            }
        },
        [CURRENT_BOARD_SET_BOARD_INDEX]: () => {
            return {
                ...state,
                progressIndex: action.progressIndex
            }
        },
        [CURRENT_BOARD_SET_MAX_VOTE_COUNT]: () => {
            return {
                ...state,
                maxVoteCount: action.maxVoteCount
            }
        },
        [CURRENT_BOARD_FETCH_VOTES_START]: () => {
            return {
                ...state,
                loading: true
            }
        }, [CURRENT_BOARD_FETCH_VOTES_SUCCESS]: () => {
            return {
                ...state,
                loading: false,
                votes: action.votes
            }
        },
        [CURRENT_BOARD_REMOVE_VOTE]: () => {
            const findVoteIndex = state.votes.findIndex(vote => vote.id === action.voteId);
            const votes = [...state.votes];
            votes.splice(findVoteIndex, 1);
            return {
                ...state,
                votes

            }
        },
        [CURRENT_BOARD_ADD_VOTE]: () => {
            return {
                ...state,
                votes: [...state.votes, action.vote]
            }
        },
        [CURRENT_BOARD_FETCH_LABELS_START]: () => {
            return {
                ...state,
                loading: true
            }
        },
        [CURRENT_BOARD_FETCH_LABELS_SUCCESS]: () => {
            return {
                ...state,
                loading: false,
                labels: action.labels
            }
        },
        [CURRENT_BOARD_FETCH_COMMENTS_LABELS_START]: () => {
            return {
                ...state,
                loading: true
            }
        },
        [CURRENT_BOARD_FETCH_COMMENTS_LABELS_SUCCESS]: () => {
            const commentsLabels = action.labels.reduce((acc, label) => {
                const mapLabel = state.labels.find(lbl => label.labelId === lbl.id);
                if (mapLabel) {
                    acc.push({...label, name: mapLabel.name, color: mapLabel.color})
                }
                return acc;
            }, [])
            return {
                ...state,
                loading: false,
                commentsLabels
            }
        },
        [CURRENT_BOARD_ADD_LABEL]: () => {
            return {
                ...state,
                labels: [action.label, ...state.labels]
            }
        },
        [CURRENT_BOARD_DELETE_LABEL]: () => {
            const findLabelIndex = state.labels.findIndex(label => label.id === action.labelId);
            const labels = [...state.labels];
            labels.splice(findLabelIndex, 1);
            const filterCommentsLabels = state.commentsLabels.filter(label => label.labelId !== action.labelId)
            return {
                ...state,
                labels,
                commentsLabels: filterCommentsLabels
            }
        }, [CURRENT_BOARD_DELETE_COMMENT_LABEL]: () => {
            const findCommentLabelIndex = state.commentsLabels.findIndex(val => val.id === action.id);
            const commentsLabels = [...state.commentsLabels];
            commentsLabels.splice(findCommentLabelIndex, 1);
            return {
                ...state,
                commentsLabels
            }
        },
        [CURRENT_BOARD_ADD_COMMENT_LABEL]: () => {
            let mapLabel = state.labels.find(lbl => action.label.labelId === lbl.id);
            mapLabel = {...action.label, name: mapLabel.name, color: mapLabel.color};
            return {
                ...state,
                commentsLabels: [mapLabel, ...state.commentsLabels]
            }
        },
        [CURRENT_BOARD_ADD_ACTION_ITEM]: () => {
            return {
                ...state,
                actionItems: [action.actionItem, ...state.actionItems]
            }
        },
        [CURRENT_BOARD_FETCH_ACTION_ITEMS]: () => {
            return {
                ...state,
                actionItems: action.actionItems
            }
        },
        [CURRENT_BOARD_LOCK]: () => {
            return {
                ...state,
                isBoardLocked: true
            }
        },
        [CURRENT_BOARD_EDIT_NAME]: () => {
            return {
                ...state,
                boardName: action.boardName
            }
        }, [CURRENT_BOARD_SET_ANONYMOUS_COMMENTS]: () => {
            return {
                ...state,
                areAnonymousComments: action.areAnonymousComments
            }
        },
        [CURRENT_BOARD_FETCH_USERS_MOOD]: () => {
            return {
                ...state,
                userMood: action.userMood
            }
        },
        [CURRENT_BOARD_SET_USERS_MOOD]: () => {
            const userMood = [...state.userMood];
            const foundUserMoodIndex = userMood.findIndex(mood => mood.id === action.userMood.id);
            if (foundUserMoodIndex !== -1) {
                userMood[foundUserMoodIndex] = action.userMood;
            } else {
                userMood.push(action.userMood);
            }

            return {
                ...state,
                userMood
            }
        }
    };

    if (Object.keys(reducersMap).includes(action.type)) {
        return reducersMap[action.type]();
    }
    return state;
};

export default reducer;