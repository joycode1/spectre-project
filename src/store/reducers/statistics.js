import {
    STATISTICS_CLEAR_BOARD,
    STATISTICS_FETCH_ACTION_ITEMS,
    STATISTICS_FETCH_BOARD_DATA_START,
    STATISTICS_FETCH_BOARD_DATA_SUCCESS, STATISTICS_FETCH_BOARD_LABELS,
    STATISTICS_FETCH_COMMENTS, STATISTICS_FETCH_COMMENTS_LABELS, STATISTICS_FETCH_USERS_MOOD, STATISTICS_FETCH_VOTES
} from "../actions/actionTypes";


const initialState = {
    currentBoard:{},
    currentTeam:{},
    votes:null,
    boardLabels:null,
    commentsLabels: null,
    comments:null,
    actionItems:null,
    userMood:null,
    loading:false
};

const reducer = (state = initialState, action) => {
    const reducersMap = {
        [STATISTICS_FETCH_BOARD_DATA_START]:()=>{
            return{
                ...initialState,
                loading:true
            }
        },
        [STATISTICS_FETCH_BOARD_DATA_SUCCESS]:()=>{
            return {
                ...state,
                loading: false,
                currentBoard:action.board,
                currentTeam:action.team
            }
        },
        [STATISTICS_FETCH_COMMENTS]:()=>{
            return {
                ...state,
                comments:action.comments
            }
        },
        [STATISTICS_FETCH_VOTES]:()=>{
            return {
                ...state,
                votes:action.votes
            }
        },
        [STATISTICS_FETCH_ACTION_ITEMS]:()=>{
            return {
                ...state,
                actionItems:action.actionItems
            }
        },
        [STATISTICS_FETCH_BOARD_LABELS]:()=>{
            return {
                ...state,
                boardLabels: action.boardLabels
            }
        },
        [STATISTICS_FETCH_COMMENTS_LABELS]:()=>{
            return {
                ...state,
                commentsLabels: action.commentsLabels
            }
        },
        [STATISTICS_FETCH_USERS_MOOD]:()=>{
            return {
                ...state,
                userMood:action.userMood
            }
        },
        [STATISTICS_CLEAR_BOARD]:()=>{
            return {
                ...initialState
            }
        }
    };

    if (Object.keys(reducersMap).includes(action.type)) {
        return reducersMap[action.type]();
    }
    return state;
};

export default reducer;