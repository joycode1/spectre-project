import {USER_SEARCH, USER_SEARCH_CLEAR} from "../actions/actionTypes";

const initialState = {
    users:[]
};

const reducer = (state = initialState, action) => {
    const reducersMap = {
        [USER_SEARCH]: () => {
            return {
                ...state,
                users: action.users
            }

        },
        [USER_SEARCH_CLEAR]:()=>{
            return {
                ...initialState
            }
        }
    };

    if (Object.keys(reducersMap).includes(action.type)) {
        return reducersMap[action.type]();
    }
    return state;
};

export default reducer;