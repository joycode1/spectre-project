import {
    NO_TEAMS_FETCH_SUCCESS,
    TEAM_USERS_FETCH_SUCCESS, TEAMS_ACTION_ITEMS_FETCH_START, TEAMS_ACTION_ITEMS_FETCH_SUCCESS,
    TEAMS_BOARD_DELETE_START,
    TEAMS_BOARD_DELETE_SUCCESS,
    TEAMS_BOARDS_FETCH_START,
    TEAMS_BOARDS_FETCH_SUCCESS,
    TEAMS_CLEAR,
    TEAMS_CREATE_START,
    TEAMS_CREATE_SUCCESS,
    TEAMS_DELETE_START,
    TEAMS_DELETE_SUCCESS,
    TEAMS_EDIT_START,
    TEAMS_EDIT_SUCCESS,
    TEAMS_FETCH_START,
    TEAMS_FETCH_SUCCESS,
    TEAMS_PENDING_FETCH_START,
    TEAMS_PENDING_FETCH_SUCCESS,
    USER_INVITE_FAIL,
    USER_INVITE_START,
    USER_INVITE_SUCCESS,
    USER_REMOVED_TEAM_START,
    USER_REMOVED_TEAM_SUCCESS,
    TEAMS_ACTION_ITEM_EDIT_SUCCESS,
    TEAMS_ACTION_ITEM_DELETE_SUCCESS
} from "../actions/actionTypes";


const initialState = {
    teams: [],
    error: false,
    loading: false,
    boards: [],
    actionItems: {}
}

function findTeam(teams, teamId) {
    return teams.findIndex(team => team.id === teamId);
}

const reducer = (state = initialState, action) => {
    const reducersMap = {
        [TEAMS_FETCH_START]: () => {
            return {
                ...state,
                error: false,
                loading: true
            }
        },
        [TEAMS_FETCH_SUCCESS]: () => {
            return {
                ...state,
                error: false,
                teams: action.teams
            }
        },
        [NO_TEAMS_FETCH_SUCCESS]: () => {
            return {
                ...state,
                error: false,
                loading: false
            }
        },
        [TEAM_USERS_FETCH_SUCCESS]: () => {
            const foundTeamIndex = findTeam(state.teams, action.teamId);
            const teams = [...state.teams];
            teams[foundTeamIndex].users = action.users;
            return {
                ...state,
                error: false,
                loading: false,
                teams: teams
            }
        },
        [TEAMS_CREATE_START]: () => {
            return {
                ...state,
                error: false,
                loading: true,
            }
        },
        [TEAMS_CREATE_SUCCESS]: () => {
            return {
                ...state,
                error: false,
                teams: [action.team, ...state.teams]
            }
        },
        [TEAMS_DELETE_START]: () => {
            return {
                ...state,
                loading: true,
                error: false,
            }
        },
        [TEAMS_DELETE_SUCCESS]: () => {
            const foundTeamIndex = findTeam(state.teams, action.teamId);
            const updateTeams = [...state.teams];
            updateTeams.splice(foundTeamIndex, 1);
            return {
                ...state,
                loading: false,
                error: false,
                teams: updateTeams
            }
        },
        [TEAMS_EDIT_START]: () => {
            return {
                ...state,
                loading: true,
                error: false,
            }
        },
        [TEAMS_EDIT_SUCCESS]: () => {
            const teamIndex = findTeam(state.teams, action.teamId);
            const team = {
                ...state.teams[teamIndex],
                teamName: action.teamName,
                description: action.description,
                avatar: action.avatar
            };
            let teams = [...state.teams];
            teams[teamIndex] = team;
            return {
                ...state,
                loading: false,
                error: false,
                teams
            }
        },
        [USER_INVITE_START]: () => {
            return {
                ...state,
                loading: true,
                error: false
            }
        },
        [USER_INVITE_SUCCESS]: () => {
            const teamIndex = findTeam(state.teams, action.teamId);
            const team = {
                ...state.teams[teamIndex],
                users: [...state.teams[teamIndex].users, {...action.user, status: 'pending'}]
            }
            let teams = [...state.teams];
            teams[teamIndex] = team;
            return {
                ...state,
                loading: false,
                error: false,
                teams
            }
        },
        [USER_INVITE_FAIL]: () => {
            return {
                ...state,
                loading: false
            }
        },
        [TEAMS_PENDING_FETCH_START]: () => {

            return {
                ...state,
                error: false,
                loading: true,

            }
        },
        [TEAMS_PENDING_FETCH_SUCCESS]: () => {
            const foundTeamIndex = findTeam(state.teams, action.teamId);
            const teams = [...state.teams];
            teams[foundTeamIndex].users.push(...action.users);
            return {
                ...state,
                error: false,
                loading: false,
                teams: teams
            }
        },
        [USER_REMOVED_TEAM_START]: () => {
            return {
                ...state,
                error: false,
                loading: true
            }
        },
        [USER_REMOVED_TEAM_SUCCESS]: () => {
            const foundTeamIndex = findTeam(state.teams, action.teamId);
            let teams = [...state.teams];
            if (!action.isCreator) {
                teams.splice(foundTeamIndex, 1)
            } else {
                teams[foundTeamIndex].users = teams[foundTeamIndex].users.filter(user => user.id !== action.userId);
            }
            return {
                ...state,
                error: false,
                loading: false,
                teams
            }
        },
        [TEAMS_CLEAR]: () => {
            return {
                ...initialState
            }
        },
        [TEAMS_BOARDS_FETCH_START]: () => {
            return {
                ...state,
                loading: true
            }
        },
        [TEAMS_BOARDS_FETCH_SUCCESS]: () => {
            return {
                ...state,
                loading: false,
                boards: action.boards
            }
        },
        [TEAMS_BOARD_DELETE_START]: () => {
            return {
                ...state,
                loading: true
            }
        }, [TEAMS_BOARD_DELETE_SUCCESS]: () => {
            const findBoardIndex = findTeam(state.boards, action.boardId);
            let boards = [...state.boards];
            boards.splice(findBoardIndex, 1);
            return {
                ...state,
                loading: false,
                boards
            }
        },
        [TEAMS_ACTION_ITEMS_FETCH_START]: () => {
            return {
                ...state,
                loading: true
            }
        },
        [TEAMS_ACTION_ITEMS_FETCH_SUCCESS]: () => {
            const actionItems = {
                ...state.actionItems,
                [action.teamId]: action.actionItems,
            }
            return {
                ...state,
                loading: false,
                actionItems
            }
        },
        [TEAMS_ACTION_ITEM_EDIT_SUCCESS]: () => {

            const actionItems = {...state.actionItems};
            const foundActionItemIndex = actionItems[action.teamId].findIndex(actionItem => actionItem.id === action.id);
            actionItems[action.teamId][foundActionItemIndex] = {
                ...actionItems[action.teamId][foundActionItemIndex],
                message: action.message,
                assignedTo: action.assignedTo,
                status: action.status,
                dateEdited:action.dateEdited
            }
            return {
                ...state,
                actionItems
            }
        },
        [TEAMS_ACTION_ITEM_DELETE_SUCCESS]: () => {
            const actionItems = {...state.actionItems};
            const foundActionItemIndex = actionItems[action.teamId].findIndex(actionItem => actionItem.id === action.id);
            actionItems[action.teamId].splice(foundActionItemIndex, 1)
            return {
                ...state,
                actionItems
            }
        }
    };
    if (Object.keys(reducersMap).includes(action.type)) {
        return reducersMap[action.type]();
    }
    return state;
};

export default reducer;