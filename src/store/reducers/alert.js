import {ALERT_CLEAR, ALERT_ERROR, ALERT_SUCCESS} from "../actions/actionTypes";

const initialState = {
    openAlert: false,
    hasError: false,
    message: null,
    msgType: null
};

const reducer = (state = initialState, action) => {

    const reducersMap = {
        [ALERT_SUCCESS]: () => {
            return {
                ...state,
                message: action.msg,
                openAlert: true,
                hasError: false,
                msgType: 'success'
            }
        },
        [ALERT_ERROR]: () => {
            return {
                ...state,
                message: action.msg,
                hasError: true,
                openAlert: true,
                msgType: 'error'
            }
        },
        [ALERT_CLEAR]: () => {
            return {
                ...initialState,
                msgType: state.msgType
            }
        }
    };
    if (Object.keys(reducersMap).includes(action.type)) {
        return reducersMap[action.type]();
    }
    return state;

};

export default reducer;