import {
    NO_TEAMS_FETCH_SUCCESS,
    TEAM_USERS_FETCH_SUCCESS, TEAMS_ACTION_ITEM_DELETE_SUCCESS, TEAMS_ACTION_ITEM_EDIT_SUCCESS,
    TEAMS_ACTION_ITEMS_FETCH_START,
    TEAMS_ACTION_ITEMS_FETCH_SUCCESS,
    TEAMS_BOARD_DELETE_START,
    TEAMS_BOARD_DELETE_SUCCESS,
    TEAMS_BOARDS_FETCH_START,
    TEAMS_BOARDS_FETCH_SUCCESS,
    TEAMS_CLEAR,
    TEAMS_CREATE_START,
    TEAMS_CREATE_SUCCESS,
    TEAMS_DELETE_START,
    TEAMS_DELETE_SUCCESS,
    TEAMS_EDIT_START,
    TEAMS_EDIT_SUCCESS,
    TEAMS_FETCH_START,
    TEAMS_FETCH_SUCCESS,
    TEAMS_PENDING_FETCH_START,
    TEAMS_PENDING_FETCH_SUCCESS,
    USER_INVITE_FAIL,
    USER_INVITE_START,
    USER_INVITE_SUCCESS,
    USER_REMOVED_TEAM_START,
    USER_REMOVED_TEAM_SUCCESS,
} from "./actionTypes";
import axios from '../../utilities/axios';
import {filterClear} from "./userSearch";
import {deleteUserInvitationsByTeam} from "./auth";
import {alertError, alertSuccess} from "./alert";

const root = 'teams';
const usersRoot = 'users';
const invitationsRoot = 'invitations';
const boardsRoot = 'boards';
const actionItemsRoot = 'actionItems';
const teamsFetchStart = () => {
    return {
        type: TEAMS_FETCH_START
    }
}
const teamsFetchSuccess = (teams) => {
    return {
        type: TEAMS_FETCH_SUCCESS,
        teams
    }
}
const teamUsersFetchSuccess = (users, teamId) => {
    return {
        type: TEAM_USERS_FETCH_SUCCESS,
        users,
        teamId
    }
}
const noTeamsFetchSuccess = () => {
    return {
        type: NO_TEAMS_FETCH_SUCCESS,
    }
}
const fetchTeams = (userId) => {
    return dispatch => {
        dispatch(teamsFetchStart());
        setTimeout(() => {
            axios.get(`${root}?users_like=${userId}`)
                .then(res => {
                    if (res.data.length === 0) {
                        dispatch(noTeamsFetchSuccess([]))
                    } else {
                        dispatch(teamsFetchSuccess(res.data))
                        dispatch(fetchTeamsBoards(res.data));
                        res.data.forEach(team => {
                            dispatch(fetchTeamMembersData(team.users, team.id));
                            dispatch(fetchInvitedUsers(team.id));

                            dispatch(teamsActionItemsFetch(team.id));
                        })
                    }

                })
                .catch(console.log)
        }, 1000)

    }
}
const fetchTeamMembersData = (users, teamId) => {
    return dispatch => {
        let query = users.map(user => `id=${user}`).join('&');
        axios.get(`${usersRoot}?${query}`)
            .then(res => {
                dispatch(teamUsersFetchSuccess(res.data, teamId));
            });
    }
}
const pendingUsersFetchStart = () => {
    return {
        type: TEAMS_PENDING_FETCH_START,

    }
}
const pendingUsersFetchSuccess = (users, teamId) => {
    return {
        type: TEAMS_PENDING_FETCH_SUCCESS,
        users,
        teamId
    }
}
const fetchInvitedUsers = (teamId) => {
    return dispatch => {
        dispatch(pendingUsersFetchStart());
        axios.get(`${invitationsRoot}?teamId=${teamId}`)
            .then(res => {
                if (res.data.length > 0) {
                    let query = res.data.map(invitation => `id=${invitation.userId}`).join('&');
                    axios.get(`${usersRoot}?${query}`)
                        .then(res => {
                            const users = res.data.reduce((acc, user) => {
                                acc = [...acc, {...user, status: 'pending'}];
                                return acc;
                            }, [])
                            dispatch(pendingUsersFetchSuccess(users, teamId));
                        })
                } else {
                    dispatch(pendingUsersFetchSuccess([], teamId));
                }

            })
    }
}
const createTeamStart = () => {
    return {
        type: TEAMS_CREATE_START
    }
};
const createTeamSuccess = (team) => {
    return {
        type: TEAMS_CREATE_SUCCESS,
        team
    }
}
const createTeam = (teamName, description, avatar, userId) => {
    return dispatch => {
        dispatch(createTeamStart());
        const team = {
            teamName,
            creatorId: userId,
            description,
            avatar,
            users: [userId],
            boards: []
        }
        axios.post(`${root}`, team)
            .then(res => {
                dispatch(createTeamSuccess(res.data))
                dispatch(alertSuccess('Team created successfully'))
                dispatch(fetchTeamMembersData(res.data.users, res.data.id));
            })
    }
}
const deleteTeamStart = () => {
    return {
        type: TEAMS_DELETE_START
    }
};
const deleteTeamSuccess = (teamId) => {
    return {
        type: TEAMS_DELETE_SUCCESS,
        teamId
    }
};
const deleteTeam = (teamId) => {
    return dispatch => {
        dispatch(deleteTeamStart())
        axios.delete(`${root}/${teamId}`)
            .then(res => {
                setTimeout(() => {
                    dispatch(deleteTeamSuccess(teamId))
                    dispatch(alertError('Team deleted successfully'))
                    dispatch(deleteUserInvitationsByTeam(teamId));
                }, 600);

            });
    }
}
const editTeamStart = () => {
    return {
        type: TEAMS_EDIT_START
    }
}
const editTeamSuccess = (teamId, teamName, description, avatar) => {
    return {
        type: TEAMS_EDIT_SUCCESS,
        teamId,
        teamName,
        description,
        avatar
    }
}
const editTeam = (teamId, teamName, description, avatar) => {
    return dispatch => {
        dispatch(editTeamStart());
        axios.patch(`${root}/${teamId}`, {teamName, description, avatar})
            .then(res => {
                setTimeout(() => {
                    dispatch(editTeamSuccess(teamId, teamName, description, avatar))
                    dispatch(alertSuccess('Team Edited Successfully'));
                }, 1000);

            })
    }
}
const inviteUserStart = () => {
    return {
        type: USER_INVITE_START
    }
}
const inviteUserSuccess = (user, teamId) => {
    return {
        type: USER_INVITE_SUCCESS,
        user,
        teamId
    }
}
const inviteUserFail = () => {
    return {
        type: USER_INVITE_FAIL
    }
}
const inviteUser = (user, teamId) => {
    return dispatch => {
        dispatch(inviteUserStart());
        setTimeout(() => {
            axios.get(`${invitationsRoot}?userId=${user.id}&teamId=${teamId}`)
                .then(res => {
                    if (res.data.length === 0) {
                        axios.post(invitationsRoot, {userId: user.id, teamId})
                            .then(res => {
                                dispatch(inviteUserSuccess(user, teamId));
                                dispatch(alertSuccess('Invitation sent'))
                                dispatch(filterClear());
                            })
                    } else {
                        dispatch(inviteUserFail());
                    }
                })
        }, 300);

    }
}
const removeUserStart = () => {
    return {
        type: USER_REMOVED_TEAM_START
    }
};
const removeUserSuccess = (userId, teamId, isCreator) => {
    return {
        type: USER_REMOVED_TEAM_SUCCESS,
        userId,
        teamId,
        isCreator
    }
}
const removeUser = (userId, teamId, users, isCreator) => {
    return dispatch => {
        dispatch(removeUserStart());
        const filteredUsers = users.filter(user => user.id !== userId && !user.status).map(user => user.id);
        axios.patch(`${root}/${teamId}`, {users: filteredUsers})
            .then(res => {
                axios.get(`${invitationsRoot}?teamId=${teamId}&userId=${userId}`)
                    .then(res => {
                        if (res.data.length > 0) {
                            axios.delete(`${invitationsRoot}/${res.data[0].id}`)
                                .then(res => {
                                    dispatch(removeUserSuccess(userId, teamId, isCreator))
                                    dispatch(alertError('Invitation removed'))
                                })
                        } else {
                            dispatch(removeUserSuccess(userId, teamId, isCreator))
                            dispatch(alertError('User left team'))
                        }
                    })

            })
    }
}
const teamsClear = () => {
    return {
        type: TEAMS_CLEAR
    }
}
const fetchTeamsBoardsStart = () => {
    return {
        type: TEAMS_BOARDS_FETCH_START
    }
}
const fetchTeamsBoardsSuccess = (boards) => {
    return {
        type: TEAMS_BOARDS_FETCH_SUCCESS,
        boards
    }
}
const fetchTeamsBoards = (teams) => {
    return dispatch => {
        dispatch(fetchTeamsBoardsStart());
        const query = teams.map(team => `teamId=${team.id}`).join('&');
        axios.get(`${boardsRoot}?${query}&_sort=id&_order=desc`)
            .then(res => {
                const boards = [];
                res.data.forEach(board => {
                    board.teamInfo = teams.find(team => team.id === board.teamId);
                    boards.push(board);
                });
                dispatch(fetchTeamsBoardsSuccess(boards));
            })
    }
}
const teamsBoardDeleteStart = () => {
    return {
        type: TEAMS_BOARD_DELETE_START
    }
}
const teamsBoardDeleteSuccess = (boardId) => {
    return {
        type: TEAMS_BOARD_DELETE_SUCCESS,
        boardId
    }
}
const teamsBoardDelete = (boardId) => {
    return dispatch => {
        dispatch(teamsBoardDeleteStart());
        axios.delete(`${boardsRoot}/${boardId}`)
            .then(res => {
                dispatch(teamsBoardDeleteSuccess(boardId))
                dispatch(alertError('Board Successfully deleted.'))
            })
    }
}
const teamsActionItemsFetchStart = () => {
    return {
        type: TEAMS_ACTION_ITEMS_FETCH_START
    }
}
const teamsActionItemsFetchSuccess = (teamId, actionItems) => {
    return {
        type: TEAMS_ACTION_ITEMS_FETCH_SUCCESS,
        teamId,
        actionItems
    }
}
const teamsActionItemsFetch = (teamId) => {
    return dispatch => {
        dispatch(teamsActionItemsFetchStart());
        axios.get(`${actionItemsRoot}?teamId=${teamId}`)
            .then(res => {
                dispatch(teamsActionItemsFetchSuccess(teamId, res.data));
            })
    }
}
const teamActionItemEditSuccess = (teamId, id, message, status, assignedTo,dateEdited) => {
    return {
        type: TEAMS_ACTION_ITEM_EDIT_SUCCESS,
        teamId,
        message,
        status,
        assignedTo,
        dateEdited,
        id
    }
}
const teamActionItemEdit = (teamId, id, message, status, assignedTo) => {
    return dispatch => {
        const dateEdited= new Date();
        axios.patch(`${actionItemsRoot}/${id}`, {message, status, assignedTo,dateEdited})
            .then(res => {
                dispatch(teamActionItemEditSuccess(teamId, id, message, status, assignedTo,dateEdited))
                dispatch(alertSuccess('Action Item Edited'))
            })
    }
}
const teamActionItemDeleteSuccess = (teamId, id) => {
    return {
        type: TEAMS_ACTION_ITEM_DELETE_SUCCESS,
        teamId,
        id
    }
}
const teamActionItemDelete = (teamId, id) => {
    return dispatch => {
        axios.delete(`${actionItemsRoot}/${id}`)
            .then(res => {
                dispatch(teamActionItemDeleteSuccess(teamId, id));
                dispatch(alertError('Action Item Deleted'))
            })
    }
}

export {
    fetchTeams,
    fetchTeamMembersData,
    createTeam,
    deleteTeam,
    editTeam,
    inviteUser,
    teamsClear,
    removeUser,
    fetchTeamsBoards,
    teamsBoardDelete,
    teamsActionItemsFetch,
    teamActionItemEdit,
    teamActionItemDelete
}