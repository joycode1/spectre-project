import {USER_SEARCH, USER_SEARCH_CLEAR} from "./actionTypes";
import axios from '../../utilities/axios';
const root = 'users';
const filteredUsers = (users) => {
    return {
        type: USER_SEARCH,
        users
    }
};
const filterUsers = (value,currentUsers) => {
    return dispatch => {
        if(value){
            axios.get(`${root}?email_like=${value}`)
                .then(res => {
                    const userList = res.data.filter(user=>{
                        return !currentUsers.some(currentUser => user.id===currentUser.id)
                    });
                    dispatch(filteredUsers(userList));
                });
        }else {
            dispatch(filteredUsers([]))
        }
    }
};
const filterClear = ()=>{
    return {
        type: USER_SEARCH_CLEAR
    }
}
export {
    filterUsers,
    filterClear
}