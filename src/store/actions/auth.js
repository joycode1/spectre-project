import {
    AUTH_FAIL,
    AUTH_START,
    FETCH_USER_INVITATIONS_START, FETCH_USER_INVITATIONS_SUCCESS,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    REGISTER_SUCCESS, USER_ACCEPT_INVITATION_START, USER_REMOVE_INVITATION_SUCCESS, USER_SET_CURRENT_BOARD_SUCCESS
} from "./actionTypes";
import axios from '../../utilities/axios';
import {alertSuccess,fetchTeams,teamsClear,filterClear} from "./actionsMap";

const root = 'users';
const invitationsRoot = 'invitations';
const teamsRoot = 'teams';

const authStart = () => {
    return {
        type: AUTH_START
    }
};

const registerSuccess = () => {
    return {
        type: REGISTER_SUCCESS
    }
};
const loginSuccess = (userData) => {
    return {
        type: LOGIN_SUCCESS,
        userData
    }
};
const logoutSuccess = () => {
    return {
        type: LOGOUT_SUCCESS,

    }
};
const authFail = (errorMsg) => {
    return {
        type: AUTH_FAIL,
        errorMsg
    }
};

const register = (firstName, lastName, email, password) => {
    const RegisterData = {
        firstName,
        lastName,
        email,
        password,
        teams: [],
        currentBoard: '',
        boards: []
    };
    return dispatch => {
        dispatch(authStart());
        axios.get(`${root}?email=${email}`)
            .then(res => {
                if (res.data.length > 0) {
                    dispatch(authFail('Email already exists.'))
                } else {
                    axios.post(root, RegisterData)
                        .then(res => {
                            setTimeout(() => {
                                dispatch(registerSuccess());
                                dispatch(alertSuccess('Register successful'))
                            }, 2000);
                        })
                        .catch(res => dispatch(authFail('Error Registering.')))
                }
            });
    }
}
const signIn = (address) => {
    return dispatch => {
        dispatch(authStart());
        axios.get(address)
            .then(res => {
                if (res.data.length > 0) {
                    const userData = res.data[0];
                    localStorage.setItem('userId', userData.id);
                    localStorage.setItem('email', userData.email);
                    dispatch(fetchUserInvitations(userData.id));
                    dispatch(fetchTeams(userData.id));
                    dispatch(loginSuccess(userData));
                    dispatch(alertSuccess('Login successful'))
                } else {
                    dispatch(authFail('Email and password do not match.'))
                }
            })
    }
};
const autoSignIn = (id) => {
    return dispatch => {
        dispatch(signIn(`${root}?id=${id}`));
    }

}
const login = (email, password) => {
    return dispatch => {
        dispatch(signIn(`${root}?email=${email}&password=${password}`))
    }
}
const logout = () => {
    return dispatch => {
        dispatch(authStart());
        setTimeout(() => {
            localStorage.removeItem('userId');
            localStorage.removeItem('email');
            dispatch(teamsClear());
            dispatch(filterClear());
            dispatch(logoutSuccess());
        }, 2000)
    }
}
const fetchUserInvitationsStart = () => {
    return {
        type: FETCH_USER_INVITATIONS_START
    }
}
const fetchUserInvitationsSuccess = (invitations) => {
    return {
        type: FETCH_USER_INVITATIONS_SUCCESS,
        invitations
    }
}
const fetchUserInvitations = (userId) => {
    return dispatch => {
        dispatch(fetchUserInvitationsStart());
        axios.get(`${invitationsRoot}?userId=${userId}`)
            .then(res => {
                if (res.data.length > 0) {
                    res.data.forEach(({teamId, id}) => {
                        axios.get(`${teamsRoot}?id=${teamId}`)
                            .then(res => {
                                dispatch(fetchUserInvitationsSuccess([{...res.data[0], invitationId: id}]))
                            })
                    })
                } else {
                    dispatch(fetchUserInvitationsSuccess([]))
                }

            })
    }
}
const acceptUserInvitationStart = () => {
    return {
        type: USER_ACCEPT_INVITATION_START
    }
}
const deleteUserInvitationSuccess = (teamId) => {
    return {
        type: USER_REMOVE_INVITATION_SUCCESS,
        teamId
    }
}
const acceptUserInvitation = (teamId, teamUsers, userId) => {
    return dispatch => {
        dispatch(acceptUserInvitationStart());
        const users = [...teamUsers, userId];
        axios.patch(`${teamsRoot}/${teamId}`, {users})
            .then(res => {
                dispatch(fetchTeams(userId));
                dispatch(alertSuccess(`Team joined successfully`));
                dispatch(deleteUserInvitation(userId, teamId))
            });
    }
}
const deleteUserInvitation = (userId, teamId) => {
    return dispatch => {
        axios.get(`${invitationsRoot}?userId=${userId}&teamId=${teamId}`)
            .then(res => {
                if (res.data.length > 0) {
                    console.log(res.data[0])
                    axios.delete(`${invitationsRoot}/${res.data[0].id}`)
                        .then(res => {
                            dispatch(deleteUserInvitationSuccess(teamId))
                        })
                }
            })

    }
}
const deleteUserInvitationsByTeam = (teamId) => {
    return dispatch => {
        axios.get(`${invitationsRoot}?teamId=${teamId}`)
        .then(res=>{
            if(res.data.length>0){
                res.data.forEach(invitation=>{
                    axios.delete(`${invitationsRoot}/${invitation.id}`)
                })
            }
        })


    }
}
const setCurrentBoardSuccess = (boardId)=>{
    return {
        type:USER_SET_CURRENT_BOARD_SUCCESS,
        boardId
    }
}
const setCurrentBoard = (userId,boardId)=>{
    return dispatch =>{
        axios.patch(`${root}/${userId}`,{currentBoard:boardId})
            .then(res=> dispatch(setCurrentBoardSuccess(boardId)))
    }
}
export {
    register,
    login,
    autoSignIn,
    logout,
    fetchUserInvitations,
    acceptUserInvitation,
    deleteUserInvitation,
    deleteUserInvitationsByTeam,
    setCurrentBoard
}
