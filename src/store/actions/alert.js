import {ALERT_CLEAR, ALERT_ERROR, ALERT_SUCCESS} from "./actionTypes";
const alertSuccess = (msg) => {
    return {
        type: ALERT_SUCCESS,
        msg,
    }
};

const alertError = (msg) => {
    return {
        type: ALERT_ERROR,
        msg
    }
};

const alertClear = () =>{
    return {
        type: ALERT_CLEAR
    }
};

export {
    alertError,
    alertSuccess,
    alertClear
}