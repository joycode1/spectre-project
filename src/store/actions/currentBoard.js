import {
    CHANGE_COLUMN_COMMENT_INDEX,
    CREATE_BOARD_START,
    CREATE_BOARD_SUCCESS, CURRENT_BOARD_ADD_ACTION_ITEM, CURRENT_BOARD_ADD_COMMENT_LABEL,
    CURRENT_BOARD_ADD_COMMENT_START,
    CURRENT_BOARD_ADD_COMMENT_SUCCESS, CURRENT_BOARD_ADD_LABEL,
    CURRENT_BOARD_ADD_VOTE,
    CURRENT_BOARD_COMMENT_CHANGE_COLOR_SUCCESS, CURRENT_BOARD_DELETE_COMMENT_LABEL,
    CURRENT_BOARD_DELETE_COMMENT_SUCCESS, CURRENT_BOARD_DELETE_LABEL,
    CURRENT_BOARD_EDIT_COMMENT_START,
    CURRENT_BOARD_EDIT_COMMENT_SUCCESS, CURRENT_BOARD_EDIT_NAME, CURRENT_BOARD_FETCH_ACTION_ITEMS,
    CURRENT_BOARD_FETCH_COMMENTS_LABELS_START,
    CURRENT_BOARD_FETCH_COMMENTS_LABELS_SUCCESS,
    CURRENT_BOARD_FETCH_COMMENTS_START,
    CURRENT_BOARD_FETCH_COMMENTS_SUCCESS,
    CURRENT_BOARD_FETCH_LABELS_START,
    CURRENT_BOARD_FETCH_LABELS_SUCCESS,
    CURRENT_BOARD_FETCH_START,
    CURRENT_BOARD_FETCH_SUCCESS, CURRENT_BOARD_FETCH_USERS_MOOD,
    CURRENT_BOARD_FETCH_VOTES_START,
    CURRENT_BOARD_FETCH_VOTES_SUCCESS, CURRENT_BOARD_LOCK,
    CURRENT_BOARD_REMOVE_VOTE, CURRENT_BOARD_SET_ANONYMOUS_COMMENTS,
    CURRENT_BOARD_SET_BOARD_INDEX,
    CURRENT_BOARD_SET_MAX_VOTE_COUNT, CURRENT_BOARD_SET_USERS_MOOD
} from "./actionTypes";
import {templateColumns} from "../../utilities/templateMap";
import axios from "../../utilities/axios";
import {alertError, alertSuccess, setCurrentBoard} from "./actionsMap";

const root = 'boards';
const commentsRoot = 'comments';
const votesRoot = 'votes';
const labelsRoot = 'labels';
const actionItemsRoot = 'actionItems';
const commentLabelsRoot = 'commentsLabels';
const userMood ='userMood';
const createBoardStart = () => {
    return {
        type: CREATE_BOARD_START
    }
}
const createBoardSuccess = (board) => {
    return {
        type: CREATE_BOARD_SUCCESS,
        board
    }
}
const createBoard = (boardName, teamId, template, userId) => {
    return dispatch => {
        dispatch(createBoardStart());
        const board = {
            boardName,
            teamId,
            creatorId: userId,
            template,
            dateCreated: new Date(),
            dateEdited: "",
            progressIndex: 0,
            isBoardLocked: false,
            areAnonymousComments:false,
            maxVoteCount:3,
        }
        axios.post(root, board)
            .then(res => {
                board.boardId = res.data.id;
                board.columns = templateColumns[template]();
                dispatch(setCurrentBoard(userId, res.data.id))
                dispatch(createBoardSuccess(board));
                dispatch(alertSuccess('Board successfully created'))
            });
    }
}
const fetchBoardDataStart = () => {
    return {
        type: CURRENT_BOARD_FETCH_START
    }
}
const fetchBoardDataSuccess = (board) => {
    return {
        type: CURRENT_BOARD_FETCH_SUCCESS,
        board
    }
}
const fetchBoardData = (currentBoardId) => {
    return dispatch => {
        dispatch(fetchBoardDataStart());
        setTimeout(()=>{
            axios.get(`${root}/${currentBoardId}`)
                .then(res => {
                    const board = res.data;
                    board.columns = templateColumns[board.template]();
                    dispatch(fetchBoardDataSuccess(board))
                    dispatch(fetchComments(currentBoardId))
                    dispatch(fetchBoardLabels(currentBoardId))
                    dispatch(fetchActionItems(currentBoardId));
                    dispatch(fetchUserMood(currentBoardId));
                })
        },600);
    }
}

const fetchBoardCommentsStart = () => {
    return {
        type: CURRENT_BOARD_FETCH_COMMENTS_START
    }
}
const fetchBoardCommentsSuccess = (comments) => {
    return {
        type: CURRENT_BOARD_FETCH_COMMENTS_SUCCESS,
        comments
    }
}
const fetchComments = (boardId) => {
    return dispatch => {
        dispatch(fetchBoardCommentsStart());
        axios.get(`${commentsRoot}?boardId=${boardId}`)
            .then(res => {
                dispatch(fetchBoardCommentsSuccess(res.data))
                dispatch(fetchCommentsVotes(res.data))
                dispatch(fetchCommentsLabels(res.data))
            })
    }
}
const changeColumnIndexSuccess = (columns) => {
    return {
        type: CHANGE_COLUMN_COMMENT_INDEX,
        columns
    }
}
const changeColumnIndex = (result, columns) => {
    return dispatch => {
        if (!result.destination) return;
        const {source, destination} = result;
        if (source.droppableId !== destination.droppableId) {
            const sourceColumn = columns[source.droppableId];
            const destColumn = columns[destination.droppableId];
            const sourceItems = [...sourceColumn.comments];
            const destItems = [...destColumn.comments];
            const [removed] = sourceItems.splice(source.index, 1);
            destItems.splice(destination.index, 0, removed);
            dispatch(changeColumnIndexSuccess({
                ...columns,
                [source.droppableId]: {
                    ...sourceColumn,
                    comments: sourceItems
                },
                [destination.droppableId]: {
                    ...destColumn,
                    comments: destItems
                }
            }));
            axios.patch(`${commentsRoot}/${Number(result.draggableId)}`, {boardIndex: Number(destination.droppableId)})
        } else {
            const column = columns[source.droppableId];
            const copiedItems = [...column.comments];
            const [removed] = copiedItems.splice(source.index, 1);
            copiedItems.splice(destination.index, 0, removed);
            dispatch(changeColumnIndexSuccess({
                ...columns,
                [source.droppableId]: {
                    ...column,
                    comments: copiedItems
                }
            }));
            axios.patch(`${commentsRoot}/${Number(result.draggableId)}`, {boardIndex: Number(destination.droppableId)})
        }
    }
}
const addCommentStart = () => {
    return {
        type: CURRENT_BOARD_ADD_COMMENT_START
    }
}
const addCommentSuccess = (comment) => {
    return {
        type: CURRENT_BOARD_ADD_COMMENT_SUCCESS,
        comment
    }
}
const addComment = (message, boardId, creatorId, boardIndex) => {
    return dispatch => {
        dispatch(addCommentStart());
        const comment = {
            message,
            boardId,
            creatorId,
            boardIndex,
            dateCreated: new Date(),
            color: ''
        }
        axios.post(`${commentsRoot}`, comment)
            .then(res => {
                comment.id = res.data.id.toString();
                dispatch(addCommentSuccess(comment))
                dispatch(alertSuccess('Comment added'))
            })
    }
}
const editCommentStart = () => {
    return {
        type: CURRENT_BOARD_EDIT_COMMENT_START
    }
}
const editCommentSuccess = (message, commentId, boardIndex) => {
    return {
        type: CURRENT_BOARD_EDIT_COMMENT_SUCCESS,
        message,
        commentId,
        boardIndex
    }
}
const editComment = (message, commentId, boardIndex) => {
    return dispatch => {
        dispatch(editCommentStart());
        axios.patch(`${commentsRoot}/${commentId}`, {message})
            .then(res => {
                dispatch(editCommentSuccess(message, commentId, boardIndex))
                dispatch(alertSuccess('Comment edited'))
            })
    }
}
const changeColorSuccess = (color, commentId, boardIndex) => {
    return {
        type: CURRENT_BOARD_COMMENT_CHANGE_COLOR_SUCCESS,
        color,
        commentId,
        boardIndex
    }
}
const changeCommentColor = (color, commentId, boardIndex) => {
    return dispatch => {

        axios.patch(`${commentsRoot}/${commentId}`, {color})
            .then(res => {
                dispatch(changeColorSuccess(color, commentId, boardIndex))
                dispatch(alertSuccess('Comment Color Changed'))
            })
    }
}
const deleteCommentSuccess = (commentId, boardIndex) => {
    return {
        type: CURRENT_BOARD_DELETE_COMMENT_SUCCESS,
        commentId,
        boardIndex
    }
}
const deleteComment = (commentId, boardIndex) => {
    return dispatch => {
        axios.delete(`${commentsRoot}/${commentId}`)
            .then(res => {
                dispatch(deleteCommentSuccess(commentId, boardIndex))
            })
    }
}
const setProgressIndexSuccess = (progressIndex)=>{
    return {
        type:CURRENT_BOARD_SET_BOARD_INDEX,
        progressIndex
    }
}
const setProgressIndex = (progressIndex,boardId)=>{
    return dispatch =>{
        axios.patch(`${root}/${boardId}`,{progressIndex:progressIndex})
            .then(res=>{
                dispatch(setProgressIndexSuccess(progressIndex));
            })
    }
}

const setMaxVoteCountSuccess = (maxVoteCount)=>{
    return {
        type:CURRENT_BOARD_SET_MAX_VOTE_COUNT,
        maxVoteCount
    }
}
const setMaxVoteCount = (maxVoteCount,boardId)=>{
    return dispatch =>{
        axios.patch(`${root}/${boardId}`,{maxVoteCount:maxVoteCount})
            .then(res=>{
                dispatch(setMaxVoteCountSuccess(maxVoteCount));
            })
    }
}
const fetchCommentsVotesStart = ()=>{
    return {
        type:CURRENT_BOARD_FETCH_VOTES_START
    }
}
const fetchCommentsVotesSuccess = (votes)=>{
    return{
        type:CURRENT_BOARD_FETCH_VOTES_SUCCESS,
        votes
    }
}
const fetchCommentsVotes = (comments)=>{
    return dispatch=>{
        dispatch(fetchCommentsVotesStart());
        let query = comments.map(comment=>`commentId=${comment.id}`).join('&');
        axios.get(`${votesRoot}?${query}`)
            .then(res=>{
               dispatch(fetchCommentsVotesSuccess(res.data))
            })
    }
}
const removeVoteSuccess = (voteId)=>{
    return {
        type:CURRENT_BOARD_REMOVE_VOTE,
        voteId
    }
}
const removeVote = (voteId)=>{
    return dispatch =>{
        axios.delete(`${votesRoot}/${voteId}`)
            .then(res=>{
                dispatch(removeVoteSuccess(voteId));
                dispatch(alertError('Comment Downvoted'))
            })
    }
}
const addVoteSuccess = (vote)=>{
    return {
        type:CURRENT_BOARD_ADD_VOTE,
        vote
    }
}
const addVote = (commentId,userId)=>{
    return dispatch =>{
       axios.post(`${votesRoot}`,{commentId,userId})
           .then(res=>{
               dispatch(addVoteSuccess({id:res.data.id,commentId,userId}));
               dispatch(alertSuccess('Comment Upvoted'))
           })
    }
}
const fetchBoardLabelsStart = ()=>{
    return {
        type:CURRENT_BOARD_FETCH_LABELS_START
    }
}
const fetchBoardLabelsSuccess = (labels)=>{
    return {
        type:CURRENT_BOARD_FETCH_LABELS_SUCCESS,
        labels
    }
}
const fetchBoardLabels = (boardId)=>{
    return dispatch =>{
        dispatch(fetchBoardLabelsStart())
        axios.get(`${labelsRoot}?boardId=${boardId}`)
            .then(res=>{
                dispatch(fetchBoardLabelsSuccess(res.data));
            })
    }
}
const fetchCommentsLabelsStart = ()=>{
    return {
        type:CURRENT_BOARD_FETCH_COMMENTS_LABELS_START
    }
}
const fetchCommentsLabelsSuccess = (labels)=>{
    return{
        type:CURRENT_BOARD_FETCH_COMMENTS_LABELS_SUCCESS,
        labels
    }
}
const fetchCommentsLabels = (comments)=>{
    return dispatch=>{
        dispatch(fetchCommentsLabelsStart());
        let query = comments.map(comment=>`commentId=${comment.id}`).join('&');
        axios.get(`${commentLabelsRoot}?${query}`)
            .then(res=>{
                dispatch(fetchCommentsLabelsSuccess(res.data))
            })
    }
}
const addBoardLabelSuccess = (label)=>{
    return{
        type:CURRENT_BOARD_ADD_LABEL,
        label
    }
}
const addBoardlabel = (boardId,name,color)=>{
    return dispatch =>{
        axios.post(`${labelsRoot}`,{boardId,name,color})
            .then(res=>{
                dispatch(addBoardLabelSuccess({id:res.data.id,boardId,name,color}))
                dispatch(alertSuccess('Label Added'))
            })
    }
}
const deleteBoardLabelSuccess = (labelId)=>{
    return {
        type:CURRENT_BOARD_DELETE_LABEL,
        labelId
    }
}
const deleteBoardLabel = (id)=>{
    return dispatch =>{
        axios.delete(`${labelsRoot}/${id}`)
            .then(res=>{
                dispatch(deleteBoardLabelSuccess(id));
                dispatch(alertError('Label deleted'))
            })
    }
}

const deleteCommentLabelSuccess = (id)=>{
    return {
        type:CURRENT_BOARD_DELETE_COMMENT_LABEL,
        id
    }
}

const deleteCommentLabel = (id)=> {
    return dispatch => {
        axios.delete(`${commentLabelsRoot}/${id}`)
            .then(res => {
                dispatch(deleteCommentLabelSuccess(id))
            });
    }
}
const addCommentLabelSuccess = (label)=>{
    return {
        type:CURRENT_BOARD_ADD_COMMENT_LABEL,
        label
    }
}
const addCommentLabels = (labels,commentId)=>{
    return dispatch =>{
        labels.forEach(label=>{
            axios.post(`${commentLabelsRoot}`,{labelId:label,commentId})
                .then(res=>{
                    dispatch(addCommentLabelSuccess(res.data))
                })
        })
    }
}
const addActionItemSuccess = (actionItem)=>{
    console.log(actionItem)
    return {
        type:CURRENT_BOARD_ADD_ACTION_ITEM,
        actionItem
    }
}
const addActionItem = (message,assignedTo,status,boardId,teamId)=>{
    return dispatch=>{
        axios.post(`${actionItemsRoot}`,{message,assignedTo,status,boardId,teamId,dateCreated:new Date()})
            .then(res=>{
                dispatch(addActionItemSuccess(res.data))
            })
    }
}
const fetchActionItemsSuccess = (actionItems)=>{
    return {
        type:CURRENT_BOARD_FETCH_ACTION_ITEMS,
        actionItems
    }
}
const fetchActionItems = (boardId)=>{
    return dispatch =>{
        axios.get(`${actionItemsRoot}?boardId=${boardId}`)
            .then(res=>{
                dispatch(fetchActionItemsSuccess(res.data))
            })
    }
}
const currentBoardLockSuccess = ()=>{
    return {
        type:CURRENT_BOARD_LOCK,

    }
}
const currentBoardLock = boardId=>{
    return dispatch=>{
        axios.patch(`${root}/${boardId}`, {isBoardLocked: true})
            .then(res=>{
                dispatch(currentBoardLockSuccess());
                dispatch(alertSuccess('Retrospective Complete and Board is Now Locked'))
            })
    }
}
const editBoardNameSuccess = boardName=>{
    return {
        type:CURRENT_BOARD_EDIT_NAME,
        boardName
    }
}
const editBoardName = (boardName,boardId)=>{
    return dispatch =>{
        axios.patch(`${root}/${boardId}`, {boardName: boardName})
            .then(res=>{
                dispatch(editBoardNameSuccess(boardName));
                dispatch(alertSuccess('Board Name Edited'))
            })
    }
}
const setCommentsAnonymousSuccess = (areAnonymousComments)=>{
    return {
        type:CURRENT_BOARD_SET_ANONYMOUS_COMMENTS,
        areAnonymousComments
    }
}
const setCommentsAnonymous = (areAnonymousComments,boardId)=>{
    return dispatch=>{
        axios.patch(`${root}/${boardId}`, {areAnonymousComments})
            .then(res=>{
                dispatch(setCommentsAnonymousSuccess(areAnonymousComments));
                dispatch(alertSuccess( areAnonymousComments ? 'Comments are Now Anonymous' : 'Comments are Not Anonymous'))
            })
    }
}
const fetchUserMoodSuccess=(userMood)=>{
    return {
        type:CURRENT_BOARD_FETCH_USERS_MOOD,
        userMood
    }
}
const fetchUserMood=(boardId)=>{
    return dispatch =>{
        axios.get(`${userMood}?boardId=${boardId}`)
            .then(res=>dispatch(fetchUserMoodSuccess(res.data)))
    }
}
const setUserMoodSuccess=(userMood)=>{
    return {
        type:CURRENT_BOARD_SET_USERS_MOOD,
        userMood
    }
}
const setUserMood=(usersMood,boardId,userId,mood)=>{
    return dispatch =>{
        const foundUser=usersMood.find(mood=>mood.userId===userId);
        if(foundUser){
            axios.patch(`${userMood}/${foundUser.id}`,{mood})
                .then(res=>{
                    dispatch(setUserMoodSuccess(res.data))
                })
        }else {
            axios.post(`${userMood}`,{boardId,userId,mood})
                .then(res=>{
                    dispatch(setUserMoodSuccess(res.data))
                })
        }
    }

}
export {
    createBoard,
    fetchBoardData,
    changeColumnIndex,
    addComment,
    editComment,
    changeCommentColor,
    deleteComment,
    setProgressIndex,
    setMaxVoteCount,
    removeVote,
    addVote,
    fetchBoardLabels,
    addBoardlabel,
    deleteBoardLabel,
    addCommentLabels,
    deleteCommentLabel,
    addActionItem,
    fetchActionItems,
    currentBoardLock,
    editBoardName,
    setCommentsAnonymous,
    setUserMood
}