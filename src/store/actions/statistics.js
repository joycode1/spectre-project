import axios from "../../utilities/axios";
import {
    STATISTICS_CLEAR_BOARD,
    STATISTICS_FETCH_ACTION_ITEMS,
    STATISTICS_FETCH_BOARD_DATA_START,
    STATISTICS_FETCH_BOARD_DATA_SUCCESS, STATISTICS_FETCH_BOARD_LABELS,
    STATISTICS_FETCH_COMMENTS, STATISTICS_FETCH_COMMENTS_LABELS, STATISTICS_FETCH_USERS_MOOD, STATISTICS_FETCH_VOTES
} from "./actionTypes";
import {templateColumns} from "../../utilities/templateMap";

const commentsRoot = 'comments';
const actionItemsRoot = 'actionItems';
const votesRoot = 'votes';
const labelsRoot = 'labels';
const commentsLabelsRoot = 'commentsLabels';
const userMoodRoot ='userMood';
const fetchBoardDataStart = () => {

    return {
        type: STATISTICS_FETCH_BOARD_DATA_START
    }
}
const fetchBoardDataSuccess = (board, team) => {
    return {
        type: STATISTICS_FETCH_BOARD_DATA_SUCCESS,
        board, team
    }
}
const fetchStatisticsBoardData = (board, team) => {

    return dispatch => {
        dispatch(fetchBoardDataStart());
        dispatch(fetchBoardComments(board.id));
        dispatch(fetchBoardActionItems(board.id));
        dispatch(fetchBoardLabels(board.id));
        dispatch(fetchUserMood(board.id));
        board.columns = templateColumns[board.template]();
        dispatch(fetchBoardDataSuccess(board, team))
    }
}
const fetchBoardLabelsSuccess = (boardLabels)=>{
    return {
        type:STATISTICS_FETCH_BOARD_LABELS,
        boardLabels
    }
}
const fetchBoardLabels = (boardId)=>{
    return dispatch =>{
        axios.get(`${labelsRoot}?boardId=${boardId}`)
            .then(res=>{
                dispatch(fetchBoardLabelsSuccess(res.data))
            })
    }
}
const fetchBoardCommentsSuccess = (comments) => {
    return {
        type: STATISTICS_FETCH_COMMENTS,
        comments
    }
}
const fetchBoardComments = (boardId) => {
    return dispatch => {
        axios.get(`${commentsRoot}?boardId=${boardId}`)
            .then(res => {
                dispatch(fetchBoardCommentsSuccess(res.data))
                dispatch(fetchBoardVotes(res.data));
                dispatch(fetchCommentsLabels(res.data))
            })
    }
}
const fetchCommentsLabelsSuccess = (commentsLabels) => {
    return {
        type: STATISTICS_FETCH_COMMENTS_LABELS,
        commentsLabels
    }
}
const fetchCommentsLabels = (comments) => {
    return dispatch => {
        const query = comments.map(comment=>`commentId=${comment.id}`).join('&');
        axios.get(`${commentsLabelsRoot}?${query}`)
            .then(res => {
               dispatch(fetchCommentsLabelsSuccess(res.data));
            })
    }
}
const fetchBoardVotesSuccess = (votes) => {
    return {
        type: STATISTICS_FETCH_VOTES,
        votes
    }
}
const fetchBoardVotes = (comments) => {
    return dispatch => {
        const query = comments.map(comment=>`commentId=${comment.id}`).join('&');
        axios.get(`${votesRoot}?${query}`)
            .then(res => {
                dispatch(fetchBoardVotesSuccess(res.data))
            })
    }
}
const fetchBoardActionItemsSuccess = (actionItems) => {
    return {
        type: STATISTICS_FETCH_ACTION_ITEMS,
        actionItems
    }
}
const fetchBoardActionItems = (boardId) => {
    return dispatch => {
        axios.get(`${actionItemsRoot}?boardId=${boardId}`)
            .then(res => {
                dispatch(fetchBoardActionItemsSuccess(res.data))
            })
    }
}
const fetchUserMoodSuccess=(userMood)=>{
    return {
        type:STATISTICS_FETCH_USERS_MOOD,
        userMood
    }
}
const fetchUserMood=(boardId)=>{
    return dispatch =>{
        axios.get(`${userMoodRoot}?boardId=${boardId}`)
            .then(res=>dispatch(fetchUserMoodSuccess(res.data)))
    }
}
const clearStatisticsBoard = ()=>{
    return {
        type:STATISTICS_CLEAR_BOARD
    }
}
export {
    fetchStatisticsBoardData,
    clearStatisticsBoard
}