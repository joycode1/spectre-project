import React from "react";
import {Avatar, CardHeader, Typography} from "@material-ui/core";
import defaultAvatar from "../../assets/images/default_team.png";
import AvatarGroup from "@material-ui/lab/AvatarGroup";
import {makeStyles} from "@material-ui/core/styles";
const useStyles = makeStyles(theme => ({

    teamAvatar: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
    usersAvatar: {
        backgroundColor: theme.palette.secondary.dark,
    },
    blur:{
        filter:'blur(8px)'
    }

}));
const TeamInfo = props =>{
    const classes = useStyles();
    return <div className={[!props.teamData && classes.blur,classes.teamInfo].join(' ')}>
        <CardHeader
            avatar={
                <Avatar
                    className={[!props.teamData && classes.blur,classes.teamAvatar].join(' ')}
                    src={props.teamData && props.teamData.avatar ? props.teamData.avatar : defaultAvatar}
                    variant="square"/>
            }
            title={
                <React.Fragment>
                    <Typography variant="h6"
                    >{props.teamData && props.teamData.teamName}</Typography>
                    <Typography variant="subtitle2"
                                color="textSecondary">Creator: {props.teamData && props.teamData.users && props.teamData.users.filter(user => user.id === props.creatorId).map((user, i) => `${user.firstName} ${user.lastName}`).join('')}</Typography>
                </React.Fragment>

            }
            subheader={
                <AvatarGroup max={4}>
                    {props.teamData && props.teamData.users && props.teamData.users.filter(user => !user.status).map((user, i) =>
                        <Avatar key={i}
                                className={[!user.firstName && classes.blur, classes.usersAvatar].join(' ')}>
                            {user.firstName && `${user.firstName[0]}${user.lastName[0]}`}
                        </Avatar>)}
                </AvatarGroup>
            }
        />
    </div>
}
export default TeamInfo;