import React from 'react';
import MuiAlert from '@material-ui/lab/Alert';
import {Snackbar} from "@material-ui/core";

const Alert = (props) => {

    return <Snackbar
        autoHideDuration={1000}
        anchorOrigin={{vertical: 'top', horizontal: 'center'}}
        onClose={props.closed}
        key={`top,center`}
        open={props.open}
    ><MuiAlert elevation={6} variant="filled" severity={props.severity}>{props.message}</MuiAlert></Snackbar>

};
export default Alert;