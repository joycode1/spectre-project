import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Typography, IconButton} from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const useStyles = makeStyles(theme => ({
    counter: {
        marginRight: theme.spacing(1)
    }
}));

const LikeCounter = props => {
    const classes = useStyles();

    const increaseMaxVoteCountHandler = () => {
        props.setMaxVoteCount(props.maxCount+1,props.boardId)
    }
    const decreaseMaxVoteCountHandler = () => {
        props.setMaxVoteCount(props.maxCount-1,props.boardId)
    }
    return (
        <div className={classes.counter}>
            <Typography variant="body2" display="inline">Votes Left: </Typography>
            <Typography variant="h6" display="inline">{props.votesLeft>=0 ? props.votesLeft : 0}</Typography>
            <Typography variant="body1" display="inline"> • </Typography>
            <Typography variant="body2" display="inline">Max Vote Count: </Typography>
            <Typography variant="h6" display="inline">{props.maxCount}</Typography>
            {props.creatorId === props.currentUserId &&
            <React.Fragment>
                <IconButton size="small" onClick={increaseMaxVoteCountHandler}>
                    <AddIcon color="secondary"/>
                </IconButton>
                <IconButton size="small" onClick={decreaseMaxVoteCountHandler} disabled={props.maxCount === 0}>
                    <RemoveIcon color="error"/>
                </IconButton>
            </React.Fragment>
            }

        </div>
    )
}
export default LikeCounter;