import React from "react";
import { TimePicker } from "@material-ui/pickers";

const Timer = props => {

    return (
            <TimePicker
                ampm={false}
                openTo="minutes"
                views={["minutes", "seconds"]}
                format="mm:ss"
                label="Minutes and seconds"
                value={props.selectedDate}
                onChange={props.handleDateChange}
            />

    );
}

export default Timer;