import React, {useEffect, useRef, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {IconButton, Tooltip} from "@material-ui/core";
import Counter from "../Counter/Counter";
import AlarmAddIcon from '@material-ui/icons/AlarmAdd';
import Prompt from "../Prompt/Prompt";
import Timer from "../Counter/Timer";
import PauseIcon from '@material-ui/icons/Pause';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import * as moment from "moment";

const useStyles = makeStyles(theme => ({
    header: {},
    upperHeader: {
        marginBottom: theme.spacing(1),
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        fontFamily: 'Shrikhand, cursive'
    },
    bottomHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing(1)
    },
    timer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }

}));
const CounterTimer = props => {
    const classes = useStyles();
    const [timer, setTimer] = useState(false);
    let [counterRef, setClockCallbackRef] = useState(null);
    const [selectedDate, handleDateChange] = useState(moment("2020-01-01 00:00:00"));
    const [selectedTime, setSelectedTime] = useState(0);
    const [open, setOpen] = useState(false);
    useEffect(()=>{
        setTimer(false);
    },[props.progressIndex])
    const handlePrompt = () => {
        setOpen(!open);
    };
    const startTimerHandler = () => {
        setTimer(false);
        setSelectedTime(moment("2020-01-01 00:00:00"));
        let time = moment(selectedDate).seconds() * 1000 + moment(selectedDate).minutes() * 60000;
        setSelectedTime(time);
        handlePrompt();
        setTimer(true);
    }
    const startTimer = () => {
        counterRef.start();
    }
    const pauseTimer = () => {
        counterRef.pause();
    }
    const setClockRef = (ref) => {
        setClockCallbackRef(ref);
    }
    const onCompleteHandler = () => {
        setTimer(false)
        props.timeAlert();

    }
    return (
        <div className={classes.timer}>
            {timer &&
            <React.Fragment>
                <Counter timer={selectedTime} refCallback={setClockRef} complete={onCompleteHandler}/>
                    <Tooltip title="Continue Timer">
                        <IconButton onClick={startTimer}>
                            <PlayArrowIcon color="primary" />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Pause Timer">
                        <IconButton onClick={pauseTimer}>
                            <PauseIcon color="primary" />
                        </IconButton>
                    </Tooltip>

            </React.Fragment>
            }
            { props.isCreator &&
                <Tooltip title="Set Timer">
                    <IconButton onClick={handlePrompt}>
                        <AlarmAddIcon color="primary" fontSize="large"/>
                    </IconButton>
                </Tooltip>
            }
            <Prompt open={open} close={handlePrompt}
                    message='Set a timer'
                    formContent={<Timer selectedDate={selectedDate}
                                        handleDateChange={handleDateChange}/>}
                    accept={startTimerHandler}/>
        </div>

    )

}

export default CounterTimer;