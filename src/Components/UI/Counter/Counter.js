import React from "react";
import Countdown,{zeroPad} from 'react-countdown';
import * as moment from 'moment';
import {Typography,Box} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {green,red} from "@material-ui/core/colors";
const useStyles = makeStyles(theme=>({
    time:{
        backgroundColor:green["600"],
        color: theme.palette.getContrastText(green["600"]),
        padding:theme.spacing(1),
    },

    timeLeft:{
        backgroundColor:red["600"],
        color: theme.palette.getContrastText(red["600"])
    }
}));
const Completionist = () => <Typography variant="h6">00:00</Typography>;
    const Time = (props)=>{
        const classes=useStyles();
        return <Box borderRadius="borderRadius" className={[ props.minutes===0 && props.seconds<5 && classes.timeLeft, classes.time].join((' '))}>
            <Typography  variant="h5"> {zeroPad(props.minutes)}:{zeroPad(props.seconds)}</Typography>
        </Box>
    }
const renderer = ({ hours, minutes, seconds, completed }) => {

    if (completed) {
        return <Completionist />;
    } else {
        return <Time minutes={minutes} seconds={seconds}/>;
    }
};
const Counter = props => {

 return (<Countdown
     ref={props.refCallback}
     date={moment() + props.timer}
     renderer={renderer}
     onComplete={props.complete}
 />)
}

export default Counter;