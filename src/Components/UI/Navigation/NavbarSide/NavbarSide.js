import React from "react";
import {useStyles} from "../../../../hooks/drawerStyle";
import {Drawer,Toolbar} from "@material-ui/core";
import NavbarSideListItems from "./NavbarSideListItems";
const NavbarSide = props =>{
    const classes = useStyles();
    return <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
            paper: classes.drawerPaper,
        }}
    >
        <Toolbar />
        <div className={classes.drawerContainer}>
            <NavbarSideListItems isAuth={props.isAuth}/>
        </div>
    </Drawer>
};

export default NavbarSide;