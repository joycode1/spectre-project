import React from "react";
import {List, Divider, Toolbar, makeStyles} from "@material-ui/core";
import {teal} from "@material-ui/core/colors";
import {withRouter} from "react-router-dom";
import RouterLink from "../../RouterLink/RouterLink";
import DashboardIcon from '@material-ui/icons/Dashboard';
import LockIcon from '@material-ui/icons/Lock';
import AssignmentIcon from '@material-ui/icons/Assignment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import GroupIcon from '@material-ui/icons/Group';
import TimelineIcon from '@material-ui/icons/Timeline';
const styles = makeStyles(theme =>({
divider:{
    [theme.breakpoints.up('xs')]: {
        marginTop:theme.spacing(10),
        marginBottom:theme.spacing(10)
    },[theme.breakpoints.up('sm')]: {
        marginTop:theme.spacing(30),
        marginBottom:theme.spacing(30)
    },
    col:{

    }
}
}));
const SidebarListItems = props => {
    const classes = styles();
    let list = (
        <React.Fragment>
            <RouterLink to="/dashboard" icon={<AssignmentIcon style={{ color: teal["A400"]}}/>} tooltipText="Current Board"/>
            <RouterLink to="/boards" icon={<DashboardIcon style={{ color: teal["A400"]}}/>} tooltipText="My Boards"/>
            <RouterLink to="/team-management" icon={<GroupIcon style={{ color: teal["A400"]}}/>} tooltipText="Team Management"/>
            <RouterLink to="/statistics" icon={<TimelineIcon style={{ color: teal["A400"]}}/>} tooltipText="Statistics"/>
            <Toolbar />
            <Divider className={classes.divider}/>
            <Toolbar/>
            <RouterLink to="/profile" icon={<AccountCircleIcon style={{ color: teal["A400"]}}/>} tooltipText="Profile"/>
            <RouterLink to="/logout" icon={<LockIcon style={{ color: teal["A400"]}}/>} tooltipText="Logout"/>
        </React.Fragment>
    )
    if (!props.isAuth) {
        list = (
            <React.Fragment>
                <RouterLink to="/login" icon={<LockOpenIcon style={{ color: teal["A400"]}}/>} tooltipText="Login"/>
                <RouterLink to="/register" icon={<AssignmentIndIcon style={{ color: teal["A400"]}}/>} tooltipText="Register"/>
            </React.Fragment>
        )
    }
    return (
        <List>
            {list}
        </List>
    )
};

export default withRouter(SidebarListItems);