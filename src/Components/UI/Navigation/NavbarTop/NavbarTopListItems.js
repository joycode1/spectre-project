import React from "react";
import RouterLink from "../../RouterLink/RouterLink";

const NavbarTopListItems = props =>{

    return  <React.Fragment>
        <RouterLink to="/login" primary="Login" />
        <RouterLink to="/register" primary="Register"/>
    </React.Fragment>;
}

export default NavbarTopListItems;