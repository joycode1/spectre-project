import React from "react";
import {AppBar, Toolbar, Typography, makeStyles, Avatar, Badge} from "@material-ui/core";
import NotificationsIcon from '@material-ui/icons/Notifications';
import {useStyles} from "../../../../hooks/drawerStyle";
import NavbarTopListItems from "./NavbarTopListItems";
import PopperMenu from "../../Popper/PopperMenu";
import UserInvitations from "../../../../Containers/User/UserInvitations/UserInvitations";

const styles = makeStyles(theme => ({
    title: {
        flexGrow: 1,
        fontFamily: 'Shrikhand, cursive'
    },
    avatar: {
        backgroundColor: theme.palette.secondary.dark,
    },
    authItems: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    badgeStyle: {
        cursor: 'pointer',
        marginRight: theme.spacing(10)
    }
}));
const NavbarTop = props => {
    const navClasses = useStyles();
    const classes = styles();
    let items = null;
    if (props.isAuth) {
        items = <div className={classes.authItems}>
            <PopperMenu btnClass={classes.badgeStyle} btnType={Badge}
                        color="error"
                        iconColor="inherit"
                        otherProps={{badgeContent: props.invitations.length}}
                        icon={NotificationsIcon}>
                <UserInvitations invitations={props.invitations} userId={props.userId}/>
            </PopperMenu>
            <Avatar className={classes.avatar}>{props.firstName[0] + props.lastName[0]}</Avatar></div>
    } else {
        items = <NavbarTopListItems/>
    }
    return <AppBar position="fixed" className={navClasses.appBar}>
        <Toolbar variant="dense">
            <Typography variant="h4" className={classes.title}>
                Spectre
            </Typography>
            {items}
        </Toolbar>
    </AppBar>
};

export default NavbarTop;