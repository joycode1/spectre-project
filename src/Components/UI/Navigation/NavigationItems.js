import React from "react";
import NavbarSide from "./NavbarSide/NavbarSide";
import NavbarTop from "./NavbarTop/NavbarTop";
import {connect} from "react-redux";

const NavigationItems = props => {

    return (
        <React.Fragment>
            <NavbarTop isAuth={props.isAuth} loading={props.loading} userId={props.userId}  invitations={props.invitations}
                       firstName={props.firstName}
                       lastName={props.lastName}/>
            <NavbarSide isAuth={props.isAuth}/>
        </React.Fragment>
    )
}
const mapStateToProps = state => {
    return {
        isAuth: state.auth.userId !== null,
        userId:state.auth.userId,
        firstName: state.auth.firstName,
        lastName: state.auth.lastName,
        invitations: state.auth.invitations,
        loading: state.auth.loading
    }
};

export default connect(mapStateToProps, null)(NavigationItems);