import React from 'react';
import PopperAction from "./PopperAction";

const PopperMenu = props => {

    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    const handleToggle = () => {
        setOpen(prevOpen => !prevOpen);
    };
    const handleClose = event => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };

    const prevOpen = React.useRef(open);
    const PopperMenuIcon = props.icon;
    const BtnType = props.btnType;
    React.useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }
        prevOpen.current = open;
    }, [open]);
    return (
        <React.Fragment>
            <BtnType
                ref={anchorRef}
                aria-label="more"
                aria-controls={open ? 'menu-list-grow' : undefined}
                aria-haspopup="true"
                onClick={handleToggle}
                color={props.color}
                {...props.otherProps}
                className={props.btnClass}>
                <PopperMenuIcon color={props.iconColor} style={{fontSize:25}}/>
            </BtnType>
            <PopperAction
                anchorRef={anchorRef}
                open={open}
                close={handleClose}
            >
                {props.children}
            </PopperAction>
        </React.Fragment>
    );
};
export default PopperMenu;