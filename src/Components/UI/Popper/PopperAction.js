import React from 'react';
import { Popper, Grow, MenuList, ClickAwayListener,Paper} from '@material-ui/core';
const PopperAction = props => {
    return (
        <Popper open={props.open} role={undefined} anchorEl={props.anchorRef.current} transition disablePortal style={{zIndex:'2000'}}>
            {({TransitionProps, placement}) => (
                <Grow
                    {...TransitionProps}
                    style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
                >
                    <ClickAwayListener onClickAway={props.close}>
                        <Paper elevation={3}>
                            <MenuList  autoFocusItem={props.open} id="menu-list-grow">
                                {props.children}
                            </MenuList>
                        </Paper>
                    </ClickAwayListener>
                </Grow>)}
        </Popper>
    );
};

export default PopperAction;