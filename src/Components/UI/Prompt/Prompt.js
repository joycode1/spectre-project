import React from 'react';
import {Dialog,DialogActions,DialogTitle,Slide,DialogContent,DialogContentText} from '@material-ui/core';
import {IconButton} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Prompt = props => {
    return (
        <div>
            <Dialog
                open={props.open}
                TransitionComponent={Transition}
                onClose={props.close}
                style={{backgroundColor: 'transparent'}}
            >
                <DialogTitle >{props.message}</DialogTitle>
                <DialogContent>
                    {props.secondaryText && <DialogContentText>
                        {props.secondaryText}
                    </DialogContentText>}
                    {props.formContent && props.formContent}
                </DialogContent>
                <DialogActions>
                    <IconButton onClick={props.accept}>
                        <CheckIcon color="secondary"/>
                    </IconButton>
                    <IconButton onClick={props.close}>
                        <ClearIcon color="error"/>
                    </IconButton >
                </DialogActions>
            </Dialog>
        </div>
    );
}
export default  Prompt;