import React from "react";
import {CirclePicker} from "react-color";

const ColorSwitcher = props =>{

    return (
        <CirclePicker
            colors={props.colors}
            onChangeComplete={props.changedColor}
        />
    )
}
export default ColorSwitcher;