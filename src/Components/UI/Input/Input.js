import React from 'react';
import {Grid, TextField} from "@material-ui/core";

const Input = props => {

    return (<Grid item xs={props.itemSize}>
            <TextField
                variant="outlined"
                required
                fullWidth
                {...props.config}
                value={props.value}
                error={props.error}
                onChange={props.changed}
                helperText={props.errorMsg}

            />
        </Grid>
    )
};
export default Input;