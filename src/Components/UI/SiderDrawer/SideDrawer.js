import React from 'react';
import {Drawer} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles( theme =>({
    drawer: {
        width: 600,
        padding: theme.spacing(3)
    }
    })
);
const SideDrawer = props => {
    const classes = useStyles();
    return (
        <Drawer anchor="right" open={props.open} onClose={props.toggleDrawer}>
            <div className={classes.drawer}>
                {props.children}
            </div>
        </Drawer>)

};
export default SideDrawer;