import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { MobileStepper} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Prompt from "../Prompt/Prompt";

const useStyles = makeStyles({
    root: {
        minWidth:350,
        maxWidth: 400,
        flexGrow: 1,
        backgroundColor:"white"
    }
});

const ProgressStepper = props => {
    const classes = useStyles();
    const [open,setOpen] = useState(false);
    const handlePrompt= ()=>{
        setOpen(!open)
    }
    const btnStepMap ={
        0:'Reveal',
        1:'Vote',
        2:'Label',
        3:'Actions',
        4:'Complete'

    };
    const handleNext = () => {
        if(props.progressIndex+1===5){
            handlePrompt();
        }else {
            props.setIndex(props.progressIndex+1,props.boardId)

        }
    };

    const handleBack = () => {
        props.setIndex(props.progressIndex-1,props.boardId)
    };

    return (
        <React.Fragment>
            <MobileStepper
                variant="progress"
                steps={5}
                position="static"
                activeStep={props.progressIndex}
                className={classes.root}
                nextButton={
                    <Button size="medium" onClick={handleNext} disabled={props.creatorId!==props.currentUserId} >
                        {btnStepMap[props.progressIndex]}
                        {props.progressIndex !== 4 &&  <KeyboardArrowRight />}
                    </Button>
                }
                backButton={
                    <Button size="medium" onClick={handleBack} disabled={props.progressIndex === 0 || props.creatorId!==props.currentUserId} >
                        <KeyboardArrowLeft />
                        {btnStepMap[props.progressIndex - 1]}
                    </Button>
                }

            />
            <Prompt open={open} close={handlePrompt} message="Are you sure you want to complete this retrospective?"  secondaryText="You're about to lock this board. This action is irreversible. Do you want to continue?"  accept={props.lockBoard.bind(undefined,handlePrompt)}/>
        </React.Fragment>


    );
}
export default ProgressStepper;