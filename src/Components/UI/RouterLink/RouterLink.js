import React from "react";
import {NavLink} from "react-router-dom";
import {ListItemIcon,ListItemText,ListItem,Tooltip} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyle = makeStyles(theme => ({
    active: {
        color: theme.palette.text.small,
        backgroundColor: theme.palette.primary.light,
        borderBottom: '2px solid ' + theme.palette.secondary.main,
        '&:hover': {
            color: theme.palette.secondary.small,
            backgroundColor: theme.palette.primary.main,
            borderBottom: '2px solid ' + theme.palette.secondary.main,
        }
    }
}));
const RouterLink = props => {
    const classes = useStyle();
    const {to} = props;
    const renderLink = React.useMemo(
        () => React.forwardRef((itemProps, ref) => <NavLink to={to} style={{width: 'auto' , textTransform:'uppercase'}}
                                                            activeClassName={classes.active}
                                                            ref={ref} {...itemProps} />),
        [to,classes.active],
    );
    return (
        <Tooltip title={props.tooltipText || props.primary} placement="bottom">
        <ListItem button component={renderLink}>
            {props.icon ? <ListItemIcon>{props.icon}</ListItemIcon> : null}
            {props.primary ? <ListItemText primary={props.primary}/> : null}
        </ListItem>
        </Tooltip>
            )
};

export default RouterLink;