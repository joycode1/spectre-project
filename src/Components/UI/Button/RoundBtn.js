import React from 'react';

import AddIcon from "@material-ui/icons/Add";
import {Button, withStyles} from "@material-ui/core";

const RoundedButton = withStyles((theme) => ({
    root: {
        borderRadius: 16,

    },
}))(Button);
const RoundBtn = props => {

    return (<RoundedButton variant="outlined" color="primary" size="small" onClick={props.clicked}>
        <AddIcon color="primary"/> {props.text}
    </RoundedButton>)
}

export default RoundBtn;