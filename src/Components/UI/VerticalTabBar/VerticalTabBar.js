import React from 'react';
import {Paper, Tabs, Tab, Button, makeStyles,Typography,Divider} from "@material-ui/core";
import GroupAddIcon from '@material-ui/icons/GroupAdd';

const useStyles = makeStyles(theme => ({
    paper: {
        height: '100%',
        marginRight: theme.spacing(3),
        paddingBottom: theme.spacing(1)
    },
    tabs: {
        paddingLeft: theme.spacing(1),
    },
    tab: {
        fontWeight: 'bold',
    },
    blur:{
        filter:'blur(8px)'
    },
    tabTitle:{
        textAlign:'center',
        paddingLeft:theme.spacing(1)
    }
}));
const VerticalTabBar = props => {
    const classes = useStyles();
    let tabs = <Tab className={classes.tab}/>;
    if (props.teamNames.length > 0) {
        tabs = props.teamNames.map((name,i)=> <Tab key={i} className={classes.tab} label={name}/>);
    }
    return (
        <Paper elevation={3} className={classes.paper}>
            <Typography variant="overline"  className={classes.tabTitle}>My Teams</Typography>
            <Divider variant="middle" component="p" />
            <Tabs
                orientation="vertical"
                value={props.value}
                onChange={props.changed}
                className={[!props.teamNames && classes.blur,classes.tabs].join(' ')}
            >
                {tabs}

            </Tabs>
            <Button
                size="small"
                startIcon={<GroupAddIcon/>}
                color="primary"
                onClick={props.toggleDrawer}
            >
                Create new team...
            </Button>
        </Paper>)
};
export default VerticalTabBar;