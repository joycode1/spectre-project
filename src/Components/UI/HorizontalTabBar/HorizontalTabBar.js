import React from 'react';
import {Tabs,Tab} from "@material-ui/core";

const HorizontalTabBar = props =>{

    return (<Tabs
        value={props.value}
        onChange={props.changed}
        indicatorColor="secondary"
        textColor="primary"
        variant="fullWidth"
        centered
    >
        <Tab label="To do" />
        <Tab label="In Progress" />
        <Tab label="Done" />
    </Tabs>)
}
export default HorizontalTabBar;