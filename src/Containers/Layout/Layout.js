import React from "react";
import {LinearProgress, ThemeProvider} from "@material-ui/core";
import theme from "../../utilities/theme";
import NavigationItems from "../../Components/UI/Navigation/NavigationItems";
import {Container, CssBaseline,Toolbar} from "@material-ui/core";
import {useStyles} from "../../hooks/drawerStyle";
import withAlert from "../../hoc/withAlert";
const Layout = props => {
    const classes = useStyles();
    return (
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <CssBaseline/>
                <NavigationItems/>
                <main className={classes.content}>
                    <Toolbar/>
                    {props.loading && <LinearProgress color="secondary"/>}
                    <Container fixed maxWidth="xl" >
                            {props.children}
                    </Container>

                </main>
            </div>
        </ThemeProvider>
    )
}
export default withAlert(Layout);