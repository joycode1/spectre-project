import React from 'react';
import no_board from '../../assets/images/no_board.png';
import {makeStyles} from "@material-ui/core/styles";
import {Typography, Button, Container} from "@material-ui/core";
import PostAddIcon from '@material-ui/icons/PostAdd';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import {Link} from "react-router-dom";

const useStyle = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    background: {
        width: '100%',
        height: 'auto',
        margin: '0 auto',
        maxWidth: '600px'
    },
    button: {
        margin: '0 auto'
    }
}));
const EmptyDashboard = props => {
    const classes = useStyle();
    return (
        <Container fixed maxWidth="sm" className={classes.root}>
            <div><img alt='' className={classes.background} src={no_board}/></div>
            <Typography variant="h5" component="h2">
                You currently don't have any active boards.
            </Typography>
            <Typography variant="subtitle1" component="p">
                Click here to start a new retrospective board.
            </Typography>
            <Button component={Link} to='/create-board' className={classes.button} variant="contained" color="primary"
                    startIcon={<PostAddIcon/>}>
                Create a board
            </Button>
            <Typography variant="subtitle1" component="p">
                or
            </Typography>
            <Button component={Link} to='/team-management' className={classes.button} variant="contained" color="primary"
                    startIcon={<GroupAddIcon/>}>
               Create a team
            </Button>
        </Container>

    )

};


export default EmptyDashboard;