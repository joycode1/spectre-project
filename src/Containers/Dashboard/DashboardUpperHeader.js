import React from 'react';
import {
    CardContent,
    Checkbox,
    FormControlLabel,
    IconButton,
    TextField,
    Typography
} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import {titleTemplateMap} from "../../utilities/templateMap";
import CounterTimer from "../../Components/UI/Counter/CounterTimer";
import {makeStyles} from "@material-ui/core/styles";
import * as moment from "moment";
import TeamInfo from "../../Components/TeamInfo/TeamInfo";

const useStyles = makeStyles(theme => ({
    upperHeader: {
        marginBottom: theme.spacing(1),
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column'
        }
    },
    title: {
        fontFamily: 'Shrikhand, cursive'
    },
    bottomHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing(1),
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
        }
    },
    timer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }, teamInfo: {
        display: "flex",
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: "center"
    },
    teamAvatar: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
    usersAvatar: {
        backgroundColor: theme.palette.secondary.dark,
    },
    boardComplete: {display: "flex", alignItems: 'center', justifyContent: 'flex-start'},
    titleActions: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        [theme.breakpoints.down('sm')]: {
            justifyContent: 'center',
        }
    },
    upperHeaderText: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        [theme.breakpoints.down('sm')]: {
            justifyContent: 'center',
            alignItems: 'center'
        }
    },
    blur: {
        filter: 'blur(8px)'
    }

}));

const DashboardUpperHeader = props => {
    const classes = useStyles();
    const dateCreated = props.dateCreated && moment(props.dateCreated).format("Do MMM YY");
    return <div className={classes.upperHeader}>
        <div>
            <CardContent>
                <div className={[props.loading && classes.blur, classes.titleActions].join(' ')}>
                    {!props.openBoardName ?
                        <React.Fragment>
                            <Typography variant="h5"
                                        className={[!props.boardName && classes.blur, classes.title].join(' ')}>{props.boardName}</Typography>
                            {props.isCreator &&
                            <IconButton className={classes.margin} size="small"
                                        onClick={props.handleSetBoardEdit}>
                                <EditIcon fontSize="inherit"/>
                            </IconButton>}
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <TextField value={props.BoardNameEdit} onChange={props.changeBoardNameHandler}/>
                            <IconButton className={classes.margin} size="small"
                                        onClick={props.submitChangeBoardNameHandler}>
                                <CheckIcon fontSize="inherit" color="secondary"/>
                            </IconButton>
                            <IconButton className={classes.margin} size="small"
                                        onClick={props.closeSetBoardEdit}>
                                <ClearIcon color="error" fontSize="inherit"/>
                            </IconButton>
                        </React.Fragment>
                    }

                </div>
                <div className={[props.loading && classes.blur, classes.upperHeaderText].join(' ')}>
                    <Typography variant="subtitle1" color="textSecondary"
                    >{props.template && titleTemplateMap[props.template]()}</Typography>
                    <Typography variant="subtitle2" color="textSecondary" align="left"
                    >Created: {dateCreated}</Typography>
                </div>
            </CardContent>
        </div>
        <div>
            {!props.isBoardLocked &&
            <CounterTimer isCreator={props.isCreator}
                          timeAlert={props.onErrorAlert}
                          progressIndex={props.progressIndex}
            />
            }

            <FormControlLabel className={[props.loading && classes.blur].join(' ')}
                              control={
                                  <Checkbox
                                      checked={props.areAnonymousComments}
                                      onChange={props.changeAnonymousCommentsHandler}
                                      color="primary"
                                      className={[props.loading && classes.blur].join(' ')}
                                      disabled={props.isBoardLocked || !props.isCreator}
                                  />
                              }
                              label={<Typography variant="subtitle2"
                                                 color="textSecondary">Anonymous Comments</Typography>}
            />
        </div>
        <TeamInfo creatorId={props.creatorId}
                  teamData={props.teamData}
                  loading={props.loading}
        />
    </div>
}

export default DashboardUpperHeader;