import React from "react";
import {Slider, withStyles,Typography} from "@material-ui/core";

import {moodColorMap} from "../../../utilities/moodColorMap";

const StyledSlider = withStyles({
    root: {
        background: `linear-gradient(to right, ${Object.values(moodColorMap).join(', ')})`,
        height: 2,
        borderRadius: 4
    },
    thumb: {
        height: 12,
        width: 12,
        backgroundColor: '#fff',
        border: '1px solid currentColor',
        '&:focus, &:hover, &$active': {
            boxShadow: 'inherit',
        },
    },
    track: {
        height: 2,
        borderRadius: 4,
    },
    rail: {
        height: 2,
        borderRadius: 4,
    },
    active: {}
})(Slider);
const marks = [
    {
        value: 1,
        label: '1',
    },
    {
        value: 2,
        label: '2',
    },
    {
        value: 3,
        label: '3',
    },
    {
        value: 4,
        label: '4',
    }, {
        value: 5,
        label: '5',
    }, {
        value: 6,
        label: '6',
    }, {
        value: 7,
        label: '7',
    }, {
        value: 8,
        label: '8',
    }, {
        value: 9,
        label: '9',
    }, {
        value: 10,
        label: '10',
    },
];
const DashboardMoodSlider = props => {
    return <div style={{width: '50%'}}>
        {!props.userMood && <Typography variant="body2" color="textSecondary">Please select your mood:</Typography>}
        <StyledSlider valueLabelDisplay="on"
                      step={1}
                      marks={marks}
                      value={props.userMood}
                      onChange={props.changeUserMoodHandler}
                      onChangeCommitted={props.submitUserMoodHandler}
                      disabled={props.isBoardLocked}
                      min={1}
                      max={10}/>
    </div>
}

export default DashboardMoodSlider;