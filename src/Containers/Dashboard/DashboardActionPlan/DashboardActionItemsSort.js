import React, {useState} from 'react';
import RoundBtn from "../../../Components/UI/Button/RoundBtn";
import {Select, MenuItem, FormControl, InputLabel} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

import AddEditActionItem from "./AddEditActionItem";

const useStyles = makeStyles(theme => ({
    formControl: {
        marginBottom: theme.spacing(2),
        minWidth: 120
    },
    formLabels: {
        marginLeft: theme.spacing(2),
    },
    root: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    select: {
        minWidth: 80
    }
}))

const DashboardActionItemsSort = props => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [ActionItemForm, setActionItemForm] = useState({
        message: '',
        assignedTo: 'team',
        status: 'todo'
    });
    const handleChange = (type, ev) => {
        setActionItemForm({...ActionItemForm, [type]: ev.target.value})
    }

    const handlePrompt = () => {
        setOpen(!open)
        setActionItemForm({
            message: '',
            assignedTo: 'team',
            status: 'todo'
        })
    }
    return (
        <div className={classes.root}>
            {!props.isBoardLocked &&
            <React.Fragment>
                <RoundBtn text="Create Action Item" clicked={handlePrompt}/>
                <AddEditActionItem
                    submit={props.submit.bind(undefined, ActionItemForm.message, ActionItemForm.assignedTo, ActionItemForm.status, handlePrompt)}
                    assignedTo={ActionItemForm.assignedTo}
                    open={open}
                    message={ActionItemForm.message} status={ActionItemForm.status}
                    handlePrompt={handlePrompt}
                    handleChange={handleChange}
                    team={props.team}
                />
            </React.Fragment>
            }
            <FormControl className={classes.formLabels}>
                <InputLabel>Group By</InputLabel>
                <Select
                    className={classes.select}
                    value={props.groupBy}
                    onChange={props.groupByHandler}
                >
                    <MenuItem value={"labels"}>Labels</MenuItem>
                    <MenuItem value={"votes"}>Votes</MenuItem>
                </Select>
            </FormControl>
        </div>
    )
}

export default DashboardActionItemsSort;