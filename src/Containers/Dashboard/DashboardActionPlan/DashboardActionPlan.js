import React from 'react';
import DashboardComment from "../DashboardComments/DashboardComment";
import {Chip, Paper, Grid, Typography, Divider} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import DashboardActionItem from "./DashboardActionItem";
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import DescriptionIcon from '@material-ui/icons/Description';
import DashboardMoodSlider from "./DashboardMoodSlider";
import MoodIcon from '@material-ui/icons/Mood';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
        marginTop: theme.spacing(2),
        padding: theme.spacing(1),
        minHeight: 200
    },
    label: {
        marginBottom: theme.spacing(1),
        padding: theme.spacing(1),
        fontSize: '1em',

    },
    actionItemsRoot: {
        minHeight: 150,
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    actionItemsTitle: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        display: "flex",
        alignItems: 'center',
        justifyContent: 'flex-start'

    },
    moodSliderRoot: {
        minHeight: 100,
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',

    },
    grid: {
        minHeight: 50
    },
    blur:{
        filter:'blur(8px)'
    }
}));
const DashboardActionPlan = props => {
    const classes = useStyles();


    const comments = Object.values(props.columns).reduce((acc, curr) => {
        let cmnts = curr.comments.reduce((accComment, accCurr) => {
            accComment.push({
                ...accCurr,
                votes: props.votes.filter(vote => vote.commentId === Number(accCurr.id)).length,
                columnTitle: curr.title
            })
            return accComment;
        }, [])
        acc.push(...cmnts)
        return acc;
    }, []).sort((a, b) => b.votes - a.votes);

    let ungroupedComments = [...comments];
    let groupedByLabel = props.boardLabels && props.boardLabels.reduce((acc, curr) => {
        const cmnts = props.commentsLabels.filter(lbl => lbl.labelId === curr.id).reduce((labelAcc, labelCurr) => {
            const columnComments = comments.filter(comment => Number(comment.id) === labelCurr.commentId);
            ungroupedComments = ungroupedComments.filter(comment => Number(comment.id) !== labelCurr.commentId);
            labelAcc.push(...columnComments);
            return labelAcc;
        }, []);
        acc.push({comments: cmnts, labelName: curr.name, labelColor: curr.color})
        return acc;
    }, []).filter(({comments}) => comments.length > 0);
    let display = null;
    if (groupedByLabel && props.groupBy === 'labels') {
        groupedByLabel.push({comments: ungroupedComments, labelName: 'Ungrouped'})
        display = groupedByLabel && groupedByLabel.map((grouped, i) => {
            return <React.Fragment key={i}>
                <Chip
                    className={classes.label}
                    label={grouped.labelName}
                    style={{
                        backgroundColor: grouped.labelColor ? grouped.labelColor : 'gray',
                        color: 'white'
                    }}/>
                <Grid container spacing={2} className={classes.grid}>
                    {grouped.comments.map(comment => {
                        return <Grid item xs={12} sm={6} md={4} lg={3} key={comment.id}>
                            <DashboardComment
                                team={props.team}
                                content={comment}
                                groupBy={props.groupBy}
                                areAnonymousComments={props.areAnonymousComments}
                            />
                        </Grid>
                    })}
                </Grid>
            </React.Fragment>
        })
    } else {
        display =
            <React.Fragment>
                <Grid container spacing={2}>
                    {comments.map(comment => <Grid item xs={12} sm={6} md={4} lg={3} key={comment.id}>
                            <DashboardComment
                                team={props.team}
                                content={comment}
                                groupBy={props.groupBy}
                                areAnonymousComments={props.areAnonymousComments}
                            />
                        </Grid>
                    )
                    }
                </Grid>
            </React.Fragment>
    }

    return <Paper className={classes.root} elevation={1}>
        <Typography variant="h6" className={classes.actionItemsTitle}>
            <MoodIcon color="primary"/>
            Your Overall Mood During the Last Sprint</Typography>
        <Paper className={classes.moodSliderRoot} elevation={0} variant="outlined">
            <DashboardMoodSlider userMood={props.userMood} changeUserMoodHandler={props.changeUserMoodHandler}
                                 submitUserMoodHandler={props.submitUserMoodHandler}
                                 isBoardLocked={props.isBoardLocked}
            />
        </Paper>
        <Typography variant="h6" className={classes.actionItemsTitle}>
            <DescriptionIcon color="primary"/>
            Action Items</Typography>
        <Paper className={classes.actionItemsRoot} elevation={0} variant="outlined">
            {props.actionItems && props.actionItems.length === 0 ?
                <Typography variant="h6">You currently don't have any action items.</Typography>
                :
                <Grid container spacing={2} alignItems="center" justify="center">
                    {props.actionItems && props.actionItems.map(item =>
                        <Grid item xs={12} sm={6} md={4} lg={3} key={item.id}>
                            <DashboardActionItem content={item} team={props.team}/>
                        </Grid>)
                    }
                </Grid>

            }
        </Paper>
        <Typography variant="h6" className={classes.actionItemsTitle}>
            <ChatBubbleIcon color="primary"/>
            Comments</Typography>
        <Divider/>
        {display}</Paper>

}

export default DashboardActionPlan;