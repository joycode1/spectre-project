import React from 'react';
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";
import Prompt from "../../../Components/UI/Prompt/Prompt";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    formControl: {
        marginBottom: theme.spacing(2),
        minWidth: 120
    },
    formLabels: {
        marginLeft: theme.spacing(2),
    },
    root: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    select:{
        minWidth:80
    }
}))
const AddEditActionItem = props =>{
    const classes = useStyles();
    return <Prompt open={props.open} close={props.handlePrompt}
                   formContent={
                       <div className={classes.addLabelDiv}>
                           <FormControl fullWidth className={classes.formControl}>
                               <TextField fullWidth  label="Action Item"
                                          value={props.message}
                                          onChange={props.handleChange.bind(undefined, 'message')}/>
                           </FormControl>
                           <FormControl fullWidth className={classes.formControl}>
                               <InputLabel>Assign to:</InputLabel>
                               <Select
                                   multiline
                                   fullWidth
                                   value={props.assignedTo}
                                   onChange={props.handleChange.bind(undefined, 'assignedTo')}
                               >
                                   <MenuItem value="team">Team</MenuItem>
                                   {props.team && props.team.users.map((user,i) => <MenuItem key={i}
                                       value={user.id}>{user.firstName} {user.lastName}</MenuItem>)}
                               </Select>
                           </FormControl>
                           <FormControl fullWidth className={classes.formControl}>
                               <InputLabel>Status:</InputLabel>
                               <Select
                                   multiline
                                   fullWidth
                                   value={props.status}
                                   onChange={props.handleChange.bind(undefined, 'status')}
                               >
                                   <MenuItem value="todo">To do</MenuItem>
                                   <MenuItem value="inProgress">In progress</MenuItem>
                                   <MenuItem value="done">Done</MenuItem>

                               </Select>
                           </FormControl>
                       </div>
                   }
                   message='Create Action Item'
                   accept={props.submit}
    />


}
export default AddEditActionItem;