import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Card,CardContent,Typography,CardHeader} from "@material-ui/core";
import DescriptionIcon from '@material-ui/icons/Description';
import * as moment from "moment";
const useStyles = makeStyles(theme=>({
    root:{
        height: '100%',
        marginTop:theme.spacing(1),
        marginBottom:theme.spacing(1),
    }
}));
const DashboardActionItem = props =>{
    const date = props.content.dateCreated && moment(props.content.dateCreated).format('Do MMM YY');
    const user = props.content.assignedTo !== 'team' &&  props.team && props.team.hasOwnProperty('users') ? props.team.users.find(user => user.id === props.content.assignedTo) : null ;
    const assignedTo = user ? `${user.firstName} ${user.lastName}` : 'Team';
    const classes = useStyles();
    const statusMap={
        inProgress:'In Progress',
        todo:'To do',
        done:'Done'
    };
    return (
        <Card className={classes.root}>
            <CardHeader
                avatar={
                    <DescriptionIcon fontSize="large" color="primary"/>
                }
                title={'Status: ' + statusMap[props.content.status]}
                subheader={'Created: ' + date}
            />
            <CardContent>
                <Typography variant="h6"  component="h2">
                    {props.content.message}
                </Typography>
                <Typography className={classes.title} variant="subtitle2" color="textSecondary" gutterBottom>
                    Assigned To: {assignedTo}
                </Typography>
            </CardContent>

        </Card>
    )

}

export default DashboardActionItem;