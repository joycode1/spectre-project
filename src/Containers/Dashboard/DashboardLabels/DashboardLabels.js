import React, {useState} from 'react';
import {Chip, TextField, Select, MenuItem, FormControl} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

import Prompt from "../../../Components/UI/Prompt/Prompt";
import ColorSwitcher from "../../../Components/UI/ColorSwatcher/ColorSwatcher";
import {labelColors} from "../../../utilities/colorsMap";
import DashboardLabelsMap from "./DashboardLabelsMap";
import RoundBtn from "../../../Components/UI/Button/RoundBtn";

const useStyles = makeStyles(theme => ({
    labelsRoot: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        '& > *': {
            margin: theme.spacing(0.5),
        },
    },
    addLabelDiv: {
        width: 300,
        height: 160,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        marginBottom: theme.spacing(2)
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2
    }
}));

const DashboardLabels = props => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);
    const [AddLabelForm, setAddLabelForm] = useState({
        name: '',
        color: '',
        labels: []
    });

    const handleChange = (type, ev) => {
        if (type === 'color') {
            setAddLabelForm({name: AddLabelForm.name, color: ev.hex, labels: []})
        } else if (type === 'name') {
            setAddLabelForm({name: ev.target.value, color: AddLabelForm.color, labels: []})
        } else if (type === 'labels') {
            setAddLabelForm({name: AddLabelForm.name, color: AddLabelForm.color, labels: ev.target.value})
        }
    }
    const handlePrompt = () => {
        setAddLabelForm({name: '', color: '', labels: []})
        setOpen(!open);
    };
    return (
        <div className={classes.labelsRoot}>
            {props.isCreator &&
            <React.Fragment>
                <RoundBtn clicked={handlePrompt} text="Add Label"/>
                <Prompt open={open} close={handlePrompt}
                        formContent={
                            <div className={classes.addLabelDiv}>
                                {props.type === 'addBoard' ?
                                    <React.Fragment>
                                        <TextField fullWidth className={classes.input} label="Label Name"
                                                   value={AddLabelForm.name}
                                                   onChange={handleChange.bind(undefined, 'name')}/>
                                        <ColorSwitcher colors={labelColors}
                                                       changedColor={handleChange.bind(undefined, 'color')}/>
                                    </React.Fragment>
                                    :
                                    <React.Fragment>
                                        <FormControl fullWidth className={classes.formControl}>
                                            <Select
                                                multiple
                                                multiline
                                                value={AddLabelForm.labels}
                                                onChange={handleChange.bind(undefined, 'labels')}
                                                renderValue={(selected) =>
                                                    <div className={classes.chips}>
                                                        {props.boardLabels.filter(label => selected.includes(label.id)).map(label =>
                                                            <Chip key={label.id}
                                                                  className={classes.chip}
                                                                  label={label.name}
                                                                  style={{
                                                                      backgroundColor: label.color ? label.color : 'gray',
                                                                      color: 'white'
                                                                  }}/>
                                                        )}
                                                    </div>}
                                            >
                                                {props.boardLabels && props.boardLabels.map((label) => (
                                                    <MenuItem key={label.id} value={label.id}>
                                                        <Chip
                                                            label={label.name}
                                                            style={{
                                                                backgroundColor: label.color ? label.color : 'gray',
                                                                color: 'white'
                                                            }}/>
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>

                                    </React.Fragment>
                                }
                            </div>
                        }
                        message='Add a New Label'
                        accept={props.type === 'addBoard' ? props.submit.bind(undefined, AddLabelForm.name, AddLabelForm.color, handlePrompt)
                            : props.submit.bind(undefined, AddLabelForm.labels,props.commentId, handlePrompt)}
                />
            </React.Fragment>
            }
            {props.labels && <DashboardLabelsMap labels={props.labels}
                                                 delete={props.delete}
            />}
        </div>
    )
}

export default DashboardLabels;