import React from 'react';
import {Chip} from "@material-ui/core";

const DashboardLabelsMap = props =>{

    return (
        props.labels.map(label => <Chip
                key={label.id}
                label={label.name}
                onDelete={props.delete.bind(undefined, label.id)}
                style={{backgroundColor: label.color ? label.color : 'gray', color: 'white'}}
            />
        )
    )
}
export default DashboardLabelsMap;