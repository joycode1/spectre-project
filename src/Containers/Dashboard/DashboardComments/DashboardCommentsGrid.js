import React from "react";
import {
    Grid,
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import DashboardCommentSection from "./DashboardCommentsSection";
import {DragDropContext} from "react-beautiful-dnd";

const useStyles = makeStyles(theme => ({
    grid: {
        marginTop: theme.spacing(2),
        paddingBottom: theme.spacing(1)
    }
}));

const DashboardCommentsGrid = props => {
    const classes = useStyles();

    return (<Grid className={classes.grid} container spacing={1} direction="row"
                  justify="space-evenly"
                  alignItems="flex-start">
        <DragDropContext onDragEnd={result => props.dragEnd(result, props.sections)}>
            {Object.entries(props.sections).map(([columnId, column], index) => {
                return (
                    <DashboardCommentSection key={index} columnId={columnId} column={column} team={props.team}
                                             addComment={props.addComment}
                                             template={props.template}
                                             editComment={props.editComment}
                                             deleteComment={props.deleteComment}
                                             removeVote={props.removeVote}
                                             addVote={props.addVote}
                                             onAddCommentLabels={props.addCommentLabels}
                                             deleteCommentLabel={props.deleteCommentLabel}
                                             votesLeft={props.votesLeft}
                                             commentsLabels={props.commentsLabels}
                                             boardLabels={props.boardLabels}
                                             changeCommentColor={props.changeCommentColor}
                                             currentUserId={props.currentUserId}
                                             currentBoardId={props.currentBoardId}
                                             currentBoardCreatorId={props.currentBoardCreatorId}
                                             progressIndex={props.progressIndex}
                                             votes={props.votes}
                                             areAnonymousComments={props.areAnonymousComments}

                    />
                )
            })}
        </DragDropContext>

    </Grid>)
}

export default DashboardCommentsGrid;