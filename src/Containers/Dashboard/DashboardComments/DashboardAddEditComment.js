import React from "react";
import {Card, CardActions, CardContent, IconButton, TextField} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    comment: {
        width: '100%',
        marginBottom: theme.spacing(1),
        minHeight: 10
    },
}));
const DashboardAddEditComment = props => {
    const classes = useStyles();
    return <Card className={classes.comment} style={{background: `${props.color ? props.color : '#F4F8FC'}`}} elevation={0}
                 variant="outlined">
        <form>
            <CardContent>
                <TextField
                    variant="outlined"
                    value={props.commentMsg}
                    onChange={props.changed}
                    fullWidth
                    multiline
                    rows={4}
                    placeholder="Write your thoughts..."
                />
            </CardContent>
            <CardActions disableSpacing>
                <IconButton type="submit" onClick={props.confirm}>
                    <CheckIcon color="secondary"/>
                </IconButton>
                <IconButton onClick={props.cancel}>
                    <ClearIcon color="error"/>
                </IconButton>
            </CardActions>
        </form>
    </Card>
}
export default DashboardAddEditComment;