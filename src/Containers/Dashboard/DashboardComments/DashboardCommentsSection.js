import React, {useState} from 'react';
import {
    Button,
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    Paper,
    Typography
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {indigo, teal} from "@material-ui/core/colors";
import {Droppable, Draggable} from "react-beautiful-dnd";
import AddCommentIcon from "@material-ui/icons/AddComment";
import DashboardAddEditComment from "./DashboardAddEditComment";
import DashboardComment from "./DashboardComment";
import {templateGridColorMap} from "../../../utilities/templateMap";

const useStyles = makeStyles(theme => ({
    grid: {
        marginTop: theme.spacing(2),
        paddingBottom: theme.spacing(1)
    },
    paperRoot: {
        paddingBottom: theme.spacing(1),
        flexWrap: 'wrap'
    },
    sectionTitle: {
        backgroundColor: teal["100"],
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexShrink: 0,
        height: ' 7.5rem',
        zIndex: 1
    },
    media: {
        height: 80,
        paddingTop: '56.25%',
        width: 80

    },
    header: {
        alignSelf: 'flex-start'
    },
    commentsSection: {
        marginTop: -20,
        minHeight: 100,
        padding: theme.spacing(1),
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    },
    subHeader:{
        fontSize:'0.8em',
        [theme.breakpoints.up('lg')]: {
            fontSize:'0.9em',
        },
    }
}));
const DashboardCommentSection = props => {
    const classes = useStyles();
    const [openAddComment, setOpenAddComment] = useState(false);
    const [openEditComment, setEditComment] = useState(false);
    const [editCommentIndex, setEditCommentIndex] = useState('');
    const [openColorSwatch, setOpenColorSwatch] = useState(false);
    const [commentMsg, setCommentMsg] = useState('');
    const openAddCommentHandler = () => {
        setCommentMsg('')
        setOpenAddComment(true);
    }
    const inputCommentHandler = (ev) => {
        setCommentMsg(ev.target.value);
    }
    const closeAddCommentHandler = () => {
        setOpenAddComment(false);
    }
    const openColorSwatchHandler = () => {
        setOpenColorSwatch(!openColorSwatch)
    }
    const openEditCommentHandler = (content, ev) => {
        setEditComment(true);
        setEditCommentIndex(content.id);
        setCommentMsg(content.message);
    }
    const closeEditCommentHandler = () => {
        setEditComment(false);
        setEditCommentIndex('');
    }
    const submitCommentHandler = (ev) => {
        ev.preventDefault();
        if (commentMsg !== '') {
            if (openAddComment) {
                props.addComment(commentMsg, props.currentBoardId, props.currentUserId, Number(props.columnId))
            } else {
                props.editComment(commentMsg, Number(editCommentIndex), Number(props.columnId))
            }
        }
        closeAddCommentHandler();
        closeEditCommentHandler();
    }
    const submitColorHandler = (content, color) => {
        props.changeCommentColor(color.hex, Number(content.id), Number(props.columnId))
        openColorSwatchHandler();
    }
    const upVoteDownVoteHandler = (isUpVoted, commentId, userId, voteId) => {
        if (isUpVoted !== -1) {
            props.removeVote(voteId)
        } else {
            props.addVote(Number(commentId), userId);
        }

    }
    const addCommentLabelsHandler = (labels, commentId, promptCallback) => {
        if (labels.length > 0) {
            props.onAddCommentLabels(labels, Number(commentId));
        }
        promptCallback();
    }
    return (<Grid item xs={12} sm={12} md={6} lg={3} >
        <Paper className={classes.paperRoot}>
            <Card className={classes.sectionTitle} style={{ backgroundColor: templateGridColorMap[props.template] ? templateGridColorMap[props.template] : teal["100"]}}>
                <CardHeader
                    className={classes.header}
                    title={<Typography variant="h5"
                                       className={classes.title}>{props.column.title}</Typography>
                    }
                    subheader={
                        <Typography className={classes.subHeader} variant="body2"
                        >{props.column.secondaryText}</Typography>}

                />
                <CardContent>
                    <CardMedia
                        className={classes.media}
                        image={props.column.image}
                        title='test'
                    />
                </CardContent>
            </Card>
            <Droppable droppableId={props.columnId} key={props.columnId}>
                {(provided, snapshot) => {
                    return (
                        <Paper className={classes.commentsSection} elevation={0}
                               {...provided.droppableProps}
                               ref={provided.innerRef}
                               style={{
                                   background: snapshot.isDraggingOver
                                       && indigo["50"]
                               }}
                        >
                            {props.column.comments.map((item, index) => {
                                if (editCommentIndex === item.id && openEditComment) {
                                    return (
                                        <DashboardAddEditComment
                                            key={item.id}
                                            color={item.color}
                                            commentMsg={commentMsg}
                                            changed={inputCommentHandler}
                                            confirm={submitCommentHandler}
                                            cancel={closeEditCommentHandler}
                                        />
                                    )
                                } else {
                                    return (
                                        <Draggable
                                            key={item.id}
                                            draggableId={item.id}
                                            index={index}
                                            isDragDisabled={!(props.currentUserId === item.creatorId || props.currentUserId === props.currentBoardCreatorId)}
                                        >
                                            {(provided, snapshot) => {
                                                return (
                                                    <DashboardComment
                                                        team={props.team}
                                                        reffer={provided.innerRef}
                                                        currentUserId={props.currentUserId}
                                                        openEdit={openEditCommentHandler}
                                                        deleteComment={props.deleteComment.bind(undefined, item.id, Number(props.columnId))}
                                                        openColorSwatch={openColorSwatchHandler}
                                                        submitColor={submitColorHandler}
                                                        upDownVote={upVoteDownVoteHandler}
                                                        onAddCommentLabels={addCommentLabelsHandler}
                                                        isColorSwatchOpen={openColorSwatch}
                                                        currentBoardCreatorId={props.currentBoardCreatorId}
                                                        draggableProps={provided.draggableProps}
                                                        handleProps={provided.dragHandleProps}
                                                        content={item}
                                                        progressIndex={props.progressIndex}
                                                        votesLeft={props.votesLeft}
                                                        votes={props.votes.filter(vote => vote.commentId === Number(item.id))}
                                                        labels={props.commentsLabels.filter(label => label.commentId === Number(item.id))}
                                                        deleteCommentLabel={props.deleteCommentLabel}
                                                        boardLabels={props.boardLabels}
                                                        areAnonymousComments={props.areAnonymousComments}
                                                    />
                                                );
                                            }}
                                        </Draggable>
                                    );
                                }

                            })}
                            {provided.placeholder}
                        </Paper>
                    );
                }}
            </Droppable>
            {props.children}
            {props.progressIndex < 2 ? openAddComment ?
                <DashboardAddEditComment commentMsg={commentMsg} changed={inputCommentHandler}
                                         confirm={submitCommentHandler} cancel={closeAddCommentHandler}/>
                :
                <Button onClick={openAddCommentHandler}
                        variant="outlined"
                        fullWidth
                        color="primary"
                        className={classes.button}
                        startIcon={<AddCommentIcon/>}
                >
                    Add Comment
                </Button>
                : null
            }
        </Paper>
    </Grid>)
}
export default DashboardCommentSection;