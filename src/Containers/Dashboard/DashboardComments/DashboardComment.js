import React, {useState} from 'react';
import {
    Avatar,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Chip,
    IconButton,
    Menu,
    MenuItem,
    Typography
} from "@material-ui/core";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import ColorLensIcon from '@material-ui/icons/ColorLens';
import ColorLensOutlinedIcon from '@material-ui/icons/ColorLensOutlined';
import {makeStyles} from "@material-ui/core/styles";
import * as moment from "moment";
import ColorSwitcher from "../../../Components/UI/ColorSwatcher/ColorSwatcher";
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import {commentsColors} from "../../../utilities/colorsMap";
import DashboardLabels from "../DashboardLabels/DashboardLabels";

const useStyles = makeStyles(theme => ({
    comment: {
        width: '100%',
        marginBottom: theme.spacing(1),
        minHeight: 40
    },
    commentAvatar: {
        width: theme.spacing(4),
        height: theme.spacing(4),
        backgroundColor: theme.palette.secondary.dark
    },
    date: {
        fontSize: '0.9em'
    },
    blur: {
        filter: "blur(4px)"
    }
}));
const DashboardComment = props => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const dateCreated = moment(props.content.dateCreated).format("Do MMM YY");
    const user = props.team && props.team.hasOwnProperty('users') ? props.team.users.find(user => user.id === props.content.creatorId) : null;
    const isUpvoted = props.votes && props.votes.findIndex(vote => vote.userId === props.currentUserId);

    const openMenuHandler = (ev) => {
        setAnchorEl(ev.currentTarget);
    }
    const closeMenuHandler = () => {
        setAnchorEl(null);
    };
    return (<Card className={classes.comment} elevation={0}
                  variant="outlined"
                  ref={props.reffer}
                  {...props.draggableProps}
                  {...props.handleProps}

    >
        <div className={[props.progressIndex===0 && !(props.currentUserId === props.content.creatorId) && classes.blur].join(' ')} style={{background: `${props.content.color ? props.content.color : '#F4F8FC'}`}}>
            <CardHeader
                avatar={
                   !props.areAnonymousComments ? <Avatar className={classes.commentAvatar}>
                        {user && `${user.firstName[0]}${user.lastName[0]}`}
                    </Avatar> : <Avatar className={classes.commentAvatar} />
                }
                title={!props.areAnonymousComments ? user && `${user.firstName} ${user.lastName}` :'Anonymous'}
                subheader={
                    <Typography className={classes.date} variant="body2"
                                color="textPrimary">{!props.content.columnTitle ? `Created: ${dateCreated}` : `Column: ${props.content.columnTitle}`}</Typography>
                }
                action={
                    ((props.currentBoardCreatorId === props.currentUserId || props.currentUserId === props.content.creatorId) && props.progressIndex < 4) &&
                    <React.Fragment>
                        <IconButton aria-label="settings" onClick={openMenuHandler}>
                            <MoreHorizIcon/>
                        </IconButton>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={closeMenuHandler}
                        >
                            <MenuItem onClick={props.openColorSwatch}>{!props.isColorSwatchOpen ? <ColorLensIcon/> :
                                <ColorLensOutlinedIcon/>}{!props.isColorSwatchOpen ? 'Open Palette' : 'Close Palette'}</MenuItem>
                            <MenuItem
                                onClick={props.openEdit.bind(undefined, props.content)}><EditIcon/> Edit</MenuItem>
                            <MenuItem onClick={props.deleteComment}><DeleteIcon color="error"/>Delete</MenuItem>
                            {props.isColorSwatchOpen && <MenuItem>
                                <ColorSwitcher
                                    changedColor={props.submitColor.bind(undefined, props.content)}
                                    colors={commentsColors}
                                />
                            </MenuItem>}
                        </Menu>

                    </React.Fragment>

                }
            />
            <CardContent> <Typography variant="body1">{props.content.message}</Typography></CardContent>
            {props.progressIndex > 1 &&
            <CardActions disableSpacing>
                <Chip
                    clickable
                    color="primary"
                    onClick={props.upDownVote.bind(undefined, isUpvoted, props.content.id, props.currentUserId, isUpvoted !== -1 && props.votes[isUpvoted].id)}
                    label={props.votes.length}
                    icon={isUpvoted !== -1 ? <ThumbUpIcon/> : <ThumbUpAltOutlinedIcon/>}
                    size="small"
                    disabled={props.currentUserId === props.content.creatorId || (props.votesLeft <= 0 && isUpvoted) || props.progressIndex > 2}
                />
            </CardActions>
            }
            {props.groupBy === 'votes' &&
            <CardActions disableSpacing>
                <Chip
                    clickable
                    color="primary"
                    label={props.content.votes}
                    icon={<ThumbUpIcon/>}
                    size="small"
                />
            </CardActions>
            }
            {
                props.progressIndex > 2 &&
                <CardActions>
                    <DashboardLabels
                        isCreator={props.currentUserId === props.currentBoardCreatorId}
                        labels={props.labels}
                        submit={props.onAddCommentLabels}
                        commentId={props.content.id}
                        delete={props.deleteCommentLabel}
                        type="addComment"
                        boardLabels={props.boardLabels && props.boardLabels.filter(label => !props.labels.some(lbl => lbl.labelId === label.id))}
                    />
                </CardActions>
            }
        </div>
    </Card>)
}
export default DashboardComment;