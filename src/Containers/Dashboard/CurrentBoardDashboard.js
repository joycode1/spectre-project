import React, {useEffect, useState} from "react";
import {
    Paper,
    Typography,
    Divider,
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import ProgressStepper from "../../Components/UI/Stepper/ProgressStepper";
import DashboardCommentsGrid from "./DashboardComments/DashboardCommentsGrid";
import {connect} from "react-redux";
import {
    alertError,
    changeCommentColor,
    deleteComment,
    editComment,
    fetchBoardData, setMaxVoteCount, setProgressIndex,
    removeVote,
    addComment,
    changeColumnIndex, addVote, addBoardlabel, deleteBoardLabel,
    addCommentLabels,
    deleteCommentLabel, addActionItem, editBoardName, setCommentsAnonymous,setUserMood
} from "../../store/actions/actionsMap";
import LikeCounter from "../../Components/UI/LikeCounter/LikeCounter";
import DashboardLabels from "./DashboardLabels/DashboardLabels";
import DashboardActionPlan from "./DashboardActionPlan/DashboardActionPlan";
import DashboardActionItemsSort from "./DashboardActionPlan/DashboardActionItemsSort";
import {currentBoardLock} from "../../store/actions/currentBoard";
import CheckIcon from '@material-ui/icons/Check';
import DashboardUpperHeader from "./DashboardUpperHeader";

const useStyles = makeStyles(theme => ({
    bottomHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: theme.spacing(1),
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
        }
    },
    boardComplete: {
        display: "flex",
        alignItems: 'center',
        justifyContent: 'flex-start'
    },

}));
const CurrentBoard = props => {
    const classes = useStyles();
    const teamData = props.teams && props.teamId && props.teams.find(team => team.id === props.teamId);
    const votesLeft = props.maxVoteCount - props.votes.filter(vote => vote.userId === props.userId).length;
    const currUserMood = props.userMood.find(mood => mood.userId === props.userId);
    const [groupBy, setGroupBy] = useState('labels');
    const [openBoardName, setEditBoardName] = useState(false);
    const [BoardNameEdit, setBoardName] = useState('');
    const [userMood, setUserMood] = useState(1);
    const {onFetchBoardData, currentBoardId} = props;
    useEffect(() => {
        onFetchBoardData(currentBoardId)
    }, [onFetchBoardData, currentBoardId])
    useEffect(() => {
        if (currUserMood) {
            setUserMood(currUserMood.mood)
        }else {
            setUserMood(1)
        }
    }, [currUserMood, setUserMood])
    const groupByHandler = (ev) => {
        setGroupBy(ev.target.value)
    }
    const onDragEndHandler = (result, columns) => {
        props.onChangeCommentIndex(result, columns)
    };
    const handleSetBoardEdit = () => {
        setEditBoardName(true);
        setBoardName(props.boardName);
    }
    const closeSetBoardEdit = () => {
        setEditBoardName(false);
        setBoardName('');
    }
    const changeBoardNameHandler = (ev) => {
        setBoardName(ev.target.value);
    }
    const submitChangeBoardNameHandler = () => {
        if (BoardNameEdit) {
            props.onEditBoardName(BoardNameEdit, props.boardId);
        } else {
            props.onErrorAlert('Board Name Cannot be an Empty String')
        }
        closeSetBoardEdit();
    }
    const submitBoardLabelHandler = (name, color, promptCallback) => {
        const findLabel = props.boardLabels.find(label => label.name === name);

        if (findLabel) {
            props.onErrorAlert('Label Already Exists');
        } else if (name === '') {
            props.onErrorAlert('Labels Must Have a Name');
        } else {
            props.onAddBoardLabel(props.currentBoardId, name, color);
        }
        promptCallback();
    }
    const submitActionItemHandler = (message, assignedTo, status, promptCallback) => {
        if (!message) {
            props.onErrorAlert('Action Item Must Have a Message');
        } else {
            props.onAddActionItem(message, assignedTo, status, props.boardId, props.teamId)
        }
        promptCallback();
    }
    const lockBoardHandler = (promptCallback) => {
        props.onBoardLock(props.boardId);
        promptCallback();
    }
    const changeAnonymousCommentsHandler = () => {
        props.onSetAnonymousComments(!props.areAnonymousComments, props.boardId);
    }
    const changeUserMoodHandler = (ev, val) => {
        setUserMood(val)
    }
    const submitUserMoodHandler = (ev, val) => {
        props.onSetUserMood(props.userMood, props.boardId, props.userId, val);
    }

    return (
        <React.Fragment>
            <Paper>
                <DashboardUpperHeader
                    boardName={props.boardName}
                    template={props.template}
                    isCreator={props.userId === props.creatorId}
                    handleSetBoardEdit={handleSetBoardEdit}
                    openBoardName={openBoardName}
                    BoardNameEdit={BoardNameEdit}
                    changeBoardNameHandler={changeBoardNameHandler}
                    submitChangeBoardNameHandler={submitChangeBoardNameHandler}
                    closeSetBoardEdit={closeSetBoardEdit}
                    dateCreated={props.dateCreated}
                    isBoardLocked={props.isBoardLocked}
                    onErrorAlert={props.onErrorAlert.bind(undefined, 'Time is up!')}
                    areAnonymousComments={props.areAnonymousComments}
                    changeAnonymousCommentsHandler={changeAnonymousCommentsHandler}
                    teamData={teamData}
                    loading={props.loading}
                    creatorId={props.creatorId}
                    progressIndex={props.progressIndex}
                />
                <Divider/>
                <div className={classes.bottomHeader}>
                    {!props.isBoardLocked && <ProgressStepper progressIndex={props.progressIndex}
                                                              setIndex={props.onSetProgressIndex}
                                                              lockBoard={lockBoardHandler}
                                                              boardId={props.boardId}
                                                              creatorId={props.creatorId}
                                                              currentUserId={props.userId}
                    />}
                    {props.progressIndex === 2 &&
                    <LikeCounter maxCount={props.maxVoteCount}
                                 votesLeft={votesLeft}
                                 setMaxVoteCount={props.onSetMaxVoteCount}
                                 boardId={props.boardId}
                                 creatorId={props.creatorId}
                                 currentUserId={props.userId}
                    />
                    }
                    {props.progressIndex === 3 && <DashboardLabels
                        isCreator={props.userId === props.creatorId}
                        labels={props.boardLabels}
                        submit={submitBoardLabelHandler}
                        delete={props.onDeleteBoardLabel}
                        type='addBoard'
                    />}
                    {(props.progressIndex === 4) &&
                    <DashboardActionItemsSort groupBy={groupBy} groupByHandler={groupByHandler} team={teamData}
                                              isBoardLocked={props.isBoardLocked}
                                              submit={submitActionItemHandler}/>}
                    {props.isBoardLocked &&
                    <Typography variant="h5" align="center" className={classes.boardComplete}>
                        <CheckIcon fontSize="large" color="secondary"/>
                        Retrospective is complete</Typography>
                    }
                </div>
            </Paper>
            {props.progressIndex < 4 ?
                <DashboardCommentsGrid sections={props.columns} dragEnd={onDragEndHandler} team={teamData}
                                       addComment={props.onAddComment}
                                       editComment={props.onEditComment}
                                       deleteComment={props.onDeleteComment}
                                       changeCommentColor={props.onChangeCommentColor}
                                       removeVote={props.onRemoveVote}
                                       addVote={props.onAddVote}
                                       commentsLabels={props.commentsLabels}
                                       addCommentLabels={props.onAddCommentLabels}
                                       deleteCommentLabel={props.onDeleteCommentLabel}
                                       boardLabels={props.boardLabels}
                                       currentUserId={props.userId}
                                       currentBoardId={props.currentBoardId}
                                       currentBoardCreatorId={props.creatorId}
                                       progressIndex={props.progressIndex}
                                       votes={props.votes}
                                       template={props.template}
                                       areAnonymousComments={props.areAnonymousComments}
                                       votesLeft={votesLeft}
                />
                : <DashboardActionPlan
                    groupBy={groupBy}
                    commentsLabels={props.commentsLabels}
                    boardLabels={props.boardLabels}
                    votes={props.votes}
                    columns={props.columns}
                    team={teamData}
                    actionItems={props.actionItems}
                    areAnonymousComments={props.areAnonymousComments}
                    userMood={userMood}
                    userId={props.userId}
                    changeUserMoodHandler={changeUserMoodHandler}
                    submitUserMoodHandler={submitUserMoodHandler}
                    isBoardLocked={props.isBoardLocked}
                />}
        </React.Fragment>

    )
}
const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        currentBoardId: state.auth.currentBoard,
        teams: state.teams.teams,
        boardId: state.currBoard.boardId,
        creatorId: state.currBoard.creatorId,
        teamId: state.currBoard.teamId,
        maxVoteCount: state.currBoard.maxVoteCount,
        template: state.currBoard.template,
        boardName: state.currBoard.boardName,
        dateCreated: state.currBoard.dateCreated,
        dateEdited: state.currBoard.dateEdited,
        progressIndex: state.currBoard.progressIndex,
        columns: state.currBoard.columns,
        votes: state.currBoard.votes,
        boardLabels: state.currBoard.labels,
        actionItems: state.currBoard.actionItems,
        isBoardLocked: state.currBoard.isBoardLocked,
        areAnonymousComments: state.currBoard.areAnonymousComments,
        commentsLabels: state.currBoard.commentsLabels,
        loading: state.currBoard.loading,
        userMood: state.currBoard.userMood
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchBoardData: (boardId) => dispatch(fetchBoardData(boardId)),
        onChangeCommentIndex: (result, columns) => dispatch(changeColumnIndex(result, columns)),
        onAddComment: (message, boardId, creatorId, boardIndex) => dispatch(addComment(message, boardId, creatorId, boardIndex)),
        onEditComment: (message, commentId, boardIndex) => dispatch(editComment(message, commentId, boardIndex)),
        onChangeCommentColor: (color, commentId, boardIndex) => dispatch(changeCommentColor(color, commentId, boardIndex)),
        onDeleteComment: (commentId, boardIndex) => dispatch(deleteComment(commentId, boardIndex)),
        onSetProgressIndex: (progressIndex, boardId) => dispatch(setProgressIndex(progressIndex, boardId)),
        onSetMaxVoteCount: (maxVoteCount, boardId) => dispatch(setMaxVoteCount(maxVoteCount, boardId)),
        onRemoveVote: (voteId) => dispatch(removeVote(voteId)),
        onAddVote: (commentId, userId) => dispatch(addVote(commentId, userId)),
        onAddBoardLabel: (boardId, name, color) => dispatch(addBoardlabel(boardId, name, color)),
        onErrorAlert: (msg) => dispatch(alertError(msg)),
        onDeleteBoardLabel: (id) => dispatch(deleteBoardLabel(id)),
        onAddCommentLabels: (labels, commentId) => dispatch(addCommentLabels(labels, commentId)),
        onDeleteCommentLabel: (labelId) => dispatch(deleteCommentLabel(labelId)),
        onAddActionItem: (message, assignedTo, status, boardId, teamId) => dispatch(addActionItem(message, assignedTo, status, boardId, teamId)),
        onBoardLock: (boardId) => dispatch(currentBoardLock(boardId)),
        onEditBoardName: (name, boardId) => dispatch(editBoardName(name, boardId)),
        onSetAnonymousComments: (areAnonymousComments, boardId) => dispatch(setCommentsAnonymous(areAnonymousComments, boardId)),
        onSetUserMood: (usersMood, boardId, userId, mood) => dispatch(setUserMood(usersMood, boardId, userId, mood))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CurrentBoard);