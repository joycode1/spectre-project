import React from 'react';
import EmptyDashboard from "./EmptyDashboard";
import CurrentBoardDashboard from "./CurrentBoardDashboard";
import {connect} from "react-redux";



const Dashboard = props => {

    return props.hasCurrentBoard ? <CurrentBoardDashboard/> : <EmptyDashboard/>

};
const mapStateToProps = state => {
    return {
        hasCurrentBoard: state.auth.currentBoard
    }
};
export default connect(mapStateToProps, null)(Dashboard);