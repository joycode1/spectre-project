import React, {useState} from "react";
import HorizontalTabBar from "../../../Components/UI/HorizontalTabBar/HorizontalTabBar";
import ActionItemPanel from "./ActionItemPanel";
import AddEditActionItem from "../../Dashboard/DashboardActionPlan/AddEditActionItem";


const ActionItemsPanel = props => {
    const [currentTabIndex, setCurrentTabIndex] = useState(0);
    const [open, setOpen] = useState(false);
    const [ActionItemForm, setActionItemForm] = useState({
        message: '',
        assignedTo: 'team',
        status: 'todo',
        id:''
    });
    const handleEditChange = (type, ev) => {
        setActionItemForm({...ActionItemForm, [type]: ev.target.value})
    }
    const closeHandleEditPrompt = () => {
        setOpen(!open)
        setActionItemForm({
            message: '',
            assignedTo: 'team',
            status: 'todo',
            id:''
        })
    }
    const openHandleEditPrompt = (actionItem, ev) => {
        setOpen(true)
        setActionItemForm({
            message: actionItem.message,
            assignedTo: actionItem.assignedTo,
            status: actionItem.status,
            id:actionItem.id
        })
    }
    const handleChange = (event, newValue) => {
        setCurrentTabIndex(newValue);
    };
    const actionItemsFilter = (status) => {
        return props.actionItems.filter(actionItem => actionItem.status === status)
    };
    const submitEditHandler = ()=>{
        if(ActionItemForm.message){
            props.editActionItem(props.team.id, ActionItemForm.id, ActionItemForm.message, ActionItemForm.status, ActionItemForm.assignedTo)
        }
        closeHandleEditPrompt();
    }
    return (
        <React.Fragment>
            <HorizontalTabBar value={currentTabIndex}
                              changed={handleChange}/>
            {props.actionItems &&
            ['todo', 'inProgress', 'done'].map((status, i) => <ActionItemPanel value={currentTabIndex} index={i}
                                                                               key={i}
                                                                               team={props.team}
                                                                               boards={props.boards}
                                                                               isTeamCreator={props.isTeamCreator}
                                                                               currentUser={props.currentUser}
                                                                               deleteActionItem={props.deleteActionItem}
                                                                               openHandleEditPrompt={openHandleEditPrompt}
                                                                               actionItems={actionItemsFilter(status)}/>)
            }
            <AddEditActionItem
                submit={submitEditHandler}
                assignedTo={ActionItemForm.assignedTo}
                open={open}
                message={ActionItemForm.message}
                status={ActionItemForm.status}
                handlePrompt={closeHandleEditPrompt}
                handleChange={handleEditChange}
                team={props.team}
            />
        </React.Fragment>
    )
}
export default ActionItemsPanel;