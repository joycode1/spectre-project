import React from 'react';
import {Card, CardContent, CardHeader, IconButton, Typography,Chip} from "@material-ui/core";
import DescriptionIcon from "@material-ui/icons/Description";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import * as moment from "moment";



const ActionItem = props =>{
    const dateCreated = props.dateCreated && moment(props.dateCreated).format('Do MMM YY');
    const assignedTo = props.assignedTo==='team' ? 'Team' : props.team && props.team.hasOwnProperty('users') ? props.team.users.filter(user => user.id === props.assignedTo).map(user=>`${user.firstName} ${user.lastName}`) : null

    return <React.Fragment>
    <Card
        elevation={0}
    >

        <CardHeader
            avatar={<DescriptionIcon fontSize="large" color="primary"/>}
            action={
                <React.Fragment>
                    {(props.currentUser===props.assignedTo || props.assignedTo==='team') &&
                    <Chip size="small"  color="secondary"
                          label="You are assigned"/>

                    }
                    {props.isTeamCreator && <React.Fragment>
                        <IconButton size="small" onClick={props.openHandleEditPrompt}>
                            <EditIcon color="primary"/>
                        </IconButton>
                        <IconButton size="small" onClick={props.deleteActionItem}>
                            <DeleteIcon color="error"/>
                        </IconButton>
                    </React.Fragment>
                    }
                </React.Fragment>
            }
            title={<Typography variant="h6" component="p">
                {props.message}
            </Typography>}
            subheader={
                <Typography variant="body2" color="textSecondary" component="p">
                    {dateCreated}
                </Typography>
            }
        />
        <CardContent>
            <Typography variant="body1" component="p">
                {props.board.boardName}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
                {assignedTo}
            </Typography>
        </CardContent>
    </Card>

    </React.Fragment>
}

export default ActionItem;