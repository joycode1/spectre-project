import React from 'react';
import {Divider} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import ActionItem from "./ActionItem";

const useStyles = makeStyles(theme => ({
    root: {
        minHeight: 150,
        padding: theme.spacing(2)
    }
}));

const ActionItemPanel = props => {
    const {value, index} = props;
    const classes = useStyles();
    return (
        <React.Fragment>
            {value === index && (
                <div className={classes.root}>
                    {props.actionItems && props.actionItems.map(actionItem =>
                        <React.Fragment key={actionItem.id}>
                            <ActionItem
                                message={actionItem.message}
                                dateCreated={actionItem.dateCreated}
                                assignedTo={actionItem.assignedTo}
                                team={props.team}
                                status={actionItem.status}
                                board={props.boards.find(board => board.id === actionItem.boardId)}
                                isTeamCreator={props.isTeamCreator}
                                currentUser={props.currentUser}
                                deleteActionItem={props.deleteActionItem.bind(undefined,props.team.id,actionItem.id)}
                                openHandleEditPrompt={props.openHandleEditPrompt.bind(undefined,actionItem)}
                            />
                            <Divider variant="fullWidth"/>

                        </React.Fragment>
                    )
                    }
                </div>

            )}
        </React.Fragment>
    )


}

export default ActionItemPanel;