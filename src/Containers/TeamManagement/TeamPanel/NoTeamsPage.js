import React from 'react';
import {makeStyles, Container, Typography} from "@material-ui/core";
import background from '../../../assets/images/noTeamPage.png';

const useStyle = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    background: {
        width: '100%',
        height: 'auto',
        margin: '0 auto',
        maxWidth: '600px'
    },
    title: {
        fontFamily: 'Shrikhand, cursive'
    }
}));

const NoTeamsPage = props => {
    const classes = useStyle();
    return (
        <Container className={classes.root}>
            <Typography variant="h6" className={classes.title} component="p">
                No active teams. Create your first team...
            </Typography>
            <div><img alt='' className={classes.background} src={background}/></div>
        </Container>
    );
}
export default NoTeamsPage;