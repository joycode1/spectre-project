import React from "react";
import {
    Paper,
    Typography,
    makeStyles,
    IconButton,
    List,
    Avatar,
    Chip,
    Button,
    Tooltip
} from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PeopleIcon from '@material-ui/icons/People';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import TeamMember from "./TeamMember";
import PopperMenu from "../../../Components/UI/Popper/PopperMenu";
import AddParticipantFilter from "../SearchUser/SearchUser";
import defaultTeam from '../../../assets/images/default_team.png';
import Prompt from "../../../Components/UI/Prompt/Prompt";
import DescriptionIcon from '@material-ui/icons/Description';
import ActionItemsPanel from "../ActionItemsPanel/ActionItemsPanel";
import BoardCards from "../../Boards/BoardList/BoardCards";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
    },
    paper: {
        width: '100%',
        height: 'auto',
        padding: theme.spacing(1),
        marginBottom: theme.spacing(2)
    },
    teamInfo: {
        display: 'flex',
        justifyContent: 'space-between',

    },
    infoText: {
        color: theme.palette.text.secondary
    },
    title: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        fontWeight: 'bold'

    },
    actions: {
        alignSelf: 'flex-start',

    },
    actionIcon: {
        padding: 0
    },
    membersTop: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    teamName: {
        fontFamily: 'Shrikhand, cursive'
    },
    teamAvatar: {
        width: theme.spacing(10),
        height: theme.spacing(10),
        marginRight: theme.spacing(1)
    },
    chip: {
        marginRight: theme.spacing(1),
    },
    blur: {
        filter: 'blur(8px)'
    },
    teamTitle: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    leaveButton: {
        color: theme.palette.error.main,
        borderColor: theme.palette.error.main
    }

}))
const TeamPanel = props => {
    const {value, index} = props;
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [delOpen, setDelOpen] = React.useState(false);
    const handlePrompt = () => {
        setOpen(!open);
    };

    const handleDeletePrompt = () => {
        setDelOpen(!delOpen);
    }
    const members = !props.loading ? props.team.users.map((user, i) => <TeamMember key={i} firstName={user.firstName}
                                                                                   lastName={user.lastName}
                                                                                   isCreator={props.team.creatorId === user.id}
                                                                                   leaveTeamHandler={props.removedFromTeam.bind(undefined, user.id, props.team.id, props.team.users, true,undefined)}
                                                                                   isCurrentUserCreator={props.team.creatorId === props.currentUser}
                                                                                   status={user.status}
                                                                                   email={user.email}/>) :
        <TeamMember key={0} firstName=''
                    lastName=''
                    isCreator=''
                    email=''/>;
    const membersLength = !props.loading ? props.team.users.filter(user => !user.status).length : 0;
    const pendingLength = !props.loading ? props.team.users.filter(user => user.status).length : 0;
    return (
        <React.Fragment>
            {value === index && (
                <div className={classes.root}>
                    <Paper className={classes.paper}>
                        <div className={classes.teamInfo}>
                            <div>
                                <div className={classes.teamTitle}>
                                    <Avatar className={[props.loading && classes.blur, classes.teamAvatar].join(' ')}
                                            variant="rounded"
                                            src={props.team && props.team.avatar ? props.team.avatar : defaultTeam}/>
                                    <div><Typography variant="h5"
                                                     className={[props.loading && classes.blur, classes.teamName].join(' ')}>{props.team ? props.team.teamName : 'Loading'}</Typography>
                                        <Typography variant="body1"
                                                    className={[props.loading && classes.blur].join(' ')}>{props.team ? props.team.description : 'Loading'}</Typography>
                                    </div>
                                </div>

                                <Typography variant="caption"
                                            className={classes.infoText}>{props.team ? membersLength : 0} {props.team && membersLength === 1 ? 'member' : 'members'} • {props.team ? pendingLength : 0} {props.team && pendingLength === 1 ? 'pending invitation' : 'pending invitations'} • {props.team ? props.team.boards.length : 0} {props.team && props.team.boards.length === 1 ? 'retrospective' : 'retrospectives'}</Typography>
                            </div>
                            <div className={classes.actions}>
                                {props.isTeamCreator && <Chip size="small" className={classes.chip} color="secondary"
                                                              label="You are the creator"/>}
                                {props.isTeamCreator ?
                                    <React.Fragment>
                                        <Tooltip title="Edit Team" aria-label="edit">
                                            <IconButton color="primary"
                                                        className={[props.loading && classes.blur, classes.actionIcon].join(' ')}
                                                        onClick={props.team && props.editTeamHandler.bind(undefined, props.team.id)}>
                                                <EditIcon/>
                                            </IconButton>
                                        </Tooltip>
                                        <Tooltip title="Delete Team" aria-label="delete">
                                            <IconButton
                                                className={[props.loading && classes.blur, classes.actionIcon].join(' ')}
                                                onClick={handleDeletePrompt}>
                                                <DeleteIcon/>
                                                {props.team && <Prompt open={delOpen} close={handleDeletePrompt}
                                                                       message={`Are you sure you want to delete ${props.team.teamName}?`}
                                                                       accept={props.deleteTeamHandler.bind(undefined,props.team.id,handleDeletePrompt)}/>}
                                            </IconButton>
                                        </Tooltip>
                                    </React.Fragment> :
                                    <React.Fragment>
                                        <Button
                                            variant="outlined"
                                            disableElevation
                                            className={[props.loading && classes.blur, classes.leaveButton].join(' ')}
                                            size="small"
                                            onClick={handlePrompt}
                                            endIcon={<ExitToAppIcon/>}
                                        >
                                            Leave Team
                                        </Button>
                                        {props.team && <Prompt open={open} close={handlePrompt}
                                                               message={`Are you sure you want to leave ${props.team.teamName}?`}
                                                               accept={props.leaveTeamHandler.bind(undefined,props.currentUser, props.team.id, props.team.users, false,handlePrompt)}/>}
                                    </React.Fragment>
                                }

                            </div>
                        </div>
                    </Paper>

                    <Paper className={classes.paper}>
                        <div className={classes.membersTop}>
                            <Typography variant="h6" className={classes.title}> <PeopleIcon
                                color="primary"/>Members</Typography>
                            {props.isTeamCreator && <div className={classes.actions}>
                                <PopperMenu btnClass={classes.actionIcon} btnType={IconButton} color="primary"
                                            icon={PersonAddIcon}>
                                    <AddParticipantFilter value={props.addParticipantInput}
                                                          users={props.users}
                                                          currentTeam={props.team}
                                    />
                                </PopperMenu>

                            </div>}
                        </div>
                        <List className={classes.root}>
                            {members}
                        </List>
                    </Paper>
                    <Paper className={classes.paper}>
                        <Typography variant="h6" className={classes.title}> <DescriptionIcon
                            color="primary"/>Action Items</Typography>
                        <ActionItemsPanel actionItems={props.actionItems} team={props.team}
                                          boards={props.boards}
                                          isTeamCreator={props.isTeamCreator}
                                          currentUser={props.currentUser}
                                          editActionItem={props.editActionItem}
                                          deleteActionItem={props.deleteActionItem}
                        />
                    </Paper>
                    <React.Fragment>
                        <Paper className={classes.paper}>
                            <Typography variant="h6" className={classes.title}> <DashboardIcon
                                color="primary"/>Boards</Typography>
                        </Paper>
                        {props.boards &&
                        <BoardCards boards={props.boards} searchTerm={''}
                                    onDeleteBoard={props.deleteBoard}
                                    userId={props.currentUser}
                                    onAlertError={props.alertError}
                                    onSetCurrentBoard={props.viewBoard}/>
                        }
                    </React.Fragment>
                </div>

            )}
        </React.Fragment>
    );

}

export default TeamPanel;