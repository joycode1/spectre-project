import React from "react";
import {Avatar, IconButton, ListItem, ListItemAvatar, ListItemText,ListItemSecondaryAction} from "@material-ui/core";
import ClearIcon from '@material-ui/icons/Clear';
import StarIcon from '@material-ui/icons/Star';
import PersonIcon from '@material-ui/icons/Person';
import {makeStyles} from "@material-ui/core/styles";
import Prompt from "../../../Components/UI/Prompt/Prompt";
const useStyles = makeStyles(theme=>({
    blur:{
        filter:'blur(8px)'
    },
    textPending:{
        fontStyle:'italic',
    },
    member:{
        textAlign:'left'
    }
}));
let randomMC = require('random-material-color');
const TeamMember = React.memo(props => {
    const classes=useStyles();
    const [open, setOpen] = React.useState(false);

    const handlePrompt = () => {
        setOpen(!open);
    };

    return (
        <ListItem>
            <ListItemAvatar>
                <Avatar className={[!props.firstName && classes.blur].join(' ')} style={{backgroundColor: randomMC.getColor()}}>
                    {props.firstName && `${props.firstName[0]}${props.lastName[0]}`}
                </Avatar>
            </ListItemAvatar>
            <ListItemText className={[!props.firstName && classes.blur].join(' ')} primary={props.firstName && `${props.firstName} ${props.lastName}`} secondary={props.email}/>
            <ListItemText className={[!props.firstName && classes.blur, props.status && classes.textPending,classes.member].join(' ')} secondary={props.isCreator && !props.status ? 'Creator' : props.status ? 'Pending Invitation' : 'Team Member'}
                         secondaryTypographyProps={{variant: "subtitle2",color:"textPrimary",align:"center"}}
            />
                <ListItemSecondaryAction>
                    {props.isCreator ?
                        <IconButton>
                            <StarIcon style={{color:'#FFDF00'}}/>
                        </IconButton> : props.isCurrentUserCreator ?
                    <IconButton onClick={handlePrompt}>
                        <ClearIcon color="error"/>
                    </IconButton> :
                            <IconButton>
                                <PersonIcon color="inherit" className={[!props.firstName && classes.blur].join(' ')}/>
                            </IconButton>
                    }
                    <Prompt open={open} close={handlePrompt} message="Are you sure you want to remove this user?" accept={props.leaveTeamHandler}/>
                </ListItemSecondaryAction>

        </ListItem>
    );
});
export default TeamMember;