import React from 'react';
import AddEditTeam from "../AddEditTeam/AddEditTeam";

const EditTeamDrawer = props =>{

    return <AddEditTeam close={props.close} teamId={props.teamId}/>
};

export default EditTeamDrawer;