import React from 'react';
import AddEditTeam from "../AddEditTeam/AddEditTeam";

const CreateTeamDrawer = props => {
  return <AddEditTeam close={props.close} changeTabIndex={props.changeTabIndex}/>
};

export default CreateTeamDrawer;