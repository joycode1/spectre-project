import React, {useState} from 'react';
import {fade, makeStyles} from '@material-ui/core/styles';
import {List, ListItem, ListItemText, InputBase, ListItemAvatar, Avatar, IconButton} from '@material-ui/core';
import {connect} from 'react-redux';
import AddIcon from '@material-ui/icons/Add';
import {filterUsers,inviteUser} from "../../../store/actions/actionsMap";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        '&:hover': {
            backgroundColor: fade(theme.palette.primary.light, 0.25),
        },
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: 'auto',
        },
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    blur: {
        filter: 'blur(8px)'
    }
}));

const AddParticipantFilter = props => {
    const classes = useStyles();
    const [userInput, setUserInputValue] = useState('');
    let timer = null;
    const searchUserHandler = (ev) => {
        clearTimeout(timer);
        ev.persist();
        setUserInputValue(ev.target.value);
        timer = setTimeout(search, 300);

        function search() {
            props.onFilterUsers(ev.target.value, props.currentTeam.users);
        }
    };

    const inviteUserHandler = (user, teamId, ev) => {
        ev.preventDefault();
        props.onInviteUser(user, teamId);
    }
    let usersMap = props.users && props.users.map((user, i) => {
        return <ListItem key={i}>
            <ListItemAvatar>
                <Avatar className={[!user.firstName && classes.blur].join(' ')}>
                    {user.firstName && `${user.firstName[0]}${user.lastName[0]}`}
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={user.firstName && `${user.firstName} ${user.lastName}`} secondary={user.email}/>
            <IconButton onClick={inviteUserHandler.bind(undefined, user, props.currentTeam.id)}>
                <AddIcon color="secondary"/>
            </IconButton>
        </ListItem>
    });
    return (
        <List>
            <ListItem>
                <div className={classes.search}>
                    <InputBase
                        placeholder="Search email…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        inputProps={{'aria-label': 'search'}}
                        onChange={searchUserHandler}
                        value={userInput}
                    />
                </div>
            </ListItem>
            {usersMap}
        </List>
    );
};
const mapStateToProps = state => {
    return {
        users: state.userSearch.users
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onFilterUsers: (value, currentUsers) => dispatch(filterUsers(value, currentUsers)),
        onInviteUser: (user, teamId) => dispatch(inviteUser(user, teamId))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddParticipantFilter);