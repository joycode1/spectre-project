import React, {useEffect, useState} from 'react';
import {Avatar, Button, Grid, Badge, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import InputBuilder from "../../InputBuilder/InputBuilder";
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import {connect} from "react-redux";
import {createTeam, editTeam} from "../../../store/actions/actionsMap";
import defaultTeam from '../../../assets/images/default_team.png';
import createTeamImg from '../../../assets/images/create_team.jpg';



const useStyles = makeStyles(theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(3)
    },
    avatar: {
        margin: theme.spacing(2),
        backgroundColor: theme.palette.secondary.main,
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    title: {
        textTransform: 'uppercase',
        fontFamily: 'Shrikhand,cursive'
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
        alignSelf: 'center'
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    errorMsg: {
        paddingTop: theme.spacing(2)
    },
    teamAvatar: {
        width: theme.spacing(12),
        height: theme.spacing(12),
    },
    imgRoot:{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    img:{
        width: '100%',
        height: 'auto',
        margin: '0 auto',
        maxWidth: '350px'
    }
}));
const CreateTeamDrawer = props => {
    const [isFormValid, setFormValidity] = useState(false);
    const [TeamForm, setTeamForm] = useState({
            teamName: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    label: 'Team Name',
                    variant: 'standard'
                },
                validation: {
                    required: true,
                    minLength: 3
                },
                value: '',
                itemSize: 12,
                valid: false,
                isTouched: false,
                errorMsg: ''
            },
            description: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    label: 'Description',
                    multiline: true,
                    rows: "{5}",
                    variant: 'standard',
                    required: false
                },
                validation: {},
                value: '',
                itemSize: 12,
                valid: true,
                isTouched: false,
                errorMsg: ''
            }
        }
    );
    useEffect(()=>{
        if(props.teamId){
            const teamIndex = props.teams.findIndex(team => team.id===props.teamId);
            const form = {...TeamForm};
            form.teamName.value=props.teams[teamIndex].teamName;
            form.description.value=props.teams[teamIndex].description;
            form.teamName.valid=true;
            form.description.valid=true;
            setFormValidity(true);
            setTeamAvatar(props.teams[teamIndex].avatar);
            setTeamForm(form);
        }
    },[]);
    const [teamAvatar, setTeamAvatar] = useState('');
    const classes = useStyles();
    const myCallback = (formIsValid, form) => {
        setFormValidity(formIsValid);
        setTeamForm(form);
    };
    const uploadAvatarHandler = async (ev) => {
        if (ev.target.files[0].type.includes('image')) {
            const res = await fileToB64(ev.target.files[0]);
            setTeamAvatar(res);
        }
    };
    const submitTeamHandler = (ev) => {
        ev.preventDefault();
        if (isFormValid && !props.teamId) {
            props.onCreateTeam(TeamForm.teamName.value, TeamForm.description.value, teamAvatar, props.userId);
            props.close(ev);
            props.changeTabIndex(ev, 0);
        }else if(isFormValid && props.teamId){
            props.onEditTeam(props.teamId,TeamForm.teamName.value, TeamForm.description.value, teamAvatar);
            props.close(ev);
        }
    };

    function fileToB64(file) {
        return new Promise(resolve => {
            let reader = new FileReader();
            reader.onload = function (event) {
                resolve(event.target.result);
            };
            reader.readAsDataURL(file);
        });
    }

    return (
        <div className={classes.paper}>
            <Avatar className={classes.avatar}>
                <GroupAddIcon/>
            </Avatar>
            <Typography component="h1" variant="h6" className={classes.title}>
                {props.teamId ? 'Edit Team Details' : 'Create New Team'}
            </Typography>
            <form className={classes.form} onSubmit={submitTeamHandler} noValidate>
                <Grid container spacing={2}>
                    <Grid item xs={12} align='center'>
                        <input
                            accept="image/*"
                            className={classes.input}
                            style={{display: 'none'}}
                            id="avatar-input"
                            type="file"
                            onChange={uploadAvatarHandler}
                        />
                        <label htmlFor="avatar-input">
                            <Badge
                                overlap="circle"
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                style={{cursor: 'pointer'}}
                                badgeContent={<AddAPhotoIcon/>}
                            >
                                <Avatar variant="rounded" className={classes.teamAvatar}
                                        src={teamAvatar ? teamAvatar : defaultTeam}/>
                            </Badge>
                        </label>
                    </Grid>
                    <InputBuilder
                        formInputs={TeamForm}
                        callbackFromParent={myCallback}
                    />
                    <Grid item xs={6}>
                        <Button
                            type='submit'
                            fullWidth
                            variant="contained"
                            color="primary"
                            size="large"
                            className={classes.submit}>
                            {props.teamId ? 'Edit' : 'Create'}
                        </Button>
                    </Grid>
                    <Grid item xs={6}>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            size="large"
                            className={classes.submit}
                            onClick={props.close}>
                            Cancel
                        </Button>
                    </Grid>
                </Grid>
            </form>
            <div className={classes.imgRoot} >
                <img className={classes.img} src={createTeamImg} alt=""/>
            </div>
        </div>
    )
};
const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        teams:state.teams.teams
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onCreateTeam: (name, description, avatar, userId) => dispatch(createTeam(name, description, avatar, userId)),
        onEditTeam:(teamId,name,description,avatar) => dispatch(editTeam(teamId,name,description,avatar))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateTeamDrawer);