import React, {useEffect, useState} from 'react';
import {Container, makeStyles} from "@material-ui/core";
import VerticalTabBar from "../../Components/UI/VerticalTabBar/VerticalTabBar";
import TeamPanel from "./TeamPanel/TeamPanel";
import SideDrawer from "../../Components/UI/SiderDrawer/SideDrawer";
import CreateTeamDrawer from "./CreateTeam/CreateTeamDrawer";
import NoTeamsPage from "./TeamPanel/NoTeamsPage";
import {connect} from "react-redux";
import {
    fetchTeams,
    deleteTeam,
    removeUser,
    teamsBoardDelete,
    alertError,
    setCurrentBoard, teamActionItemEdit, teamActionItemDelete
} from "../../store/actions/actionsMap";
import EditTeamDrawer from "./EditTeam/EditTeamDrawer";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
    paper: {
        height: '100%'
    }
}));

const TeamManagement = props => {
    const classes = useStyles();
    const [currentTabIndex, setCurrentTabIndex] = useState(0);
    const [openDrawer, setOpenDrawer] = useState(false);
    const [editTeam, setEditTeam] = useState('');
    const {onFetchTeamsData, userId, teams} = props;
    useEffect(() => {
        onFetchTeamsData(userId);
    }, [onFetchTeamsData, userId]);

    const handleChange = (event, newValue) => {
        setCurrentTabIndex(newValue);
    };
    const deleteTeamHandler = (teamId, promptCallback, ev) => {
        ev.preventDefault();
        props.onDeleteTeam(teamId);
        const prevTeam = teams.findIndex(team => team.id === teamId) - 1;
        handleChange(ev, prevTeam !== -1 ? prevTeam : 0);
        promptCallback();
    }
    const leaveTeamHandler = (userId, teamId, users, isCreator, promptCallback, ev) => {
        props.onLeaveTeam(userId, teamId, users, isCreator);
        const findTeamIndex = props.teams.findIndex(team => team.id === teamId);
        if (!isCreator) {
            handleChange(ev, findTeamIndex - 1 !== -1 ? findTeamIndex - 1 : 0);
        }
        if (promptCallback) {

            promptCallback()
        }
    }
    const toggleDrawer = (ev) => {
        ev.preventDefault();
        setOpenDrawer(!openDrawer);
        if (openDrawer === true) {
            setEditTeam('');
        }
    };
    const openEditHandler = (teamId, ev) => {
        setEditTeam(teamId);
        toggleDrawer(ev);
    }
    let teamPanels = [];
    let teamNames = teams.length > 0 && teams.map(team => team.teamName);
    if (props.loading && teams.length === 0) {
        teamPanels.push(<TeamPanel value={currentTabIndex} key={0} index={0} team={null}
                                   loading={props.loading}
                                   editTeamHandler={openEditHandler}
                                   leaveTeamHandler={leaveTeamHandler.bind(undefined)}
                                   deleteTeamHandler={deleteTeamHandler.bind(undefined)}
        />)
    } else if (teams.length > 0) {
        teams.forEach((team, i) => {
            teamPanels.push(<TeamPanel value={currentTabIndex} key={i} index={i} team={team}
                                       boards={props.boards.filter(board => board.teamId === team.id)}
                                       loading={props.loading}
                                       currentUser={props.userId}
                                       deleteTeamHandler={deleteTeamHandler}
                                       leaveTeamHandler={leaveTeamHandler}
                                       removedFromTeam={leaveTeamHandler}
                                       editTeamHandler={openEditHandler}
                                       editActionItem={props.onEditActionItem}
                                       deleteActionItem={props.onTeamActionItemDelete}
                                       alertError={props.onAlertError}
                                       deleteBoard={props.onDeleteBoard}
                                       viewBoard={props.onSetCurrentBoard}
                                       actionItems={props.actionItems[team.id]}
                                       isTeamCreator={team.creatorId === props.userId}/>)
        });
    } else if (!props.loading && teams.length === 0) {
        teamPanels = <NoTeamsPage/>;
    }
    return (
        <Container size="sm" className={classes.root}>
            <VerticalTabBar value={currentTabIndex}
                            changed={handleChange}
                            toggleDrawer={toggleDrawer}
                            teamNames={teamNames}
            />
            {teamPanels}
            <SideDrawer open={openDrawer} toggleDrawer={toggleDrawer}>
                {!editTeam ? <CreateTeamDrawer close={toggleDrawer} changeTabIndex={handleChange}/> :
                    <EditTeamDrawer close={toggleDrawer} teamId={editTeam}/>
                }
            </SideDrawer>
        </Container>
    )

};
const mapStateToProps = state => {
    return {
        teams: state.teams.teams,
        userId: state.auth.userId,
        loading: state.teams.loading,
        actionItems: state.teams.actionItems,
        boards: state.teams.boards
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchTeamsData: (userId) => dispatch(fetchTeams(userId)),
        onDeleteTeam: (teamId) => dispatch(deleteTeam(teamId)),
        onLeaveTeam: (userId, teamId, users, isCreator) => dispatch(removeUser(userId, teamId, users, isCreator)),
        onDeleteBoard: (boardId) => dispatch(teamsBoardDelete(boardId)),
        onAlertError: (msg) => dispatch(alertError(msg)),
        onSetCurrentBoard: (userId, boardId) => dispatch(setCurrentBoard(userId, boardId)),
        onEditActionItem: (teamId, id, message, status, assignedTo) => dispatch(teamActionItemEdit(teamId, id, message, status, assignedTo)),
        onTeamActionItemDelete: (teamId, id) => dispatch(teamActionItemDelete(teamId, id))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TeamManagement);