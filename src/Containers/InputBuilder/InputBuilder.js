import React, {useState} from "react";
import Input from "../../Components/UI/Input/Input";
import isFormValid from "../../utilities/formValidation";


const InputBuilder = props => {
    const [userForm, setUserForm] = useState(props.formInputs);
    const onChangeInputHandler = (type, evt) => {
        const {isValid, errorMsg} = isFormValid(evt.target.value, userForm, type);
        const form = {
            ...userForm,
            [type]: {
                ...userForm[type],
                value: evt.target.value,
                isTouched: true,
                valid: isValid,
                errorMsg
            }
        };
        setUserForm(form);
        let isValidForm = Object.keys(form).every((key) => form[key].valid);
        props.callbackFromParent(isValidForm, form);
    };
    return Object.entries(userForm).reduce((acc, [key, value]) => {
        acc.push({
            id: key,
            config: value
        })
        return acc;
    }, []).map((input) => <Input
        key={input.id}
        value={input.config.value}
        error={input.config.isTouched && !input.config.valid}
        config={input.config.elementConfig}
        itemSize={input.config.itemSize}
        errorMsg={input.config.errorMsg}
        changed={onChangeInputHandler.bind(undefined, input.id)}
    />);
};

export default InputBuilder;