import React, {useEffect, useState} from 'react';
import InputBuilder from "../InputBuilder/InputBuilder";
import AuthContainer from "./AuthContainer";
import {Container} from "@material-ui/core";
import {withRouter, Redirect} from 'react-router-dom';
import {connect} from "react-redux";
import {register, login, autoSignIn} from "../../store/actions/actionsMap";

const Authentication = props => {
    const [isFormValid, setFormValidity] = useState(false);
    const [isSubmitted, setSubmitted] = useState(false);
    const [authForm, setAuthForm] = useState(null);
    const {onAutoSignIn} = props;
    useEffect(() => {
        let userId = localStorage.getItem('userId');
        if (userId) {
            onAutoSignIn(userId);
            setSubmitted(true);
            setFormValidity(true)
        }
    }, [onAutoSignIn]);

    const myCallback = (formIsValid, userForm) => {
        setSubmitted(false);
        setFormValidity(formIsValid);
        setAuthForm(userForm);
    };
    const onSubmitHandler = (ev) => {
        ev.preventDefault();
        if (isFormValid) {
            if (props.label === 'login') {
                props.onLogin(authForm.email.value, authForm.password.value);
            } else {
                props.onRegister(authForm.firstName.value, authForm.lastName.value, authForm.email.value, authForm.password.value);
            }
        }
        setSubmitted(true);
    };
    let redirect = null;
    let errorMsg = null;
    if (!props.loading && isSubmitted && !props.error && isFormValid) {
        redirect = props.label === 'login' ? <Redirect to='/'/> : <Redirect to='login'/>;
    }
    if (isSubmitted && !isFormValid) {
        errorMsg = 'You must fill the form correctly.';
    }
    if (isSubmitted && isFormValid && props.errorMsg) {
        errorMsg = props.errorMsg;
    }
    return (
        <React.Fragment>
            {redirect}
            <Container maxWidth="sm">
                <AuthContainer
                    label={props.label}
                    submitForm={onSubmitHandler}
                    message={errorMsg}
                >
                    <InputBuilder
                        formInputs={props.authForm}
                        callbackFromParent={myCallback}
                    />
                </AuthContainer>
            </Container>
        </React.Fragment>

    )
};
const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        errorMsg: state.auth.errorMsg,
        isAuth: state.auth.userId
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onRegister: (firstName, lastName, email, password) => dispatch(register(firstName, lastName, email, password)),
        onLogin: (email, password) => dispatch(login(email, password)),
        onAutoSignIn: (id) => dispatch(autoSignIn(id)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Authentication));