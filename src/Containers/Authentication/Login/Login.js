import React from "react";
import Authentication from "../Authentication";

const Login = props => {
    const LoginForm = {
        email: {
            elementType:'input',
            elementConfig:{
                type:'email',
                label:'Email Address',
            },
            validation:{
                required:true
            },
            value:'',
            itemSize:12,
            valid:false,
            isTouched:false,
            errorMsg:''
        },
        password:  {
            elementType:'input',
            elementConfig:{
                type:'password',
                label:'Password',
            },
            validation:{
                required:true
            },
            value:'',
            itemSize:12,
            valid:false,
            isTouched:false,
            errorMsg:''
        }
    }
    return <Authentication authForm={LoginForm} label='login' />
};

export default Login;

