import React from 'react';
import Authentication from "../Authentication";

const Register = props => {
    const RegisterForm = {
        firstName: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                label: 'First Name',
            },
            validation: {
                required: true,
                minLength: 3
            },
            value: '',
            itemSize: 6,
            valid: false,
            isTouched: false,
            errorMsg: ''
        },
        lastName: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                label: 'Last Name',
            },
            validation: {
                required: true,
                minLength: 3
            },
            value: '',
            valid: false,
            itemSize: 6,
            isTouched: false,
            errorMsg: ''
        },
        email: {
            elementType: 'input',
            elementConfig: {
                type: 'email',
                label: 'Email Address'
            },
            validation: {
                required: true,
                minLength: 6,
                match: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            },
            value: '',
            valid: false,
            itemSize: 12,
            isTouched: false,
            errorMsg: ''
        },
        password: {
            elementType: 'input',
            elementConfig: {
                type: 'password',
                label: 'Password',
            },
            validation: {
                required: true,
                minLength: 6,
                match: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/
            },
            value: '',
            valid: false,
            itemSize: 6,
            isTouched: false,
            errorMsg: ''
        },
        repeatPassword: {
            elementType: 'input',
            elementConfig: {
                type: 'password',
                label: 'Repeat Password',

            }, validation: {
                required: true,
                minLength: 6,
                match: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/,
            },
            value: '',
            itemSize: 6,
            valid: false,
            isTouched: false,
            errorMsg: ''
        },

    };
    return (
        <Authentication authForm={RegisterForm} label='register'/>
    );
};

export default Register;