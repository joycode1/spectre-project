import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Avatar, Button, Grid, Typography,Paper} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {Link} from 'react-router-dom'


const useStyles = makeStyles(theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center',
        padding:theme.spacing(3)
    },
    avatar: {
        margin: theme.spacing(2),
        backgroundColor: theme.palette.secondary.main,
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    title:{
        textTransform:'uppercase',
        fontFamily:'Shrikhand,cursive'
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
        alignSelf: 'center'
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    errorMsg:{
        paddingTop:theme.spacing(2)
    }
}));
const AuthContainer = props => {
    const classes = useStyles();
    let form = <form className={classes.form} onSubmit={props.submitForm} noValidate>
        <Grid container spacing={2}>
            {props.children}
        </Grid>
        <Typography variant="subtitle2" color="error" align="center" className={classes.errorMsg}>{props.message}</Typography>
        <Button
            type='submit'
            fullWidth
            variant="contained"
            color="primary"
            size="large"
            className={classes.submit}>
            {props.label}
        </Button>
        <Grid container justify="flex-end">
            <Grid item>
                <Link to={props.label === 'register' ? '/login' : '/register'} variant="body2">
                    {props.label === 'register' ? 'Already have an account? Login' : 'Dont have an account? Register'}
                </Link>
            </Grid>
        </Grid>
    </form>;

    return (
        <Paper evevation="3" className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h4"  className={classes.title}>
                    {props.label === 'register' ? 'Register' : 'Login'}
                </Typography>
                {form}
        </Paper>)
};

export default AuthContainer;