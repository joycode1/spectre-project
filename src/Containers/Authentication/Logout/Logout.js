import React, {useEffect} from "react";
import {connect} from "react-redux";
import {logout} from "../../../store/actions/actionsMap";
import {Redirect} from "react-router-dom";
import {Typography, makeStyles} from "@material-ui/core";

const styles = makeStyles(theme => ({
    title: {
        textTransform:'uppercase',
        fontFamily: 'Shrikhand, cursive',
    }
}));
const Logout = props => {
    const {onLogout} = props;
    const classes = styles();
    useEffect(() => {
        onLogout();
    }, [onLogout]);
    let redirect = null;
    if (!localStorage.getItem('userId')) {
        redirect = <Redirect to='/login'/>
    }
    return (<React.Fragment>
            {redirect}
            <Typography component="h1" variant="h4" align="center" className={classes.title}>Logging out...</Typography>
        </React.Fragment>
    );
};

const mapDispatchToProps = dispatch => {
    return {
        onLogout: () => dispatch(logout())
    }
}
export default connect(null, mapDispatchToProps)(Logout)