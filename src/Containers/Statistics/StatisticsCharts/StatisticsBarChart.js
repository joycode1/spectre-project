import React from 'react';
import {Typography, useTheme} from "@material-ui/core";

import {ResponsiveBar} from "@nivo/bar";
import {makeStyles} from "@material-ui/core/styles";


const font = {
    axis: {
        fontFamily: 'inherit',
        fontWeight: 'bold',
        fontSize: 11
    }
};
const useStyles = makeStyles(theme => ({
    root: {
        height: 500,
        marginBottom:theme.spacing(2),
        marginTop:theme.spacing(1)
    }
}))
const StatisticsBarChart = props => {
    const classes=useStyles();
    const theme = useTheme();
    return (
        <div className={classes.root}>
            <Typography variant="h6">{props.title}</Typography>
            <ResponsiveBar
                colors={{scheme: 'paired'}}
                layout={props.layout}
                groupMode={props.groupMode}
                indexBy={props.indexBy}
                padding={0.3}
                margin={{top: props.top, right: 130, bottom: 50, left: props.left}}
                data={props.data}
                keys={['votes', 'comments']}
                theme={font.axis}
                defs={[
                    {
                        id: 'dots',
                        type: 'patternDots',
                        background: 'inherit',
                        color: theme.palette.primary.dark,
                        size: 1,
                        padding: 5,
                        stagger: true
                    },
                    {
                        id: 'lines',
                        type: 'patternLines',
                        background: 'inherit',
                        color: theme.palette.secondary.dark,
                        rotation: -45,
                        lineWidth: 5,
                        spacing: 13
                    }
                ]}
                fill={[
                    {
                        match: {
                            id: 'votes'
                        },
                        id: 'dots'
                    },
                    {
                        match: {
                            id: 'comments'
                        },
                        id: 'lines'
                    }
                ]}
                borderColor={{from: 'color', modifiers: [['darker', 1.6]]}}
                axisTop={null}
                axisRight={null}
                axisBottom={{
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: props.xAxisLabel,
                    legendPosition: 'middle',
                    legendOffset: 32
                }}
                axisLeft={{
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: props.yAxisLabel,
                    legendPosition: 'middle',
                    legendOffset: -80
                }}
                labelTextColor="black"
                labelSkipWidth={6}
                labelSkipHeight={6}
                legends={[
                    {
                        dataFrom: 'keys',
                        anchor: 'bottom-right',
                        direction: 'column',
                        justify: false,
                        translateX: 120,
                        translateY: 0,
                        itemsSpacing: 2,
                        itemWidth: 100,
                        itemHeight: 20,
                        itemDirection: 'left-to-right',
                        itemOpacity: 0.85,
                        symbolSize: 20,
                        effects: [
                            {
                                on: 'hover',
                                style: {
                                    itemOpacity: 1
                                }
                            }
                        ]
                    }
                ]}
                animate
                motionStiffness={90}
                motionDamping={15}
            />
        </div>
    )

}
export default StatisticsBarChart;