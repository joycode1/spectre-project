import React from 'react';
import {findOccurrences} from "../../../utilities/findOccurrences";
import StatisticsBarChart from "./StatisticsBarChart";
const StatisticsBoardParticipationChart = props => {
    const data = props.team.users.reduce((acc, curr, index) => {
        const comments = findOccurrences(props.comments, 'creatorId').find(cmt => Number(cmt[0]) === curr.id);
        const votes = findOccurrences(props.votes, 'userId').find(cmt => Number(cmt[0]) === curr.id);
        acc.push({
            name: `${curr.firstName} ${curr.lastName}`,
            votes: votes ? votes[1] : 0,
            comments: comments ? comments[1] : 0
        })
        return acc;
    }, [])

    return <StatisticsBarChart data={data}
                               layout="horizontal"
                               groupMode="stacked"
                               indexBy="name"
                               left={100}
                               top={0}
                               yAxisLabel="users"
                               xAxisLabel="votes/comments"
                               title="User Participation by Votes and Comments"
    />
}

export default StatisticsBoardParticipationChart;