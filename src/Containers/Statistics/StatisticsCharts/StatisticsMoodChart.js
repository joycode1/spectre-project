import React from "react";
import {ResponsiveSwarmPlot} from '@nivo/swarmplot'
import {blue} from "@material-ui/core/colors";
import {Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {moodColorMap} from "../../../utilities/moodColorMap";
const font = {
    axis: {
        fontFamily: 'inherit',
        fontWeight: 'bold',
        fontSize: 11
    }
};
const useStyles = makeStyles(theme => ({
    root: {
        height: 500,
        marginBottom:theme.spacing(2),
        marginTop:theme.spacing(1)
    }
}))

const StatisticsMoodChart = (props) => {
    const classes=useStyles();
    const data = props.team.users.reduce((acc, curr, index) => {
        const mood=props.userMood.find(mood=>mood.userId===curr.id);
        acc.push({
            group: "users",
            id: `${curr.firstName} ${curr.lastName}`,
            mood: mood ? mood.mood : 0
        })
        return acc;
    }, [])

    return <div className={classes.root}>
        <Typography variant="h6">Users Overall Mood During Last Sprint</Typography>
        <ResponsiveSwarmPlot
            data={data}
            groups={["users"]}
            value="mood"
            valueScale={{type: 'linear', min: 0, max: 10, reverse: false}}
            size={28}
            spacing={4}
            layout="horizontal"
            forceStrength={4}
            simulationIterations={100}
            theme={font.axis}
            colors={data.map(d => moodColorMap[d.mood] ? moodColorMap[d.mood] : blue["100"])}
            colorBy="id"
            borderColor={{theme: 'background'}}
            margin={{top: 30, right: 100, bottom: 80, left: 100}}
            enableGridX={false}
            axisTop={null}
            axisBottom={{
                orient: 'bottom',
                tickSize: 10,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'mood scale',
                legendPosition: 'middle',
                legendOffset: 50
            }}
            axisLeft={{
                orient: 'left',
                tickSize: 20,
                tickPadding: 20,
                tickRotation: 0,
                legendPosition: 'middle',
                legendOffset: -76
            }}
            motionStiffness={50}
            motionDamping={10}
        />
    </div>
}

export default StatisticsMoodChart;