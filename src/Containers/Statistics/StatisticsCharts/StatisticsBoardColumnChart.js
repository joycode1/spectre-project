import React from 'react';
import {findOccurrences} from "../../../utilities/findOccurrences";
import StatisticsBarChart from "./StatisticsBarChart";

const StatisticsBoardColumnChart = props => {

    const data = props.board.columns.reduce((acc, curr, index) => {
        const comments = findOccurrences(props.comments, 'boardIndex').find(cmt => Number(cmt[0]) === index);
        const votes = findOccurrences(props.votes.reduce((acc, curr) => {
            const boardIndex = props.comments.find(comment => comment.id === curr.commentId).boardIndex;
            acc.push({boardIndex, ...curr});
            return acc;
        }, []), 'boardIndex').find(cmt => Number(cmt[0]) === index);
        acc.push({column: curr.title, comments: comments ? comments[1] : 0, votes: votes ? votes[1] : 0})
        return acc;
    }, []);
    return (
        <StatisticsBarChart data={data}
                            layout="vertical"
                            groupMode="grouped"
                            indexBy="column"
                            left={100}
                            top={30}
                            yAxisLabel="votes/comments"
                            xAxisLabel="columns"
                            title="Comments and Vote Count By Column"
        />
    )
}

export default StatisticsBoardColumnChart;