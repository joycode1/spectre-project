import React from "react";
import {findOccurrences} from "../../../utilities/findOccurrences";
import {ResponsivePie} from "@nivo/pie";
import {Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const font = {
    axis: {
        fontFamily:'inherit',
        fontWeight:'bold',
        fontSize:11
    }
};
const useStyles = makeStyles(theme => ({
    root: {
        height: 500,
        marginBottom:theme.spacing(2),
        marginTop:theme.spacing(1)
    }
}))
const StatisticsPieChart = props =>{
    const classes=useStyles();
    let labelsData=[];
    if(props.commentsLabels.length>0){
       labelsData=findOccurrences(props.commentsLabels,'labelId').reduce((acc,curr)=>{
            const findLabel=props.boardLabels.find(label=>label.id===Number(curr[0]));
            acc.push({id:findLabel.name,label:findLabel.name,color:findLabel.color,value:curr[1]})
            return acc;
        },[]);
    }

    return <div className={classes.root}>
        <Typography variant="h6">Topics Discussed by Labels and Comments</Typography>
        <ResponsivePie
            theme={font.axis}
            colors={labelsData.map(c=>c.color)}
            data={labelsData}
            margin={{ top: 0, right: 110, bottom: 80, left:100 }}
            innerRadius={0.5}
            padAngle={0.7}
            cornerRadius={3}

            borderWidth={1}
            borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
            radialLabelsSkipAngle={10}
            radialLabelsTextXOffset={6}
            radialLabelsTextColor="#333333"
            radialLabelsLinkOffset={0}
            radialLabelsLinkDiagonalLength={16}
            radialLabelsLinkHorizontalLength={24}
            radialLabelsLinkStrokeWidth={1}
            radialLabelsLinkColor={{ from: 'color' }}
            slicesLabelsSkipAngle={10}
            slicesLabelsTextColor="#333333"
            animate={true}
            motionStiffness={90}
            motionDamping={15}
            legends={[
                {
                    anchor: 'bottom',
                    direction: 'row',
                    translateY: 56,
                    itemWidth: 100,
                    itemHeight: 18,
                    itemTextColor: '#999',
                    symbolSize: 18,
                    symbolShape: 'circle',
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemTextColor: '#000'
                            }
                        }
                    ]
                }
            ]}
        />
    </div>


}
export default StatisticsPieChart;