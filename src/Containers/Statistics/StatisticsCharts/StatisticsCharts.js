import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Paper, Grid, Typography} from "@material-ui/core";
import StatisticsBoardColumnChart from "./StatisticsBoardColumnChart";
import StatisticsBoardParticipationChart from "./StatisticsBoardParticipationChart";
import StatisticsPieChart from "./StatisticsPieChart";
import StatisticsMoodChart from "./StatisticsMoodChart";
import EqualizerIcon from '@material-ui/icons/Equalizer';

const useStyles = makeStyles(theme => ({
    paperRoot: {
        width: '100%',
        height: 'auto',
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1),

    },
    heading: {
        marginBottom: theme.spacing(1)
    },
    title: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        fontWeight: 'bold'
    }
}));

const StatisticsChart = props => {
    const classes = useStyles();
    return (
                <Paper className={classes.paperRoot}>
                    <div className={classes.heading}>
                        <Typography variant="h5" className={classes.title}>
                            <EqualizerIcon color="primary"/>Reports</Typography>
                    </div>
                    <Grid container spacing={3} direction="row" alignItems="center" justify="center">
                        <Grid item xs={12} sm={12} md={12} lg={6}>
                            <StatisticsBoardParticipationChart team={props.team} comments={props.comments}
                                                               votes={props.votes}/>
                        </Grid><
                        Grid item xs={12} sm={12} md={12} lg={6}>
                        <StatisticsBoardColumnChart board={props.board} comments={props.comments} votes={props.votes}/>
                    </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={6}>
                            <StatisticsPieChart boardLabels={props.boardLabels} commentsLabels={props.commentsLabels}/>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={6}>
                            <StatisticsMoodChart team={props.team} userMood={props.userMood}/>
                        </Grid>
                    </Grid>
                </Paper>

    )
};

export default StatisticsChart;