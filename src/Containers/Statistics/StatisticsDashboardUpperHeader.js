import React from "react";
import {
    Paper,
    Typography,
    Select,
    MenuItem,
    FormControl,
    InputLabel,
    Avatar
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import TimelineIcon from '@material-ui/icons/Timeline';
import defaultTeamAvatar from "../../assets/images/default_team.png";

const useStyles = makeStyles(theme => ({
    paper: {
        width: '100%',
        height: 'auto',
        padding: theme.spacing(2),
        marginBottom: theme.spacing(2),
        display: 'flex',
        justifyContent: 'space-between'
    },
    title: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        fontWeight: 'bold'

    },
    heading: {
        display: "flex",
        justifyContent: "space-between"
    },
    formControl: {
        minWidth: 200,
    },
    teamSelect: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",

    }
}));

const StatisticsDashboardUpperHeader = props => {
    const classes = useStyles();
    return (<Paper className={classes.paper}>
        <div className={classes.heading}>
            <Typography variant="h4" className={classes.title}>
                <TimelineIcon color="primary"/>Statistics</Typography>
        </div>
        <FormControl className={classes.formControl}>
            <InputLabel shrink required>
                Select a board
            </InputLabel>
            <Select multiline value={props.selected} onChange={props.selectBoardHandler}
                    renderValue={(value) => props.boards.filter(board=>board.id===value).map(board=> <Typography key={board.id} variant="subtitle1"> {board.boardName}</Typography>)}
            >
                <MenuItem value="">
                    <em>Select a board:</em>
                </MenuItem>
                {props.teams && props.boards &&
                props.teams.map((team, index) => {
                    return props.boards.filter(board => board.teamId === team.id && board.isBoardLocked).map((board, i) => <MenuItem key={i} value={board.id}>
                        <div className={classes.teamSelect}>
                            <Avatar src={team.avatar ? team.avatar : defaultTeamAvatar} variant="square"/>
                            <Typography variant="subtitle2" color="textSecondary">{team.teamName}</Typography>
                            <Typography variant="subtitle2" color="textSecondary">&nbsp;-&nbsp;</Typography>
                        </div>
                        <Typography variant="subtitle1"> {board.boardName}</Typography></MenuItem>)
                })
                }
            </Select>
        </FormControl>

    </Paper>)

}

export default StatisticsDashboardUpperHeader;