import React from "react";
import {CardContent, Paper, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {titleTemplateMap} from "../../utilities/templateMap";
import * as moment from "moment";
import TeamInfo from "../../Components/TeamInfo/TeamInfo";

const useStyles = makeStyles(theme => ({
    paper: {
        width: '100%',
        height: 'auto',
        padding: theme.spacing(1),
        marginBottom: theme.spacing(1),
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column'
        }
    },
    title: {
        fontFamily: 'Shrikhand, cursive'
    },
    boardInfo: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        [theme.breakpoints.down('sm')]: {
            justifyContent: 'center',
            alignItems: 'center'
        }
    },
    blur:{
        filter:'blur(8px)'
    }
}))
const StatisticsBottomHeader = props => {
    const classes = useStyles();
    const dateCreated = props.board.dateCreated && moment(props.board.dateCreated).format("Do MMM YY");
    return <Paper className={[!props.board && classes.blur, classes.paper].join(' ')}>
        {Object.keys(props.board).length > 0 ?
            <React.Fragment>
            <CardContent>
            <div className={[props.loading && classes.blur, classes.boardInfo].join(' ')}>
                <Typography variant="h5"
                            className={[!props.board.boardName && classes.blur, classes.title].join(' ')}>{props.board.boardName}
                </Typography>
                <Typography variant="subtitle1" color="textSecondary"
                >{props.board.template && titleTemplateMap[props.board.template]()}</Typography>
                <Typography variant="subtitle2" color="textSecondary" align="left"
                >Created: {dateCreated}</Typography>
            </div>
        </CardContent>
           <TeamInfo teamData={props.team} creatorId={props.team.creatorId}/>
            </React.Fragment>:
            <Typography variant="subtitle1" color="textSecondary"
            >Select a complete retrospective board in order to view statistics</Typography>
        }
    </Paper>
}
export default StatisticsBottomHeader;