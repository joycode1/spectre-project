import React, {useEffect, useState} from "react";
import StatisticsDashboardUpperHeader from "./StatisticsDashboardUpperHeader";
import {Container} from "@material-ui/core";
import {connect} from "react-redux";
import {fetchStatisticsBoardData,fetchTeamsBoards,clearStatisticsBoard} from "../../store/actions/actionsMap";
import StatisticsBottomHeader from "./StatisticsBottomHeader";
import StatisticsHighlights from "./StatisticsHighlights";
import StatisticsChart from "./StatisticsCharts/StatisticsCharts";
import StatisticsCompletedActionItems from "./StatisticsCompletedActionItems";

const StatisticsDashboard = props => {
    const [selectedBoard, setSelectedBoard] = useState("");
    const {teams,onFetchTeamsBoards,onClearBoard} = props;
    useEffect(()=>{
        onClearBoard();
    },[onClearBoard]);
    useEffect(()=>{
        if(teams){
            onFetchTeamsBoards(teams);
        }
    },[onFetchTeamsBoards,teams])
    const changeSelectedBoardHandler = (ev) => {
        setSelectedBoard(ev.target.value);
        if (ev.target.value) {
            const foundBoard = props.boards.find(board => board.id === ev.target.value);
            const foundTeam = props.teams.find(team => team.id === foundBoard.teamId);
            props.onFetchCurrentBoardData(foundBoard, foundTeam)
        }
    }

    return <Container fixed maxWidth="lg">
        <StatisticsDashboardUpperHeader selected={selectedBoard}
                                        selectBoardHandler={changeSelectedBoardHandler}
                                        teams={props.teams}
                                        boards={props.boards}
        />
        <StatisticsBottomHeader
            board={props.currentBoard}
            team={props.currentTeam}
        />
        {props.comments && props.votes && props.currentTeam && props.actionItems && props.currentBoard && props.commentsLabels && props.boardLabels && props.userMood &&
       props.actionItems &&
        <React.Fragment>
            <StatisticsHighlights comments={props.comments}
                                  votes={props.votes}
                                  team={props.currentTeam}
                                  board={props.currentBoard}
                                  actionItems={props.actionItems}
            />
            <StatisticsChart board={props.currentBoard}
                             comments={props.comments}
                             votes={props.votes}
                             team={props.currentTeam}
                             boardLabels={props.boardLabels}
                             commentsLabels={props.commentsLabels}
                             userMood={props.userMood}
            />
            <StatisticsCompletedActionItems actionItems={props.actionItems.filter(item=>item.status==='done')}
                                            team={props.currentTeam}
            />

        </React.Fragment>
        }
    </Container>
}
const mapStateToProps = state => {
    return {
        teams: state.teams.teams,
        boards: state.teams.boards,
        loading: state.statistics.loading,
        currentBoard: state.statistics.currentBoard,
        currentTeam: state.statistics.currentTeam,
        comments: state.statistics.comments,
        votes: state.statistics.votes,
        actionItems: state.statistics.actionItems,
        userId: state.auth.userId,
        boardLabels: state.statistics.boardLabels,
        commentsLabels: state.statistics.commentsLabels,
        userMood:state.statistics.userMood
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchCurrentBoardData: (board, team) => dispatch(fetchStatisticsBoardData(board, team)),
        onFetchTeamsBoards: (teams)=> dispatch(fetchTeamsBoards(teams)),
        onClearBoard:()=>dispatch(clearStatisticsBoard())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(StatisticsDashboard);