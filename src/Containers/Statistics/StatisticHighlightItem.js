import React from 'react';
import {Card, CardContent, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {amber} from "@material-ui/core/colors";
const useStyles = makeStyles(theme=>({
    root:{
        minWidth:200
    },
    cardContent:{
        display:"flex",
        flexDirection:'column',
        alignItems:'center'
    },
    startIcon:{
        color:amber["400"]
    },

}));
const StatisticsHighlightItem = props =>{
    const classes = useStyles();
    return <Card  variant="outlined" className={classes.root}>
        <CardContent className={classes.cardContent}>
            {props.icon}
            <Typography className={classes.title} variant="body1" gutterBottom>
                {props.primaryText}
            </Typography>
            <Typography variant="body2"  color="textSecondary">
                {props.secondaryText}
            </Typography>
        </CardContent>
    </Card>
}

export default StatisticsHighlightItem;