import React from "react";
import {
    Paper,
    Typography,
    List,
    ListItemText,
    ListItem,
    Divider,
} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import DescriptionIcon from '@material-ui/icons/Description';
import * as moment from 'moment';

const useStyles = makeStyles(theme => ({

    paperRoot: {
        width: '100%',
        height: 'auto',
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column'
    },
    heading: {
        marginBottom: theme.spacing(1)
    },
    title: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        fontWeight: 'bold'
    },
    list: {
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1)
    }
}));
const StatisticsCompletedActionItems = props => {
    const classes = useStyles();
    console.log(props)
    return (
        <Paper className={classes.paperRoot}>
            <div className={classes.heading}>
                <Typography variant="h5" className={classes.title}>
                    <DescriptionIcon color="primary"/>Completed Action Items</Typography>
            </div>
            {props.actionItems.length > 0 ?
                <List className={classes.list}>
                    {props.actionItems.map(item =>
                        <React.Fragment key={item.id}>
                            <ListItem>
                                <ListItemText
                                    primary={item.message}
                                />
                                <div>
                                    <Typography color="textSecondary"
                                        variant="subtitle2"
                                    >Completed: {moment(item.dateEdited).format("Do MMM YY")}</Typography>
                                    <Typography color="textSecondary" variant="subtitle2" >Assigned
                                        to: {item.assignedTo === 'team' ? 'Team' : props.team && props.team.hasOwnProperty('users') ? props.team.users.filter(user => user.id === item.assignedTo).map(user => `${user.firstName} ${user.lastName}`) : null}</Typography>
                                </div>
                            </ListItem>
                            <Divider variant="middle"/>
                        </React.Fragment>
                    )}
                </List> :
                <Typography align="center" variant="body1">No Completed Action Items</Typography>
            }
        </Paper>
    )

}

export default StatisticsCompletedActionItems;