import React from "react";
import {Grid, Paper, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import StarIcon from '@material-ui/icons/Star';
import {amber} from "@material-ui/core/colors";
import StatisticsHighlightItem from "./StatisticHighlightItem";
import WhatshotIcon from '@material-ui/icons/Whatshot';
import DescriptionIcon from '@material-ui/icons/Description';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import {findOccurrences} from "../../utilities/findOccurrences";

const useStyles = makeStyles(theme => ({
    root: {
        minHeight: 150
    },
    cardContent: {
        display: "flex",
        flexDirection: 'column',
        alignItems: 'center'
    },
    startIcon: {
        color: amber["400"]
    },
    paperRoot: {
        width: '100%',
        height: 'auto',
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column'
    },
    heading: {
        marginBottom: theme.spacing(1)
    },
    title: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        fontWeight: 'bold'
    }
}));
const StatisticsHighlights = props => {
    const classes = useStyles();
    let [commenter, commentsCount] = findOccurrences(props.comments, 'creatorId')[0];
    commenter = props.team.users.find(user => user.id === Number(commenter));

    let completedActionItems = props.actionItems.filter(actionItem => actionItem.status === 'done');
    let [column, columnCommentsCount] = findOccurrences(props.comments, 'boardIndex')[0];

    return (
        <Paper className={classes.paperRoot}>
            <div className={classes.heading}>
                <Typography variant="h5" className={classes.title}>
                    <WhatshotIcon color="primary"/>Highlights</Typography>
            </div>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={4} lg={3}>
                    <StatisticsHighlightItem
                        primaryText={commenter ? `${commenter.firstName} ${commenter.lastName}`: "-"}
                        secondaryText={`Most Comments (${commentsCount})`}
                        icon={<StarIcon className={classes.startIcon} fontSize="large"/>}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={3}>
                    <StatisticsHighlightItem
                        primaryText={`${completedActionItems.length} Completed Action Items`}
                        secondaryText={`${props.actionItems.length - completedActionItems.length} Action Items Left`}
                        icon={<DescriptionIcon color="primary" fontSize="large"/>}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={3}>
                    <StatisticsHighlightItem
                        primaryText={`${props.votes.length} Votes Registered`}
                        secondaryText={`Out of ${props.team.users.length} Participants`}
                        icon={<ThumbUpIcon color="primary" fontSize="large"/>}
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={3}>
                    <StatisticsHighlightItem
                        primaryText={`${props.board.columns[column].title}`}
                        secondaryText={`Column with Most Comments (${columnCommentsCount})`}
                        icon={<ChatBubbleIcon color="primary" fontSize="large"/>}
                    />
                </Grid>
            </Grid>
        </Paper>
    )

}

export default StatisticsHighlights;