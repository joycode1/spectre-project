import React, {useState} from 'react';
import PostAddIcon from '@material-ui/icons/PostAdd';
import {makeStyles} from '@material-ui/core/styles';
import {
    Container,
    Avatar,
    Button,
    TextField,
    Paper,
    Typography,
    MenuItem,
    FormControl,
    InputLabel,
    Select,
    Grid
} from "@material-ui/core";
import {templateMap, titleTemplateMap} from "../../utilities/templateMap";
import {connect} from "react-redux";
import defaultTeamAvatar from "../../assets/images/default_team.png";
import {createBoard} from "../../store/actions/actionsMap";
import {withRouter, Redirect} from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%'
    },
    image: {

        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    title: {
        textTransform: 'uppercase',
        fontFamily: 'Shrikhand,cursive'
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    formControl: {
        margin: theme.spacing(1)
    },
    teamSelect: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",

    }

}));

const CreateBoard = props => {
    const classes = useStyles();
    const {userId} = props;
    const [boardInputs, setBoardInputs] = useState({
        boardName: "",
        template: "mad_sad_glad",
        teamId: -1
    });
    const [errorMsg, setErrorMsg] = useState('');
    const [isSubmitted,setSubmitted] = useState(false);
    const boardInputChangeHandler = (element, ev) => {
        setErrorMsg('');
        setBoardInputs({
            ...boardInputs,
            [element]: ev.target.value
        });
    };
    const createBoardHandler = (ev) => {
        ev.preventDefault();
        const teamIndex = props.teams.findIndex(team=>team.id===boardInputs.teamId);
        const isCreator = teamIndex!==-1 && props.teams[teamIndex].creatorId===userId;
        if (boardInputs.boardName === "" && boardInputs.teamId === -1) {
            setErrorMsg('Boards must have a name and you must select a team.')
        } else if (boardInputs.boardName === "") {

            setErrorMsg('Boards must have a name.')
        } else if (boardInputs.teamId === -1) {
            setErrorMsg('You must select a team.')
        } else if(!isCreator) {
           setErrorMsg('You must be the creator of the team.')
        }else {
            props.onCreateBoard(boardInputs.boardName,boardInputs.teamId,boardInputs.template,userId);
            setSubmitted(true);
        }

    }
    let teamsSelect = null;
    let redirect = null;
    if (props.teams.length > 0) {
        teamsSelect = props.teams.map((team) => <MenuItem key={team.id} value={team.id}>
            <div className={classes.teamSelect}>
                <Avatar src={team.avatar ? team.avatar : defaultTeamAvatar} variant="square"/>
                <Typography variant="subtitle2">{team.teamName}</Typography>
            </div>
        </MenuItem>)
    }
    if(isSubmitted && !props.loading){
        redirect = <Redirect to='/boards'/>;
    }
    return (
        <Container fixed maxWidth="lg" className={classes.root}>
            {redirect}
            <Grid container>
                <Grid item xs={false} sm={4} md={7} component={Paper} elevation={3} square className={classes.image}
                      style={{backgroundImage: `url(${templateMap[boardInputs.template]()})`}}/>
                <Grid item xs={12} sm={8} md={5} component={Paper} elevation={3} square>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <PostAddIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5" className={classes.title}>
                            Create a board
                        </Typography>
                        <form className={classes.form} noValidate onSubmit={createBoardHandler}>
                            <FormControl className={classes.formControl} fullWidth>
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Board name"
                                    autoFocus
                                    value={boardInputs.boardName}
                                    onChange={boardInputChangeHandler.bind(undefined, 'boardName')}
                                />
                            </FormControl>
                            <Grid container>
                                <Grid item xs={12}>
                                    <FormControl className={classes.formControl} fullWidth>
                                        <InputLabel shrink required>
                                            Board Template
                                        </InputLabel>
                                        <Select
                                            value={boardInputs.template}
                                            onChange={boardInputChangeHandler.bind(undefined, 'template')}
                                        >
                                            <MenuItem value="mad_sad_glad">{titleTemplateMap["mad_sad_glad"]()}</MenuItem>
                                            <MenuItem value="appreciation">{titleTemplateMap["appreciation"]()}</MenuItem>
                                            <MenuItem value="sailboat">{titleTemplateMap["sailboat"]()}</MenuItem>
                                            <MenuItem value="start_stop_continue">{titleTemplateMap["start_stop_continue"]()}</MenuItem>
                                            <MenuItem value="4ls">{titleTemplateMap["4ls"]()}</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControl fullWidth className={classes.formControl}>
                                        <InputLabel required>
                                            Team
                                        </InputLabel>
                                        <Select
                                            multiline
                                            displayEmpty
                                            value={boardInputs.teamId}
                                            onChange={boardInputChangeHandler.bind(undefined, 'teamId')}
                                        >
                                            <MenuItem value={-1}>
                                                <em>Select a team</em>
                                            </MenuItem>
                                            {teamsSelect}
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                            <Typography variant="subtitle2" color="error" align="center"
                                        className={classes.errorMsg}>{errorMsg}</Typography>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Create
                            </Button>
                        </form>
                    </div>
                </Grid>
            </Grid>
        </Container>
    );
}
const mapStateToProps = state => {
    return {
        userId: state.auth.userId,
        teams: state.teams.teams,
        loading:state.currBoard.loading
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onCreateBoard:(boardName,teamId,template,userId)=>dispatch(createBoard(boardName,teamId,template,userId))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CreateBoard));