import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Highlighter from "react-highlight-words";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import yellow from "@material-ui/core/colors/yellow";
import {
    LinearProgress,
    CardHeader,
    CardMedia,
    CardContent,
    Avatar,
    IconButton,
    Typography,
    Menu,
    MenuItem
} from "@material-ui/core";
import {templateMap, titleTemplateMap} from "../../../utilities/templateMap";
import * as moment from 'moment';
import defaultAvatar from "../../../assets/images/default_team.png";

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 275,
        minHeight:400,
        maxHeight:460
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    blur: {
        filter: 'blur(8px)'
    },
    title: {
        fontFamily: 'Shrikhand,cursive'
    },
    progressText: {
        fontWeight: 'bold'
    },
    highlight: {
        backgroundColor: yellow["400"]
    }
}));
const BoardCard = props => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const dateCreated = moment(props.board.dateCreated).format("Do MMM YY");
    const boardProgressMap = {
        0: 0,
        1: 25,
        2: 50,
        3: 75,
        4: 100
    }
    const openMenuHandler = (ev) => {
        setAnchorEl(ev.currentTarget);
    }
    const closeMenuHandler = () => {
        setAnchorEl(null);
    };
    return (
        <Card className={classes.root}>
            <CardContent className={classes.content}>
                <Typography component="h5" variant="h6"
                            className={[!props.board.boardName && classes.blur, classes.title].join(' ')}>

                    <Highlighter
                        highlightClassName={classes.highlight}
                        searchWords={[props.searchTerm]}
                        autoEscape={true}
                        textToHighlight={props.board.boardName && props.board.boardName}
                    />
                </Typography>
                <Typography variant="subtitle2" color="textSecondary"
                            className={[!props.board.template && classes.blur].join(' ')}>
                    {props.board.template && titleTemplateMap[props.board.template]()}
                </Typography>
            </CardContent>
            <CardMedia
                className={[!props.board.template && classes.blur, classes.media].join(' ')}
                image={props.board.template && templateMap[props.board.template]()}
            />
            <CardContent className={[!props.board.teamInfo && classes.blur].join(' ')}>
                <LinearProgress value={boardProgressMap[props.board.progressIndex]} variant="determinate"/>
                <Typography variant="subtitle2" className={classes.progressText}>
                    Board Progress: {!props.board.isBoardLocked ? `${props.board.progressIndex}/4` : 'Complete' }
                </Typography>
            </CardContent>
            <CardHeader className={[!props.board.teamInfo && classes.blur].join(' ')}
                        avatar={
                            <Avatar
                                src={props.board.teamInfo && props.board.teamInfo.avatar ? props.board.teamInfo.avatar : defaultAvatar}
                                className={classes.avatar} variant="square"/>
                        }
                        action={
                            <React.Fragment>
                                <IconButton aria-label="settings" onClick={openMenuHandler}>
                                    <MoreVertIcon/>
                                </IconButton>
                                <Menu
                                    id="simple-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={Boolean(anchorEl)}
                                    onClose={closeMenuHandler}
                                >
                                    <MenuItem onClick={props.viewBoard}>View</MenuItem>
                                    <MenuItem onClick={props.openDeletePrompt}>Delete</MenuItem>
                                </Menu>
                            </React.Fragment>
                        }
                        title={props.board.teamInfo && props.board.teamInfo.teamName}
                        subheader={dateCreated}
            />
        </Card>
    );
}
export default BoardCard;