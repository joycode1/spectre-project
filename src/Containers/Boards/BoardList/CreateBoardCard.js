import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';

import {CardMedia, Typography, CardActionArea} from "@material-ui/core";
import AddImage from "../../../assets/images/create_board.png";
import {Link} from "react-router-dom";
const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth:275,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    title:{
        fontFamily:'Shrikhand,cursive',
    }
}));
const CreateBoardCard = props => {
    const classes = useStyles();

    return (
        <Card className={classes.root} elevation={6}  variant="outlined">
            <CardActionArea component={Link}  to='/create-board'>
            <CardMedia
                className={classes.media}
                image={AddImage}
                title="add"
            />
                <Typography component="h5" variant="h6" align="center" className={classes.title}>
                    Create a board
                </Typography>

            </CardActionArea>
        </Card>
    );
}
export default CreateBoardCard;