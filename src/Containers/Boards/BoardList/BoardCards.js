import React, {useState} from 'react';
import {Grid} from "@material-ui/core";
import CreateBoardCard from "./CreateBoardCard";
import BoardCard from "./BoardCard";
import {withRouter} from "react-router-dom";
import Prompt from "../../../Components/UI/Prompt/Prompt";

const BoardCards = props => {
    const [open, setOpen] = useState(false);
    const [selectedBoardId, setSelectedBoard] = useState('');
    const deleteBoardHandler = (boardId, ev) => {
        const findBoard = props.boards.findIndex(board => board.id === selectedBoardId);
        if (props.boards[findBoard].creatorId === props.userId) {
            props.onDeleteBoard(selectedBoardId);
            props.onSetCurrentBoard(props.userId, findBoard + 1 <=props.boards.length-1 ? props.boards[findBoard + 1].id : '')
        } else {
            props.onAlertError('You must be the creator of the team.')
        }
        handlePromptClose();
    }
    const viewBoardHandler = (userId, boardId) => {
        console.log(userId, boardId)
        props.onSetCurrentBoard(userId, boardId);
        setTimeout(() => {
            props.history.push('/dashboard')
        }, 100);

    }
    const handlePromptOpen = (boardId) => {
        setOpen(true);
        setSelectedBoard(boardId)
    };
    const handlePromptClose = () => {
        setOpen(false);
        setSelectedBoard('')
    }
    let boards = null;
    if (!props.loading && props.boards.length > 0) {
        boards = props.boards.filter(board => board.boardName.toLowerCase().includes(props.searchTerm.toLowerCase())).map((board, i) =>
            <Grid xs={6} sm={3}  item key={i}>
                <BoardCard board={board} searchTerm={props.searchTerm}
                           openDeletePrompt={handlePromptOpen.bind(undefined, board.id)}
                           viewBoard={viewBoardHandler.bind(undefined, props.userId, board.id)}
                /></Grid>)
    } else if (props.loading) {
        boards = <Grid xs={6} sm={3} item><BoardCard/></Grid>;
    }
    return (<Grid container spacing={1}>
        <Grid xs={6} sm={3} item><CreateBoardCard/></Grid>
        {boards}
        <Prompt open={open} close={handlePromptClose} message="Are you sure?"
                secondaryText="Deleting this board will result in losing all progress,comments and action items"
                accept={deleteBoardHandler}/>
    </Grid>)
};
export default withRouter(BoardCards);