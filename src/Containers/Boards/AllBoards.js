import React, {useEffect, useState} from "react";
import {Container, Paper, Typography, InputBase, fade} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import DashboardIcon from "@material-ui/icons/Dashboard";
import SearchIcon from '@material-ui/icons/Search';
import BoardCards from "./BoardList/BoardCards";
import {connect} from 'react-redux';
import {alertError, fetchTeamsBoards, setCurrentBoard, teamsBoardDelete} from "../../store/actions/actionsMap";


const useStyles = makeStyles(theme => ({
    paper: {
        width: '100%',
        height: 'auto',
        padding: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    title: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        fontWeight: 'bold'

    }, search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.primary.light, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.primary.light, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    heading: {
        display: "flex",
        justifyContent: "space-between"
    }
}));
const AllBoards = props => {
    const classes = useStyles();
    const {onFetchTeamsBoards, teams} = props;
    const [searchTerm, setSearchTerm] = useState('');
    useEffect(() => {
       if(teams.length>0){
           onFetchTeamsBoards(teams);
       }
    }, [onFetchTeamsBoards, teams]);
    const searchBoardsHandler = (ev) => {
        setSearchTerm(ev.target.value);
    };

    return <Container fixed maxWidth="lg">
        <Paper className={classes.paper}>
            <div className={classes.heading}>
                <Typography variant="h5" className={classes.title}> <DashboardIcon
                    color="primary"/>My Boards</Typography>
                <div className={classes.search}>
                    <div className={classes.searchIcon}>
                        <SearchIcon/>
                    </div>
                    <InputBase
                        onChange={searchBoardsHandler}
                        value={searchTerm}
                        placeholder="Search board..."
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        inputProps={{'aria-label': 'search'}}
                    />
                </div>
            </div>

        </Paper>
        <BoardCards boards={props.boards} searchTerm={searchTerm} onDeleteBoard={props.onDeleteBoard}
                    userId={props.userId}
                    onAlertError={props.onAlertError}
                    onSetCurrentBoard={props.onSetCurrentBoard}/>
    </Container>

}
const mapStateToProps = state => {
    return {
        teams: state.teams.teams,
        boards: state.teams.boards,
        loading: state.teams.loading,
        userId: state.auth.userId
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchTeamsBoards: (teams) => dispatch(fetchTeamsBoards(teams)),
        onDeleteBoard: (boardId) => dispatch(teamsBoardDelete(boardId)),
        onAlertError: (msg) => dispatch(alertError(msg)),
        onSetCurrentBoard: (userId, boardId) => dispatch(setCurrentBoard(userId, boardId))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AllBoards);