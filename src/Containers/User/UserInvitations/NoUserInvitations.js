import noInvitations from "../../../assets/images/no_invitations1.png";
import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
const useStyles = makeStyles(theme =>({
    root:{
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        width:200
    },
    avatar:{
        width: theme.spacing(18),
        height: theme.spacing(18),
    }
}))
const NoUserInvitations = props => {
    const classes= useStyles();
    return <div className={classes.root}>
        <img src={noInvitations} className={classes.avatar} alt="no_invitations"   />
    </div>
}

export default NoUserInvitations;