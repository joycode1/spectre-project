import React from 'react';
import {Avatar, Divider, IconButton, ListItem, ListItemAvatar, ListItemText} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import defaultTeam from "../../../assets/images/default_team.png";
import Prompt from "../../../Components/UI/Prompt/Prompt";

const UserInvitation = props =>{
    const [open, setOpen] = React.useState(false);

    const handlePrompt = () => {
        setOpen(!open);
    };
    return (
        <React.Fragment>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <Avatar variant="square" src={props.avatar && props.avatar ? props.avatar : defaultTeam}/>
                </ListItemAvatar>
                <ListItemText
                    primary={props.teamName}
                    secondary={
                        <React.Fragment>
                            <IconButton onClick={props.accept}>
                                <CheckIcon color="secondary"/>
                            </IconButton>
                            <IconButton onClick={handlePrompt}>
                                <ClearIcon color="error"/>
                            </IconButton >
                        </React.Fragment>
                    }
                />
            </ListItem>
            <Divider variant="middle" component="li"/>
            <Prompt open={open} close={handlePrompt} message="Are you sure you remove this invitation?" accept={props.reject}/>
        </React.Fragment>
    );
}
export  default UserInvitation;