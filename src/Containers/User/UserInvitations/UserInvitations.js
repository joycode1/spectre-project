import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {
    List,
    ListSubheader
} from "@material-ui/core";

import UserInvitation from "./UserInvitation";
import NoUserInvitations from "./NoUserInvitations";
import {acceptUserInvitation, deleteUserInvitation} from "../../../store/actions/auth";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline'
    }
}));

const UserInvitations = props => {
    const classes = useStyles();
    let userInvitations = null;
    let invitationsInfo = null;

    if (!props.loading && props.invitations.length > 0) {
        userInvitations = props.invitations.map((inv, i) => <UserInvitation key={i}
                                                                            accept={(ev) => props.onAcceptUserInvitation(inv.id, inv.users, props.userId)}
                                                                            reject={(ev) => {

                                                                                props.onRejectUserInvitation(props.userId, inv.id)
                                                                            }}
                                                                            teamName={inv.teamName}
                                                                            avatar={inv.avatar}/>);
        invitationsInfo = <ListSubheader>You have been invited to join:</ListSubheader>;
    } else {
        invitationsInfo = <ListSubheader>You have no new invitations</ListSubheader>;
        userInvitations = <NoUserInvitations/>
    }
    return (
        <div className={classes.root}>
            <List dense>
                {invitationsInfo}
                {userInvitations}
            </List>
        </div>
    )

}
const mapDispatchToProps = dispatch => {
    return {
        onAcceptUserInvitation: (teamId, teamUsers, userId) => dispatch(acceptUserInvitation(teamId, teamUsers, userId)),
        onRejectUserInvitation: (userId, teamId) => dispatch(deleteUserInvitation(userId, teamId))
    }
}
export default connect(null, mapDispatchToProps)(withRouter(UserInvitations));