import {makeStyles} from "@material-ui/core/styles";

const drawerWidth = 60;

export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,

    },
    drawerPaper: {
        width: drawerWidth,
        backgroundColor: theme.palette.primary.dark,
    },
    drawerContainer: {
        overflow: 'hidden',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),

    },
}));