import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, combineReducers, createStore, compose} from "redux";
import thunk from "redux-thunk";
import authReducer from "./store/reducers/auth";
import teamsReducer from "./store/reducers/teams";
import userSearch from "./store/reducers/userSearch";
import alertReducer from "./store/reducers/alert";
import currentBoardReducer from "./store/reducers/currentBoard";
import statisticsReducer from "./store/reducers/statistics";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from "react-redux";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
const rootReducer = combineReducers({
    auth: authReducer,
    teams:teamsReducer,
    userSearch:userSearch,
    alert: alertReducer,
    currBoard:currentBoardReducer,
    statistics:statisticsReducer
});
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
const app = <Provider store={store}>
    <MuiPickersUtilsProvider utils={MomentUtils}>
    <App/>
    </MuiPickersUtilsProvider>
</Provider>
ReactDOM.render(app, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
