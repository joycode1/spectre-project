import React, {Component} from 'react';
import {connect} from 'react-redux';
import Alert from '../Components/UI/Alert/Alert';
import {alertClear} from '../store/actions/actionsMap';
import {compose} from "redux";

const withAlert = (WrappedComponent) => {
    return class extends Component {

        closeAlertHandler = () => {
            setTimeout(()=>{
                this.props.onAlertClear();
            },1100);

        };

        render() {
            return (
                <React.Fragment>
                    <Alert open={this.props.openAlert} closed={this.closeAlertHandler}
                           severity={this.props.msgType || 'info'}
                           message={this.props.message}/>
                    <WrappedComponent {...this.props}/>
                </React.Fragment>
            )
        }
    }
};
const mapStateToProps = state => {
    return {
        message: state.alert.message,
        hasError: state.alert.hasError,
        openAlert: state.alert.openAlert,
        msgType: state.alert.msgType
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onAlertClear: () => dispatch(alertClear())
    }
};
const composedWithAlert = compose(connect(mapStateToProps, mapDispatchToProps), withAlert);
export default composedWithAlert;
