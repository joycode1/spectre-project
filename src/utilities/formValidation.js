const formValidation =  (currentValue, formObject, type,isEmailTaken) => {
    const validator = formObject[type].validation;
    const label = formObject[type].elementConfig.label;
    let isValid = true;
    let errorMsg = '';
    if (validator.required !== undefined) {
        isValid = currentValue.trim() !== '' && isValid;
        errorMsg = !isValid ? `${label} must not be an empty string` : '';
    }
    if (validator.minLength !== undefined) {
        isValid = validator.minLength <= currentValue.length && isValid;
        errorMsg = !isValid ? `${label} must be at least ${validator.minLength} characters long` : '';
    }
    if (validator.match !== undefined) {
        isValid = currentValue.match(validator.match) && isValid;
        errorMsg = !isValid && type === 'password' ? `${label} must include numbers 0-9,lower-case letters, at least one upper-case letter and must be at least ${validator.minLength} characters long` : !isValid && type === 'email' ? `${label} must contain a valid email address` : '';
    }
    if (type === 'repeatPassword') {
        isValid = currentValue === formObject.password.value && isValid;
        errorMsg = !isValid ? 'Passwords must match' : '';
    }
    return {isValid, errorMsg};
};

export default formValidation;