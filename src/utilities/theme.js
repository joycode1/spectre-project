import {createMuiTheme} from "@material-ui/core";
import {indigo, teal} from "@material-ui/core/colors";

export default createMuiTheme({
    palette: {
        primary: indigo,
        secondary: teal,
        text: {primary: '#212121', secondary: '#757575', small: '#fff' },
        background: {
            default: '#F4F8FC'
        }
    },
    typography: {
        fontFamily: [
            'Noto Sans',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    }

});