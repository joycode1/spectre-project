import {
    blue,
    deepOrange,
    deepPurple,
    green,
    indigo,
    lightGreen,
    pink,
    purple,
    red,
    teal,
    yellow
} from "@material-ui/core/colors";

const labelColors = [red["600"], pink["600"], purple["600"],green["600"],blue["600"],teal["600"],deepOrange["600"],deepPurple["600"],yellow["600"],indigo["600"],lightGreen["600"]];
const commentsColors = [red["200"], pink["200"], purple["200"],green["200"],blue["200"],teal["200"],deepOrange["200"],deepPurple["200"],yellow["200"],indigo["200"],lightGreen["200"]];
export {
    labelColors,
    commentsColors
}