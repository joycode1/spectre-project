import {green, orange, red, yellow} from "@material-ui/core/colors";

export const moodColorMap = {
    1: red["A700"],
    2: red["A400"],
    3: orange["800"],
    4: orange["700"],
    5: yellow["600"],
    6: yellow["500"],
    7: green["A100"],
    8: green["A200"],
    9: green["A400"],
    10: green["A700"]

}