export const findOccurrences = (array, search) => {
    return  Object.entries(array.reduce((acc, curr) => {
        if (typeof acc[curr[search]] == 'undefined') {
            acc[curr[search]] = 0;
        }
        acc[curr[search]] += 1;
        return acc;
    }, {})).sort((a, b) => b[1] - a[1]);

}