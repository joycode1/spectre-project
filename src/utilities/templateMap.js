import appreciation from "../assets/images/templates/appreciation/appreciation.jpg";
import fourLs from "../assets/images/templates/4ls/4ls.png";
import start_stop_continue from "../assets/images/templates/start_stop_continue/start_stop_continue.jpg";
import sailboat from "../assets/images/templates/sailboat/sailboat.png";
import mad_sad_glad from "../assets/images/templates/mad_sad_glad/mad_sad_glad.png";
import idea from "../assets/images/templates/appreciation/idea.png";
import team_spirit from "../assets/images/templates/appreciation/team_spirit.png";
import liked from "../assets/images/templates/4ls/liked.png";
import learned from "../assets/images/templates/4ls/learned.png";
import lacked from "../assets/images/templates/4ls/lacked.png";
import longed from "../assets/images/templates/4ls/longed.png";
import mad from "../assets/images/templates/mad_sad_glad/mad.png";
import glad from "../assets/images/templates/mad_sad_glad/glad.png";
import sad from "../assets/images/templates/mad_sad_glad/sad.png";
import start from "../assets/images/templates/start_stop_continue/start.png";
import stop from "../assets/images/templates/start_stop_continue/stop.png";
import cont from "../assets/images/templates/start_stop_continue/continue.png";
import rocks from "../assets/images/templates/sailboat/rocks.png";
import wind from "../assets/images/templates/sailboat/wind.png";
import anchor from "../assets/images/templates/sailboat/anchor.png";
import island from "../assets/images/templates/sailboat/island.png";
import { blue, deepOrange, teal, cyan, pink} from "@material-ui/core/colors";

const templateMap = {
    'appreciation': () => appreciation,
    '4ls': () => fourLs,
    'start_stop_continue': () => start_stop_continue,
    'sailboat': () => sailboat,
    'mad_sad_glad': () => mad_sad_glad
};
const templateGridColorMap = {
    '4ls': pink["200"],
    'appreciation': deepOrange["A100"],
    'mad_sad_glad': teal["100"],
    'start_stop_continue':blue["A100"],
    'sailboat':cyan["300"]
}
const templateColumns = {
    'appreciation': () => [
        {
            title: 'Team Spirit', secondaryText: 'You really served the team when...', comments: [],
            image: idea
        },
        {
            title: 'Team Ideas', secondaryText: 'What I\'d like to see more of.', comments: [],
            image: team_spirit
        },
    ],
    '4ls': () => [
        {
            title: 'Liked', secondaryText: 'What did you really enjoy about the sprint?', comments: [],
            image: liked
        },
        {
            title: 'Learned', secondaryText: 'What new things did you learn during the sprint?', comments: [],
            image: learned
        },
        {
            title: 'Lacked', secondaryText: 'What things could have been done better during the sprint?', comments: [],
            image: lacked

        },
        {
            title: 'Longed for',
            secondaryText: 'What things did you desire to have during the sprint that were unavailable?',
            comments: [],
            image: longed
        },
    ],
    'start_stop_continue': () => [
        {title: 'Start', secondaryText: 'What should the team start working on?', comments: [], image: start},
        {title: 'Stop', secondaryText: 'What should the team stop working on?', comments: [], image: stop},
        {title: 'Continue', secondaryText: 'What should the team continue working on?', comments: [], image: cont},
    ],
    'sailboat': () => [
        {title: 'Island', secondaryText: 'Your team\'s objective', comments: [],image:island},
        {title: 'Wind', secondaryText: 'What helps the team to move forward?', comments: [],image:wind},
        {title: 'Anchor', secondaryText: 'Things that slow the team down', comments: [],image:anchor},
        {title: 'Rocks', secondaryText: 'What are the potential risks for the team?', comments: [],image:rocks},
    ],
    'mad_sad_glad': () => [
        {title: 'Mad', secondaryText: 'Things that make me mad are...', comments: [], image: mad},
        {title: 'Sad', secondaryText: 'Things that make me sad are...', comments: [], image: sad},
        {title: 'Glad', secondaryText: 'Things that make me glad are..', comments: [], image: glad},
    ]
}
const titleTemplateMap = {
    'appreciation': () => 'Appreciation',
    '4ls': () => 'Liked – Learned – Lacked – Longed For',
    'start_stop_continue': () => 'Start – Stop – Continue',
    'sailboat': () => 'SailBoat',
    'mad_sad_glad': () => 'Mad – Sad – Glad'
}
export {templateMap, templateColumns, titleTemplateMap, templateGridColorMap};

