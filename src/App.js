import React from 'react';
import Layout from "./Containers/Layout/Layout";
import Login from "./Containers/Authentication/Login/Login";
import Register from "./Containers/Authentication/Register/Register";
import Dashboard from "./Containers/Dashboard/Dashboard";
import TeamManagement from "./Containers/TeamManagement/TeamManagement";
import Logout from "./Containers/Authentication/Logout/Logout";
import {connect} from "react-redux";
import {GuardProvider, GuardedRoute} from 'react-router-guards';
import {BrowserRouter, Switch, Redirect} from "react-router-dom";
import CreateBoard from "./Containers/Boards/CreateBoard";
import AllBoards from "./Containers/Boards/AllBoards";
import StatisticsDashboard from "./Containers/Statistics/StatisticsDashboard";
function App(props) {

    const requireLogin = (to, from, next) => {
        if (to.meta.auth) {
            if (props.isAuth) {
                next();
            }
            next.redirect('/login');
        } else {
            next();
        }
    };
    const routes = <GuardProvider guards={[requireLogin]}>
        <Switch>
            <GuardedRoute path="/login" exact component={Login}/>
            <GuardedRoute path="/register" exact component={Register}/>
            <GuardedRoute path="/dashboard" exact component={Dashboard} meta={{auth: true}}/>
            <GuardedRoute path="/create-board" exact component={CreateBoard} meta={{auth: true}}/>
            <GuardedRoute path="/boards" exact component={AllBoards} meta={{auth: true}}/>
            <GuardedRoute path="/team-management" exact component={TeamManagement} meta={{auth: true}}/>
            <GuardedRoute path="/statistics" exact component={StatisticsDashboard} meta={{auth: true}}/>
            <GuardedRoute path="/profile" exact component={() => <p>Profile</p>} meta={{auth: true}}/>
            <GuardedRoute path="/logout" exact component={Logout} meta={{auth: true}}/>
            <Redirect from='/' to='/dashboard'/>
            <GuardedRoute render={() => <p>Not found!</p>}/>
        </Switch>
    </GuardProvider>;
    return (
        <BrowserRouter>

            <Layout loading={props.loading}>
                    {routes}
            </Layout>
        </BrowserRouter>
    );
}

const mapStateToProps = state => {
    return {
        isAuth: state.auth.userId !== null,
        loading: state.auth.loading || state.teams.loading || state.currBoard.loading || state.statistics.loading
    }
};

export default connect(mapStateToProps, null)(App);
